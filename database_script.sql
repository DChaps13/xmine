CREATE DATABASE  IF NOT EXISTS `pruebas8`;
USE `pruebas8`;

SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;


DROP TABLE IF EXISTS `activo`;
CREATE TABLE `activo` (
  `id_activo` int(11) NOT NULL AUTO_INCREMENT,
  `estado` bit(1) DEFAULT NULL,
  `horas_uso` int(11) DEFAULT NULL,
  `marca` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  `peso_bruto` double DEFAULT NULL,
  `placa` varchar(255) DEFAULT NULL,
  `potencia` double DEFAULT NULL,
  `id_tipo_de_activo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_activo`),
  KEY `FKqrviy5lcxo2ybgbxlt12m5sg9` (`id_tipo_de_activo`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `activo` WRITE;

INSERT INTO `activo` VALUES (1,_binary '',0,'Toyota','Rav4',1600,'AFK-365',160,1),(2,_binary '',0,'Toyota','Rav4',1600,'ABC-365',160,1),(3,_binary '',0,'Mercedes-Benz','Sprinter',5000,'AFK-366',160,2),(4,_binary '',0,'Mercedes-Benz','Sprinter',5000,'ABC-366',160,2),(5,_binary '',0,'Toyota','Corolla',1350,'AFK-367',160,3),(6,_binary '',0,'Toyota','Corolla',1350,'ABC-367',160,3),(7,_binary '',0,'Mercedes-Benz','Actros',18000,'AFK-368',310,4),(8,_binary '',0,'Mercedes-Benz','Actros',18000,'ABC-368',310,4),(9,_binary '',0,'CATERPILLAR','907M AG HANDLER ',6000,'AFK-369',160,5),(10,_binary '',0,'CATERPILLAR','907M AG HANDLER ',6000,'ABC-369',160,5);

UNLOCK TABLES;



DROP TABLE IF EXISTS `algoritmo`;

CREATE TABLE `algoritmo` (
  `id_algoritmo` int(11) NOT NULL,
  `flag` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_algoritmo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `algoritmo`
--

LOCK TABLES `algoritmo` WRITE;
INSERT INTO `algoritmo` VALUES (1,0,'algoritmo1');
UNLOCK TABLES;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
CREATE TABLE `auditoria` (
  `id_auditoria` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `id_usuario_creador` int(11) DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `id_solicitud` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_auditoria`),
  KEY `FKidi7onj5jy3o3ygfmteu7byb5` (`id_solicitud`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;

UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;

CREATE TABLE `contrato` (
  `id_contrato` int(11) NOT NULL AUTO_INCREMENT,
  `calificacion_contrato` double DEFAULT NULL,
  `capacidad_total_mant` int(11) DEFAULT NULL,
  `capacidad_total_rep` int(11) DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL,
  `cantidad_calificado` int(11) DEFAULT NULL,
  `capacidad_mantenimiento_inicial` int(11) DEFAULT NULL,
  `capacidad_reparacion_inicial` int(11) DEFAULT NULL,
  `capacidad_total_inicial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_contrato`),
  KEY `FKrg7r9gl8lyt1esqb3m1pwgniy` (`id_proveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;

INSERT INTO `contrato` VALUES (1,10,40,50,'2019-12-30 00:00:00','2019-10-01 00:00:00','no observaciones',1,_binary '',0,40,50,90),(2,10,100,150,'2019-12-30 00:00:00','2019-10-01 00:00:00','no observaciones',2,_binary '',0,100,150,250);

UNLOCK TABLES;

--
-- Table structure for table `contrato_x_tipo_de_activo`
--

DROP TABLE IF EXISTS `contrato_x_tipo_de_activo`;

CREATE TABLE `contrato_x_tipo_de_activo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calificacion_tipo` double DEFAULT NULL,
  `capacidad_mantenimiento` int(11) DEFAULT NULL,
  `capacidad_reparacion` int(11) DEFAULT NULL,
  `tarifa` double DEFAULT NULL,
  `tiempo_estimado_est` int(11) DEFAULT NULL,
  `id_contrato` int(11) DEFAULT NULL,
  `id_tipo_de_activo` int(11) DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL,
  `cantidad_calificado` int(11) DEFAULT NULL,
  `capacidad_mantenimiento_inicial` int(11) DEFAULT NULL,
  `capacidad_reparacion_inicial` int(11) DEFAULT NULL,
  `capacidad_total_inicial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk0h8tpafog41hb4n3kqtc05tg` (`id_contrato`),
  KEY `FKklci6dkwi11tgajp9mceqkk7a` (`id_tipo_de_activo`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contrato_x_tipo_de_activo`
--

LOCK TABLES `contrato_x_tipo_de_activo` WRITE;

INSERT INTO `contrato_x_tipo_de_activo` VALUES (1,10,15,25,3000,10,1,2,_binary '',0,10,15,25),(2,10,10,25,2000,5,1,1,_binary '',0,15,10,25),(3,10,5,15,2000,5,1,3,_binary '',0,5,15,20),(4,10,10,10,5000,2,1,4,_binary '',0,10,10,20),(5,10,20,45,3000,20,2,2,_binary '',0,20,45,65),(6,10,15,25,2000,10,2,1,_binary '',0,15,25,40),(7,10,30,30,2000,5,2,3,_binary '',0,30,30,60),(8,10,35,50,5000,1,2,5,_binary '',0,35,50,85);

UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;

CREATE TABLE `estado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_estado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;

INSERT INTO `estado` VALUES (1,'REGISTRADA'),(2,'ASIGNADA'),(3,'EN ATENCION'),(4,'ATENDIDA'),(5,'RECHAZADA'),(6,'CANCELADA'),(7,'CERRADA');

UNLOCK TABLES;

--
-- Table structure for table `log_de_estados`
--

DROP TABLE IF EXISTS `log_de_estados`;

CREATE TABLE `log_de_estados` (
  `id_log_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado_final` varchar(255) DEFAULT NULL,
  `estado_inicial` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_solicitud` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_log_estado`),
  KEY `FK5nui5q3j743qh2fn3faccp2j4` (`id_solicitud`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `log_de_estados`
--

LOCK TABLES `log_de_estados` WRITE;

UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_persona`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;

UNLOCK TABLES;

--
-- Table structure for table `plan_de_mantenimiento`
--

DROP TABLE IF EXISTS `plan_de_mantenimiento`;

CREATE TABLE `plan_de_mantenimiento` (
  `id_plan_mantenimiento` int(11) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `horas_uso` int(11) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_plan_mantenimiento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;


--
-- Dumping data for table `plan_de_mantenimiento`
--

LOCK TABLES `plan_de_mantenimiento` WRITE;

UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `calificacion` double DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado` bit(1) DEFAULT NULL,
  `nombre_contacto` varchar(255) DEFAULT NULL,
  `razon_social` varchar(255) DEFAULT NULL,
  `ruc` varchar(255) DEFAULT NULL,
  `telefono_contacto` varchar(255) DEFAULT NULL,
  `ubigeo` varchar(255) DEFAULT NULL,
  `bloqueado` bit(1) DEFAULT NULL,
  `cantidad_calificado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_proveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;

INSERT INTO `proveedor` VALUES (1,10,'av. tacna 231',_binary '','Luis Arana','Cuellos Blancos','12345678912','944441181','Lima-Lima',_binary '\0',0),(2,10,'av. tacna 232',_binary '','Karla Pedraza','El matriarcado','12345678913','944441182','Lima-Lima',_binary '\0',0),(3,10,'av. tacna 233',_binary '','Gina Bustamante','Los intocables','12345678914','944441183','Lima-Lima',_binary '\0',0),(4,10,'av. tacna 234',_binary '','Fabricio Monsalve','Los mineros de Sipan','12345678915','944441184','Lima-Lima',_binary '\0',0);

UNLOCK TABLES;

--
-- Table structure for table `solicitud`
--

DROP TABLE IF EXISTS `solicitud`;

CREATE TABLE `solicitud` (
  `id_solicitud` int(11) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `asunto` varchar(255) DEFAULT NULL,
  `calificacion` double DEFAULT NULL,
  `complejidad` int(11) DEFAULT NULL,
  `costo_total` double DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `documento` longblob,
  `prioridad` int(11) DEFAULT NULL,
  `tiempo_respuesta` double DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `id_activo` int(11) DEFAULT NULL,
  `id_estado` int(11) DEFAULT NULL,
  `id_plan_mantenimiento` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_solicitud`),
  KEY `FKiwf1h2q8sru1mlo35m220knro` (`id_activo`),
  KEY `FK8c8blsv5kyyf2x73d2w2sde8s` (`id_estado`),
  KEY `FKm0okjtl6avx393yd26rgd9yai` (`id_plan_mantenimiento`),
  KEY `FKb1q6664y9wksy8xl5ldgua7yr` (`id_proveedor`),
  KEY `FKnm4y7w49isl37c27aq2fmos5t` (`id_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `solicitud`
--

LOCK TABLES `solicitud` WRITE;

UNLOCK TABLES;

--
-- Table structure for table `tipo_de_activo`
--

DROP TABLE IF EXISTS `tipo_de_activo`;

CREATE TABLE `tipo_de_activo` (
  `id_activo` int(11) NOT NULL AUTO_INCREMENT,
  `criticidad` int(11) DEFAULT NULL,
  `nombre_tipo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_activo`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `tipo_de_activo`
--

LOCK TABLES `tipo_de_activo` WRITE;

INSERT INTO `tipo_de_activo` VALUES (1,2,'transporte ejecutivo'),(2,1,'transporte planta'),(3,2,'transporte terceros'),(4,3,'transporte minerales'),(5,4,'explotacion minerales');

UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `admin` bit(1) DEFAULT NULL,
  `apellido_materno` varchar(255) DEFAULT NULL,
  `apellido_paterno` varchar(255) DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `rol` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `FK291boi1xxnn60c6t003e3g8a9` (`id_proveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;

INSERT INTO `usuario` VALUES (1,_binary '',_binary '','Alejo','Chapi','123456','Daniel',1,'admin01',NULL),(2,_binary '',_binary '','Silva','Paucarpoma','123456','Carlos',2,'sup01',NULL),(3,_binary '',_binary '','Martinez','Carrillo','123456','Carlos',4,'op01',NULL),(4,_binary '',_binary '\0',NULL,NULL,'123456',NULL,3,'12345678912',1),(5,_binary '',_binary '\0',NULL,NULL,'123456',NULL,3,'12345678913',2),(6,_binary '',_binary '\0',NULL,NULL,'123456',NULL,3,'12345678914',3),(7,_binary '',_binary '\0',NULL,NULL,'123456',NULL,3,'12345678915',4);

UNLOCK TABLES;

-- Dump completed on 2019-11-26 19:07:40