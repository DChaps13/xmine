import React, { Component } from 'react';
import axios from '../../context/axios';
import { Button, Modal, Rating, Form } from 'semantic-ui-react';


export default class ModalCerrarSolicitud extends Component {
    // props: idActual, open, handleClose
    constructor(props) {
        super(props);
        this.state = {
            comentario: '',
            rank: 5,
        }
    }

    cerrarSolicitud = () => {
        axios({
            method: 'post',
            url: '/solicitud/calificar',
            data: {
                id: this.props.idActual,
                calif: this.state.rank,
                comment: this.state.rank.toString() + '/10'
            }
        }).then((response) => {
            if (response.status === 200) {
                axios({
                    method: 'post',
                    url: '/solicitud/cambiarEstado',
                    data: {
                        idSoli: this.props.idActual,
                        idEstado: 7,
                        comment: this.state.comentario
                    }
                }).then((response) => {
                    if (response.status === 200) {
                        this.setState({ comentario: '', rank: 5 })
                        this.props.handleClose();
                    }
                });
            }
        })
    }

    render() {
        const { closeOnEscape, closeOnDimmerClick } = this.state;
        return (
            <Modal size={"tiny"} open={this.props.open}
                closeOnEscape={closeOnEscape}
                closeOnDimmerClick={closeOnDimmerClick}
                onClose={() => this.props.handleClose()}>
                <Modal.Header>{'Cerrar solicitud Nro: ' + this.props.idActual}</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Group inline>
                            <label style={{ width: '15%', minWidth: 100, alignSelf: 'start' }}>Calificación:</label>
                            <Rating maxRating={10} clearable
                                icon='heart'
                                size='huge'
                                rating={this.state.rank}
                                onRate={(event, data) => { this.setState({ rank: data.rating }) }} />
                        </Form.Group>
                        <Form.Group inline>
                            <label style={{ width: '15%', minWidth: 100, alignSelf: 'start' }}>Comentario:</label>
                            <div style={{ width: '80%' }}>
                                <textarea
                                    value={this.state.comentario}
                                    onChange={(event) => this.setState({ comentario: event.target.value })}
                                />
                            </div>
                        </Form.Group>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={() => this.props.handleClose()} negative>
                        Cancelar</Button>
                    <Button positive
                        onClick={() => this.cerrarSolicitud()}
                        labelPosition='right'
                        icon='checkmark'
                        content='Confirmar'
                    />
                </Modal.Actions>
            </Modal>
        )
    }
}
