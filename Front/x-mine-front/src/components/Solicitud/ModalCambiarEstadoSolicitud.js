import React, { Component } from 'react';
import axios from '../../context/axios';
import { Button, Modal, Form } from 'semantic-ui-react';


export default class ModalCambiarEstadoSolicitud extends Component {
    // props: idActual, open, handleClose
    //        idEstado, titulo, mensaje
    constructor(props) {
        super(props);
        this.state = {
            comentario: ''
        }
    }

    cambiarEstadoSolicitud = () => {
        console.log({
            idSoli: this.props.idActual,
            idEstado: this.props.idEstado,
            comment: this.state.comentario
        })
        axios({
            method: 'post',
            url: '/solicitud/cambiarEstado',
            data: {
                idSoli: this.props.idActual,
                idEstado: this.props.idEstado,
                comment: this.state.comentario
            }
        }).then((response) => {
            if (response.status === 200){
                this.setState({comentario: ''})
                this.props.handleClose();
            }
        });
    }

    render() {
        const { closeOnEscape, closeOnDimmerClick } = this.state;
        return (
            <Modal size={"tiny"} open={this.props.open}
                closeOnEscape={closeOnEscape}
                closeOnDimmerClick={closeOnDimmerClick}
                onClose={() => this.props.handleClose()}>
                <Modal.Header>{ this.props.titulo + ' Nro: ' + this.props.idActual}</Modal.Header>
                <Modal.Content>
                    <Form>
                        <label>{this.props.mensaje}</label>
                        <br/><br/>
                        <Form.Group inline>
                            <label style={{ width: '15%', minWidth: 100, alignSelf: 'start' }}>Comentario:</label>
                            <div style={{ width: '80%' }}>
                                <textarea
                                    value={this.state.comentario}
                                    onChange={(event) => this.setState({ comentario: event.target.value })}
                                />
                            </div>
                        </Form.Group>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={() => this.props.handleClose()} negative>
                        Cancelar</Button>
                    <Button positive
                        onClick={() => this.cambiarEstadoSolicitud()}
                        labelPosition='right'
                        icon='checkmark'
                        content='Confirmar'
                    />
                </Modal.Actions>
            </Modal>
        )
    }
}
