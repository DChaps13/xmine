import _ from 'lodash'
import React, { Component } from 'react';
import { Button, Modal, Form, Search, Label } from 'semantic-ui-react';
import { ModalContext } from '../../context/providers/ModalContextProvider';
import axios from '../../context/axios';

const initialAct = {
    isLoadingAct: false,
    resultsAct: [],
    valueAct: ''
}

const initialProv = {
    isLoadingProv: false,
    resultsProv: [],
    valueProv: ''
}

const priority = [
    { key: '1', text: 'Muy Bajo', value: 1 },
    { key: '2', text: 'Bajo', value: 2 },
    { key: '3', text: 'Medio', value: 3 },
    { key: '4', text: 'Alto', value: 4 },
    { key: '5', text: 'Critico', value: 5 },
]


export default class ModalNuevaSolicitud extends Component {
    static contextType = ModalContext;

    // Handles de Search de activos
    handleResultSelectAct = (e, { result }) => {
        result.title = result.placa;
        this.setState({
            valueAct: result.title, idActivo: result.id, selectedAct: {
                placa: result.placa, marca: result.marca, modelo: result.modelo
            }
        })
    }

    handleSearchChangeAct = (e, { value }) => {
        this.setState({ isLoadingAct: true, valueAct: value })

        setTimeout(() => {
            if (this.state.valueAct.length < 1) return this.setState(initialAct)
            //console.dir(this.state);
            const re = new RegExp(_.escapeRegExp(this.state.valueAct), 'i')
            const isMatch = (result) => re.test(result.placa);

            this.setState({
                isLoadingAct: false,
                resultsAct: _.filter(this.state.activos, isMatch),
            })
        }, 300)
    }


    // Handles de Search de Proveedores
    handleResultSelectProv = (e, { result }) => {
        //console.dir(result);
        result.title = result.razon_social;
        this.setState({
            valueProv: result.title, idProv: result.id, nombProveedor: result.razon_social,
            selectedProv: {
                razon_social: result.razon_social, ruc: result.ruc
            }
        })
    }

    handleSearchChangeProv = (e, { value }) => {
        this.setState({ isLoadingProv: true, valueProv: value })

        setTimeout(() => {
            if (this.state.valueProv.length < 1) return this.setState(initialProv)
            //console.dir(this.state);
            const re = new RegExp(_.escapeRegExp(this.state.valueProv), 'i')
            const isMatch = (result) => re.test(result.razon_social);

            this.setState({
                isLoadingProv: false,
                resultsProv: _.filter(this.state.proveedores, isMatch),
            })
        }, 300)
    }

    // Constructor
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            id: null,
            idActivo: '',
            idProv: '',
            titulo: '',
            nombProveedor: '',
            tipo: null,
            prioridad: null,
            descripcion: '',
            ...initialAct,
            ...initialProv,
            selectedAct: {
                placa: '-',
                marca: '-',
                modelo: '-'
            },
            selectedProv: {
                razon_social: '-',
                ruc: '-'
            },
            validation: {
                DUPLICATE: false,
                NO_PLACA: false,
                NO_PRIORIDAD: false
            }
        }

        axios({
            method: 'post',
            url: '/activo/modelosLibres'
        })
            .then((response) => {
                //console.log(response.data);
                let aux = response.data.map(act => {
                    return { ...act, title: act.placa, description: act.marca + ' ' + act.modelo }
                });
                this.setState({ activos: aux })
            });

        axios({
            method: 'post',
            url: '/proveedor/lista'
        })
            .then((response) => {
                //console.log(response.data);
                let aux = response.data.map(prov => {
                    return { ...prov, title: prov.razon_social, description: prov.ruc }
                });
                this.setState({ proveedores: aux })

            });
    }

    funVerificar = () => {
        this.setState(({ checked }) => ({ checked: !checked }))
        //console.log(this.state.checked)
    }

    // Envio de solicitud
    confirmarSolicitud = () => {
        this.setState({
            validation: {
                ...this.state.validation,
                DUPLICATE: false,
                NO_PLACA: false,
                NO_PRIORIDAD: false
            }
        });

        this.setState({
            validation: {
                ...this.state.validation,
                NO_PLACA: this.state.idActivo === '',
                NO_PRIORIDAD: this.state.prioridad === null
            }
        });

        if (this.state.idActivo === '' || this.state.prioridad === null) {
            return;
        }
        let params = {
            tipo: 2,
            activo_movil: {
                id: this.state.idActivo,
            },
            usuario: {
                id: 1
            },
            asunto: "",
            descripcion: this.state.descripcion,
            prioridad: this.state.prioridad
        }
        if (this.state.checked && this.state.nombProveedor !== '') {
            params.proveedor = { id: this.state.idProv };
        }
        //console.log(params);
        //console.log(this.state.validation);
        axios({
            method: 'post',
            url: '/solicitud/crear',
            data: params
        })
            .then((response) => {
                console.log(response);

                if (response.status === 200) {
                    if (response.data) {
                        this.close()
                        window.location.replace('/solicitud');
                    } else {
                        this.setState({ validation: { ...this.state.validation, DUPLICATE: true } });

                    }
                }
            }).catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
    }


    // Cerrar el modal
    close = () => {
        this.context.toggleModal('NONE');
    }

    render() {
        const { isLoadingAct, isLoadingProv, valueAct, resultsAct, valueProv, resultsProv } = this.state
        return (
            <Modal size={"large"} open={true} onClose={this.close}>
                <Modal.Header>Nueva Solicitud de Reparación</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Group inline>
                            <label style={styles.label}>Activos*</label>
                            <Search fluid label='Activo *'
                                loading={isLoadingAct}
                                onResultSelect={this.handleResultSelectAct}
                                onSearchChange={_.debounce(this.handleSearchChangeAct, 500, {
                                    leading: true,
                                })}
                                results={resultsAct}
                                value={valueAct}
                            />
                            {this.state.validation.DUPLICATE &&
                                <Label basic color='red' pointing='left'>
                                    Este activo ya tiene una solicitud pendiente
                                </Label>
                            }
                            {this.state.validation.NO_PLACA &&
                                <Label basic color='red' pointing='left'>
                                    Campo obligatorio
                                </Label>
                            }
                            <div style={styles.selectedDiv}>
                                <b>Placa: </b>
                                {this.state.selectedAct.placa}
                            </div>
                            <div style={styles.selectedDiv}>
                                <b>Marca: </b>
                                {this.state.selectedAct.marca}
                            </div>
                            <div style={styles.selectedDiv}>
                                <b>Modelo: </b>
                                {this.state.selectedAct.modelo}
                            </div>
                        </Form.Group>
                        <Form.Group inline>
                            <label style={styles.label}>Asignación</label>
                            <Form.Radio checked={this.state.checked}
                                fluid label='Automatico/Manual' toggle onChange={() => this.funVerificar()} />
                        </Form.Group>

                        <Form.Group inline>
                            <label style={styles.label}>Proveedores*</label>
                            <Search disabled={!this.state.checked}
                                fluid label='Proveedor *'
                                loading={isLoadingProv}
                                onResultSelect={this.handleResultSelectProv}
                                onSearchChange={_.debounce(this.handleSearchChangeProv, 500, {
                                    leading: true,
                                })}
                                results={resultsProv}
                                value={valueProv}
                            />
                            <div style={styles.selectedDiv}>
                                <b>Razón Social: </b>
                                {this.state.selectedProv.razon_social}
                            </div>
                            <div style={styles.selectedDiv}>
                                <b>RUC: </b>
                                {this.state.selectedProv.ruc}
                            </div>
                        </Form.Group>

                        <Form.Group inline>
                            <label style={styles.label}>Prioridad*</label>
                            <Form.Select
                                options={priority}
                                placeholder='Prioridad'
                                value={priority.value}
                                onChange={(event, data) => this.setState({ prioridad: data.value })}
                            />
                            {this.state.validation.NO_PRIORIDAD &&
                                <Label basic color='red' pointing='left'>
                                    Campo obligatorio
                                </Label>
                            }
                        </Form.Group>

                        <Form.Group inline>
                            <label style={{ ...styles.label, alignSelf: 'start' }}>Descripcion*</label>
                            <div style={{ width: '90%' }}>
                                <textarea maxLength={255}
                                    placeholder='Detalle la incidencia...'
                                    onChange={(event) => this.setState({ descripcion: event.target.value })}
                                />
                            </div>
                        </Form.Group>

                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={this.close}>Cancelar</Button>
                    <Button
                        positive
                        icon='checkmark'
                        labelPosition='right'
                        content='Confirmar'
                        onClick={this.confirmarSolicitud}
                    />
                </Modal.Actions>
            </Modal>
        )
    }
}

const styles = {
    label: {
        width: '10%',
        textAlign: 'right'
    },
    selectedDiv: {
        marginRight: 10,
        marginLeft: 10,
        width: '20%'
    }
}