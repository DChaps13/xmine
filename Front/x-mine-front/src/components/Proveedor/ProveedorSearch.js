import React, { Component } from 'react'
import _ from 'lodash';
import { Search } from 'semantic-ui-react';
import axios from '../../context/axios';

const initialState = {
    isLoading: false,
    results: [],
    valueSelected: '',
    resultSelected: null
}

export default class ProveedorSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ...initialState
        }
        axios({
            method: 'post',
            url: '/proveedor/lista'
        }).then((response) => {
            let aux = response.data.map(prov => {
                return { ...prov, title: prov.razon_social, description: prov.ruc, key:prov.id }
            });
            this.setState({ proveedores: aux })
        });
    }

    handleSearchChange = (e, { value }) => {
        this.setState({ isLoading: true, valueSelected: value })

        setTimeout(() => {
            if (this.state.valueSelected.length < 1) return this.setState(initialState)
            const re = new RegExp(_.escapeRegExp(this.state.valueSelected), 'i')
            const isMatch = (result) => re.test(result.razon_social);

            this.setState({
                isLoading: false,
                results: _.filter(this.state.proveedores, isMatch),
            })
        }, 300)
    }

    handleResultSelect = (e, { result }) => {
        result.title = result.razon_social;
        this.setState({valueSelected: result.title,resultSelected: result});
        this.props.handleSelect(result);
    }

    render() {
        const { isLoading, valueSelected, results } = this.state;
        return (
            <Search
                fluid label='Proveedor *'
                loading={isLoading}
                onResultSelect={this.handleResultSelect}
                onSearchChange={_.debounce(this.handleSearchChange, 500, {
                    leading: true,
                })}
                results={results}
                value={valueSelected}
            />
        )
    }
}
