import React, { Component } from 'react';
import { Button, Message, Form, Grid, Loader, Dimmer, Modal } from 'semantic-ui-react';
import axios from '../../context/axios';
import { withRouter, Link } from "react-router-dom";
import ProveedorSearch from '../../components/Proveedor/ProveedorSearch';


const priority = [
    { key: '1', text: 'Muy Bajo', value: 1 },
    { key: '2', text: 'Bajo', value: 2 },
    { key: '3', text: 'Medio', value: 3 },
    { key: '4', text: 'Alto', value: 4 },
    { key: '5', text: 'Critico', value: 5 },
]

class ModificarSolicitudView extends Component {
    // Constructor
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            isSaveDone: false,
            open: false,
            selectedProv: null,
            solicitud: {
                id: 0,
                placa_activo: '',
                marca: '',
                modelo: '',
                prioridad: 1,
                descripcion: '',
                estado: {
                    id: 0,
                    nombre: ''
                },
                razon_social: '',
                ruc: '',
            }
        }

        if (this.props.match.params.idSolicitud) {
            this.refresh();
        } else {
            window.location.replace('/solicitud');
        }

    }

    refresh = () => {
        axios({
            method: 'post',
            url: '/solicitud/obtenerSolicitud',
            data: {
                id: this.props.match.params.idSolicitud
            }
        }).then((response) => {
            this.setState({ solicitud: response.data })
            console.log(this.state);
            this.setState({ isLoading: false })
        }).catch((error) => {
            window.location.replace('/solicitud');
            console.log(error.message);
            this.setState({ isLoading: false });
        });
    }

    modificarSolicitud = () => {
        this.setState({ isLoading: true });

        let _data = {
            id: this.state.solicitud.id,
            prioridad: this.state.solicitud.prioridad,
            descripcion: this.state.solicitud.descripcion,
            tipo: 2,
            activo_movil: {
                id: this.state.solicitud.id_activo,
            },
            usuario: {
                id: this.state.solicitud.id_usuario,
            },
            asunto: this.state.solicitud.asunto,
            estado: {
                id: this.state.solicitud.estado.id
            }
        }

        if (this.state.solicitud.id_proveedor) {
            _data = {
                ..._data,
                proveedor: {
                    id: this.state.solicitud.id_proveedor
                }
            }
        }

        axios({
            method: 'post',
            url: '/solicitud/actualizar',
            data: _data
        }).then((response) => {
            console.log(response.data);
            if (response.status === 200) {
                this.setState({ isSaveDone: true, open: false });
                this.refresh();
            }
            this.setState({ isLoading: false });
        }).catch((error) => {
            console.log(error.message);
            this.setState({ isLoading: false });
        });
    }

    componentDidMount() {
        document.title = "Modificar solicitud";
    }

    handleSelect = (result) => {
        this.setState({ selectedProv: result });
        console.log(result);
    }

    asignarProv = () => {
        if (this.state.selectedProv) {
            axios({
                method: 'post',
                url: '/solicitud/asignarProveedor',
                data: {
                    id_solicitud: this.state.solicitud.id,
                    id_proveedor: this.state.selectedProv.id
                }
            }).then((response) => {
                if (response.status === 200) {
                    this.setState({ isSaveDone: true, open: false });
                    this.refresh();
                }
                this.setState({ isLoading: false });
            }).catch((error) => {
                console.log(error.message);
                this.setState({ isLoading: false });
            });
        }
    }

    render() {
        return (
            <div style={{ padding: 50 }}>

                <Grid columns={2}
                    style={{ marginBottom: 10, borderBottomWidth: 1, borderBottomStyle: 'solid', borderBottomColor: 'rgb(229,229,229)' }}>
                    <Grid.Column>
                        <h1>{'Modificar solicitud Nro: ' + this.state.solicitud.id}</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button as={Link} to='/solicitud'>Regresar a solicitudes</Button>
                    </Grid.Column>
                </Grid>

                {this.state.isSaveDone && <Message success
                    style={{ marginTop: 0, marginBottom: 0 }}
                    header='Los cambios fueron guardados con éxito'
                />}
                <br />
                <Form>

                    <Form.Group inline>
                        <label style={styles.label}>Estado:</label>
                        <b style={{ color: this.state.solicitud.estado.id === 7 ? 'black' : this.state.solicitud.estado.id > 4 ? 'red' : 'green' }}>
                            {this.state.solicitud.estado.nombre}
                        </b>
                    </Form.Group>

                    <Form.Group inline unstackable>
                        <label style={styles.label}>Activo:</label>
                        <div style={styles.selectedDiv}>
                            <b>Placa: </b>
                            {this.state.solicitud.placa_activo}
                        </div>
                        <div style={styles.selectedDiv}>
                            <b>Marca: </b>
                            {this.state.solicitud.marca}
                        </div>
                        <div style={styles.selectedDiv}>
                            <b>Modelo: </b>
                            {this.state.solicitud.modelo}
                        </div>
                    </Form.Group>

                    <Form.Group inline>
                        <label style={styles.label}>Proveedor:</label>
                        <div style={styles.selectedDiv}>
                            <b>RUC: </b>
                            {this.state.solicitud.ruc}
                        </div>
                        <div style={styles.selectedDiv}>
                            <b>Razón Social: </b>
                            {this.state.solicitud.razon_social}
                        </div>

                        {this.state.solicitud.estado.id === 1 &&
                            <Button primary onClick={() => this.setState({ open: true })}>
                                Asignar proveedor
                            </Button>
                        }
                    </Form.Group>

                    <Form.Group inline>
                        <label style={styles.label}>Prioridad:</label>
                        <Form.Select
                            disabled={this.state.solicitud.estado.id >= 6}
                            options={priority}
                            value={this.state.solicitud.prioridad}
                            onChange={(event, data) => this.setState({
                                solicitud: { ...this.state.solicitud, prioridad: data.value }
                            })}
                        />
                    </Form.Group>

                    <Form.Group inline>
                        <label style={{ ...styles.label, alignSelf: 'start' }}>Descripcion:</label>
                        <div style={{ width: '80%' }}>
                            <textarea
                                disabled={this.state.solicitud.estado.id >= 6}
                                value={this.state.solicitud.descripcion}
                                onChange={(event) => this.setState({
                                    solicitud: { ...this.state.solicitud, descripcion: event.target.value }
                                })}
                            />
                        </div>
                    </Form.Group>
                </Form>

                <Grid columns={1}>
                    <Grid.Column textAlign="center">
                        <Button color='green' disabled={this.state.solicitud.estado.id == 6 || this.state.solicitud.estado.id == 7} onClick={() => this.modificarSolicitud()}>Guardar cambios</Button>
                        <Button as={Link} to='/solicitud'>Cancelar</Button>
                    </Grid.Column>
                </Grid>

                <Dimmer active={this.state.isLoading}>
                    <Loader size='massive' active={this.state.isLoading} />
                </Dimmer>

                <Modal size={"mini"} open={this.state.open}
                    onClose={() => this.setState({ open: false })}>
                    <Modal.Header>Asignar Proveedor</Modal.Header>
                    <Modal.Content>
                        <b>Buscar proveedor:</b>
                        <ProveedorSearch handleSelect={this.handleSelect} />
                    </Modal.Content>
                    <Modal.Actions>
                        <Button negative
                            onClick={() => this.setState({ open: false })} >
                            Cancelar
                        </Button>
                        <Button positive
                            onClick={() => this.asignarProv()}
                            labelPosition='right'
                            icon='checkmark'
                            content='Asignar'
                        />
                    </Modal.Actions>
                </Modal>
            </div>
        )
    }
}


const styles = {
    label: {
        width: '15%',
        minWidth: 100,
        textAlign: 'left',
        fontSize: 18,
    },
    selectedDiv: {
        marginRight: '3%',
        width: '25%',
        minWidth: 100,
    }
}

export default withRouter(ModificarSolicitudView);