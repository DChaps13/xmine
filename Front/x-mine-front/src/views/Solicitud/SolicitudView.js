import React, { Component, Fragment } from 'react';
import { Table, Pagination } from 'semantic-ui-react';
import { Grid, Button, Icon, Dropdown, Menu, Input, Modal, Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import 'react-dropzone-uploader/dist/styles.css'
import Dropzone from 'react-dropzone-uploader'
import { ModalContext } from '../../context/providers/ModalContextProvider';
import ModalCerrarSolicitud from '../../components/Solicitud/ModalCerrarSolicitud';
import ModalCambiarEstadoSolicitud from '../../components/Solicitud/ModalCambiarEstadoSolicitud';
import axios from '../../context/axios';
import Swal from 'sweetalert2';

// 6: Cerrada, 5: Cancelada,  4: Rechazada, 3: En atención, 2: Asignada, 1: Registrada

const estados = [
    { key: '0', text: 'Todos los estados', value: 0 },
    { key: '1', text: 'Registrada', value: 1 },
    { key: '2', text: 'Asignada', value: 2 },
    { key: '3', text: 'En atención', value: 3 },
    { key: '4', text: 'Atendida', value: 4 },
    { key: '5', text: 'Rechazada', value: 5 },
    { key: '6', text: 'Cancelada', value: 6 },
    { key: '7', text: 'Cerrada', value: 7 },
]

const priority = [
    { key: '0', text: 'Todas las prioridades', value: 0 },
    { key: '1', text: 'Muy Bajo', value: 1 },
    { key: '2', text: 'Bajo', value: 2 },
    { key: '3', text: 'Medio', value: 3 },
    { key: '4', text: 'Alto', value: 4 },
    { key: '5', text: 'Critico', value: 5 },
]

const tipos = [
    { key: '0', text: 'Todos los tipos', value: 0 },
    { key: '1', text: 'Mantenimiento', value: 1 },
    { key: '2', text: 'Reparación', value: 2 }
]

class SolicitudView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoadingAlgorithm: false,
            // Filter
            solicitudes: [],
            resultSolicitudes: [],
            estadoFilter: 0,
            prioridadFilter: 0,
            tipoFilter: 0,
            searchFilter: '',
            // Table
            perPage: 8,
            totalPages: 1,
            activePage: 1,
            //Modal
            openCerrarSolModal: false,
            open: false,
            idActual: 0,
            tituloModal: '',
            mensajeModal: '',
            toEstado: 0,
            openCargar: false,
            showErrorFileType: false
        }
        this.refresh();
    }

    refresh = () => {
        axios({
            method: 'post',
            url: '/solicitud/lista'
        }).then((response) => {
            let aux = response.data;
            let myProv = this.props.cookies.get("usuario").proveedor;
            if (myProv && this.props.cookies.get("usuario").rol === 3) {
                aux = aux.filter(p =>
                    p.id_proveedor === myProv.id);
            }
            this.setState({ solicitudes: aux });
        }).then(() => {
            this.setState({
                totalPages:
                    Math.ceil(this.state.solicitudes.length / this.state.perPage)
            });
        }).catch((error) => {
            console.log('Error', error.message);
        });
    }


    fnAsignar = () => {
        this.setState({ isLoadingAlgorithm: true });
        axios({
            method: 'post',
            url: '/solicitud/asignacionR'
        }).then((response) => {
            axios({
                method: 'post',
                url: '/solicitud/asignacionM'
            }).then((response) => {
                this.refresh();
                console.log(response);
                this.setState({ isLoadingAlgorithm: false });
            })
        }).catch((error) => {
            console.log('Error', error.message);
            this.setState({ isLoadingAlgorithm: false });
        });
    }

    componentDidMount() { document.title = "Solicitudes"; }

    makeFilter = () => {
        let aux = this.state.solicitudes;
        if (this.state.estadoFilter !== 0) {
            aux = aux.filter(s => s.estado.id == this.state.estadoFilter);
        }
        if (this.state.prioridadFilter !== 0) {
            aux = aux.filter(s => s.prioridad == this.state.prioridadFilter);
        }
        if (this.state.tipoFilter !== 0) {
            aux = aux.filter(s => s.tipo == this.state.tipoFilter);
        }
        let lowercasedFilter = this.state.searchFilter.toLowerCase();
        aux = aux.filter(item => {
            return Object.keys(item).some(key => {
                if (key === 'placa_activo' || key === 'marca' || key === 'modelo' || key === 'razon_social') {
                    if (item[key]) {
                        return item[key].toString().toLowerCase().includes(lowercasedFilter);
                    }
                }
                return false;
            })
        });
        return aux;
    }

    pageContent = (pageNumber) => {
        let offset = (pageNumber - 1) * this.state.perPage;

        let aux = this.makeFilter();

        return aux.slice(offset, offset + this.state.perPage)
            .map(p => <Table.Row>
                <Table.Cell>{p.id}</Table.Cell>
                <Table.Cell>{p.placa_activo}</Table.Cell>
                <Table.Cell>{p.marca + ' - ' + p.modelo}</Table.Cell>
                <Table.Cell>{p.razon_social === null ? "Por Asignar" : p.razon_social}</Table.Cell>
                <Table.Cell>{p.tipo === 2 ? "Reparación" : "Mantenimiento"}</Table.Cell>
                <Table.Cell>{p.prioridad === 1 ? "Muy Bajo" : p.prioridad === 2 ? "Bajo" : p.prioridad === 3 ? "Medio" : p.prioridad === 4 ? "Alto" : "Critico"}</Table.Cell>
                <Table.Cell>
                    <div style={{ color: p.estado.id === 7 ? 'black' : p.estado.id > 4 ? 'red' : 'green' }}>
                        {p.estado.nombre}
                    </div>
                </Table.Cell>
                <Table.Cell style={{ textAlign: 'center' }}>

                    {this.props.cookies.get("usuario").rol !== 3 && //Supervisor
                        <Fragment>
                            {(p.estado.id === 4 || p.estado.id === 5) &&
                                <Button color='black' circular icon='check' title="Cerrar solicitud"
                                    onClick={() => {
                                        this.setState({ openCerrarSolModal: true, idActual: p.id });
                                    }} />
                            }
                            {(p.estado.id === 4) &&
                                <Button color='green' circular icon='retweet' title="Volver a atención"
                                    onClick={() => this.showModalConfirm(p.id,
                                        'Confirmar devolución de atención de la solicitud',
                                        'Incluir un comentario', 3)} />
                            }
                            <Button circular icon='pencil' title="Ver/modificar solicitud"
                                as={Link} to={'/solicitud/modificar/' + p.id}></Button>
                            {p.estado.id < 3 &&
                                <Button negative circular icon='x' title="Cancelar solicitud"
                                    onClick={() => this.showModalConfirm(p.id,
                                        'Confirmar cancelación de la solicitud',
                                        'Incluir un comentario', 6)} />
                            }
                        </Fragment>}

                    {(this.props.cookies.get("usuario").rol === 3 && //Provider
                        (p.estado.id === 2 || p.estado.id === 3)) &&
                        <Fragment>
                            <Button color='green' circular icon='check' title="Proceder atención"
                                onClick={() => this.showModalConfirm(p.id,
                                    (p.estado.id === 3 ? 'Finalizar atención de la' : 'Aceptar') + ' solicitud',
                                    'Ingrese un mensaje indicando información adicional', p.estado.id + 1)} />
                            <Button negative circular icon='x' title="Rechazar solicitud"
                                onClick={() => this.showModalConfirm(p.id,
                                    'Confirmar rechazo de la solicitud',
                                    'Ingrese un mensaje explicando las razones del rechazo', 5)} />
                        </Fragment>}
                </Table.Cell>
            </Table.Row>)
    }

    handlePaginationChange = (e, { activePage }) => this.setState({ activePage: activePage });

    cambiarEstadoSolicitud = (toEstado, comentario) => {
        axios({
            method: 'post',
            url: '/solicitud/cambiarEstado',
            data: {
                idSoli: this.state.idActual,
                idEstado: toEstado,
                comment: comentario
            }
        }).then((response) => {
            if (response.status === 200) this.refresh();
        })
    }

    showModalConfirm = (_id, _tituloModal, _mensajeModal, _toEstado) => {
        this.setState({
            idActual: _id, open: true,
            tituloModal: _tituloModal,
            mensajeModal: _mensajeModal,
            toEstado: _toEstado
        });
    }
    closeModalConfirm = () => this.setState({ open: false })

    openCargaModal = () => this.setState({ openCargar: true })
    closeCargar = () => this.setState({ openCargar: false })

    handleChangeStatus = ({ meta, file }, status) => {
        this.setState({ showErrorFileType: status == 'rejected_file_type', showErrorMaxFiles: status == 'rejected_max_files' })
    }

    handleSubmit = (files, allFiles) => {
        const data = new FormData();
        data.append('file', allFiles[0].file);
        data.append('id_usuario', this.props.cookies.get("usuario").id)

        axios({
            method: 'post',
            url: '/cargaMasiva/cargaSolicitudes',
            data: data,
            headers: {
                'Content-Type': undefined
            },
        }).then((response) => {
            this.setState({ openCargar: false }, () => {
                Swal.fire({
                    title: 'Éxito',
                    text: 'Carga masiva realizada correctamente',
                    icon: 'success',
                    timer: 1500
                });
                this.refresh();
            });
        }).catch((error) => {
            console.dir(error);
        });
    }

    descargarPlantilla = () => {
        axios({
            method: 'post',
            url: ''
        }).then((response) => {
            console.dir(response);
        }).catch((error) => {
            console.dir(error.message);
        });
    }

    render() {
        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}>
                    <Grid.Column>
                        <h1>Solicitudes</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        {this.props.cookies.get("usuario").rol !== 3 &&
                            <Fragment>
                                <ModalContext.Consumer>{(modalContext) => {
                                    return (
                                        <Button style={{ marginHorizontal: 20 }} color='green' onClick={() => modalContext.toggleModal('NEW_SOLICITUD')}>
                                            <Icon name="plus" /> Nueva Solicitud
                                        </Button>
                                    )
                                }
                                }
                                </ModalContext.Consumer>
                                <Button loading={this.state.isLoadingAlgorithm}
                                    disabled={this.state.isLoadingAlgorithm}
                                    color='teal'
                                    onClick={() => this.fnAsignar()}>
                                    <Icon name="wrench" />Asignar Proveedores</Button>
                                <Button
                                    color='blue'
                                    onClick={() => this.openCargaModal()}>
                                    <Icon name="file excel" /> Carga Masiva
                                </Button>
                            </Fragment>
                        }
                    </Grid.Column>
                </Grid>

                <Menu stackable>
                    <Dropdown item options={estados}
                        value={this.state.estadoFilter}
                        onChange={(event, data) =>
                            this.setState({ estadoFilter: data.value, activePage: 1 })} />
                    <Dropdown item options={priority}
                        value={this.state.prioridadFilter}
                        onChange={(event, data) =>
                            this.setState({ prioridadFilter: data.value, activePage: 1 })} />
                    <Dropdown item options={tipos}
                        value={this.state.tipoFilter}
                        onChange={(event, data) =>
                            this.setState({ tipoFilter: data.value, activePage: 1 })} />
                    <Menu.Item
                        onClick={() => this.setState({
                            estadoFilter: 0,
                            prioridadFilter: 0,
                            tipoFilter: 0,
                            searchFilter: ''
                        })}>
                        <Icon name='x' />
                        Limpiar filtros
                    </Menu.Item>
                    <Menu.Menu position='right'>
                        <Menu.Item>
                            <Input transparent
                                icon={{ name: 'search', link: true }}
                                placeholder='Buscar solicitud...'
                                value={this.state.searchFilter}
                                onChange={(event) => this.setState({ searchFilter: event.target.value })} />
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>

                <Table celled style={{ width: '100%' }}>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>PLACA</Table.HeaderCell>
                            <Table.HeaderCell>ACTIVO</Table.HeaderCell>
                            <Table.HeaderCell>PROVEEDOR</Table.HeaderCell>
                            <Table.HeaderCell>TIPO</Table.HeaderCell>
                            <Table.HeaderCell>PRIORIDAD</Table.HeaderCell>
                            <Table.HeaderCell>ESTADO</Table.HeaderCell>
                            <Table.HeaderCell>ACCION</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.pageContent(this.state.activePage)}
                    </Table.Body>

                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='8' style={{ textAlign: 'right' }}>
                                <Pagination
                                    defaultActivePage={this.state.activePage}
                                    activePage={this.state.activePage}
                                    onPageChange={this.handlePaginationChange}
                                    totalPages={Math.ceil(this.makeFilter().length / this.state.perPage)} />
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>

                <ModalCambiarEstadoSolicitud
                    idActual={this.state.idActual}
                    open={this.state.open}
                    handleClose={() => {
                        this.setState({ open: false });
                        this.refresh();
                    }}
                    idEstado={this.state.toEstado}
                    titulo={this.state.tituloModal}
                    mensaje={this.state.mensajeModal}
                />
                <ModalCerrarSolicitud
                    idActual={this.state.idActual}
                    open={this.state.openCerrarSolModal}
                    handleClose={() => {
                        this.setState({ openCerrarSolModal: false });
                        this.refresh();
                    }} />
                <Modal size={"large"} open={this.state.openCargar} closeOnDimmerClick={false} onClose={this.closeCargar}>
                    <Modal.Header>Carga Masiva de Solicitudes</Modal.Header>
                    <Modal.Content>
                        <Message negative hidden={!this.state.showErrorFileType}>
                            <Message.Header>Debe seleccionar un archivo tipo Excel</Message.Header>
                        </Message>
                        <Message negative hidden={!this.state.showErrorMaxFiles}>
                            <Message.Header>Solo se puede subir un archivo</Message.Header>
                        </Message>
                        <Dropzone
                            onChangeStatus={this.handleChangeStatus}
                            onSubmit={this.handleSubmit}
                            maxFiles={1}
                            inputContent={'Arrastrar y soltar archivos o Click para buscar (solo archivos Excel)'}
                            inputWithFilesContent={'Agregar archivos'}
                            submitButtonContent={'Subir'}
                            accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        />
                    </Modal.Content>
                    <Modal.Actions>
                        <Button onClick={this.closeCargar}>Cerrar</Button>
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}

export default withCookies(SolicitudView);