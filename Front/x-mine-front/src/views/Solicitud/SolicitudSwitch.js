import React, { Component } from 'react';

import {
    Switch,
    Route,
    withRouter
} from "react-router-dom";

import SolicitudView from './SolicitudView';
import ModificarSolicitudView from './ModificarSolicitudView';

class SolicitudSwitch extends Component {
    render() {
        let match = this.props.match;
        return (
            <Switch>
                <Route exact path={match.path}>
                    <SolicitudView />
                </Route>
                <Route path={match.path + '/modificar/:idSolicitud'}>
                    <ModificarSolicitudView />
                </Route>
            </Switch>
        );
    }
}

export default withRouter(SolicitudSwitch);