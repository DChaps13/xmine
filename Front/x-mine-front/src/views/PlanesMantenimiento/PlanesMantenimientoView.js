import _ from 'lodash'
import React, { Component } from 'react';
import { Table, Grid, Button, Modal, Form, Pagination, Search, Label } from 'semantic-ui-react';

import axios from '../../context/axios';

const initialProv = {
    isLoadingProv: false,
    resultsProv: [],
    valueProv: ''
}

class PlanesManteminientoView extends Component {

    handleResultSelectProv = (e, { result }) => {
        console.dir(result);
        result.title = result.modelo;
        this.setState({
            valueProv: result.title,
            selectedProv: {
                modelo: result.modelo,
            }
        })
    }

    handleSearchChangeProv = (e, { value }) => {
        this.setState({ isLoadingProv: true, valueProv: value })

        setTimeout(() => {
            if (this.state.valueProv.length < 1) return this.setState(initialProv)

            const re = new RegExp(_.escapeRegExp(this.state.valueProv), 'i')
            const isMatch = (result) => re.test(result.modelo);

            this.setState({
                isLoadingProv: false,
                resultsProv: _.filter(this.state.modelos, isMatch),
            })
        }, 300)
    }

    constructor(props) {
        super(props);
        this.state = {
            planes: [],
            ...initialProv,
            selectedProv: {
                modelo: null,
            },
            planActual: null,
            idActual: null,
            openCrear: false,
            openEditar: false,
            openEliminar: false,
            perPage: 8,
            totalPages: 1,
            activePage: 1,
            modelos: [],
            descripcion: '',
            frecuencia: null,
            validation: {
                MODELO_VACIO: false,
                HORAS_VACIO: false,
                HORAS_MAL: false,
                DESCRIPCION_VACIO: false
            }
        }

        axios({
            method: 'post',
            url: '/planM/lista'
        }).then((response) => {
            this.setState({ planes: response.data })
            console.log(response.data);
        }).then(() => {
            this.setState({
                totalPages:
                    Math.ceil(this.state.planes.length / this.state.perPage)
            });
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });

        axios({
            method: 'post',
            url: '/activo/modelos'
        }).then((response) => {
            let aux = []
            for (let i = 0; i < response.data.length; i++) {
                aux.push({ modelo: null });
                aux[i].modelo = response.data[i];
            }
            let aux2 = aux.map(prov => {
                return { ...prov, title: prov.modelo }
            });
            this.setState({ modelos: aux2 })
            console.log(response.data);
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });

    }

    componentDidMount() {
        document.title = "Planes de Mantenimiento";
    }

    pageContent = (pageNumber) => {
        let offset = (pageNumber - 1) * this.state.perPage;
        return this.state.planes.slice(offset, offset + this.state.perPage)
            .map(p => <Table.Row>
                <Table.Cell>{p.id}</Table.Cell>
                <Table.Cell>{p.modelo}</Table.Cell>
                <Table.Cell>{p.horas_uso}</Table.Cell>
                <Table.Cell>{p.descripcion}</Table.Cell>
                <Table.Cell style={{ textAlign: 'center' }}>
                    <Button circular icon='pencil' onClick={() => this.showModalEditar(p)}></Button>
                    <Button negative circular icon='x' onClick={() => this.showModalEliminar(p.id)}></Button>
                </Table.Cell>
            </Table.Row>)
    }

    handlePaginationChange = (e, { activePage }) => this.setState({ activePage: activePage });

    showModalEliminar = (id) => this.setState({ idActual: id, openEliminar: true });
    showModalEditar = (p) => {
        this.setState({ planActual: p, openEditar: true, valueProv: String(p.modelo) })
        if (this.state.planActual.horas_uso != null) {
            this.setState({ frecuencia: this.state.planActual.horas_uso });
        }
        if (this.state.planActual.descripcion != null) {
            this.setState({ frecuencia: this.state.planActual.frecuencia, descripcion: this.state.planActual.descripcion })
        }
    }
    showModalCrear = () => this.setState({ openCrear: true, planActual: null });
    closeCrear = () => this.setState({ openCrear: false })
    closeEditar = () => this.setState({ openEditar: false })
    closeEliminar = () => this.setState({ openEliminar: false })

    /*  editRazon = (event, data) => {
         this.state.provActual.razon_social = data.value;
         this.setState({ razon_social: data.value });
     } */

    crearPlan = () => {
        this.setState({
            validation: {
                MODELO_VACIO: false,
                HORAS_VACIO: false,
                HORAS_MAL: false,
                DESCRIPCION_VACIO: false
            }
        }, () => {
            if (this.state.selectedProv.modelo === null || this.state.selectedProv.modelo === "") {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.selectedProv.modelo))) {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (this.state.frecuencia === null || this.state.frecuencia === "") {
                this.setState({ validation: { ...this.state.validation, HORAS_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.frecuencia)) || isNaN(this.state.frecuencia) || this.state.frecuencia.includes("-") || this.state.frecuencia.includes(".")) {
                this.setState({ validation: { ...this.state.validation, HORAS_MAL: true } });
                return;
            }

            if (this.state.descripcion === null || this.state.descripcion === "") {
                this.setState({ validation: { ...this.state.validation, DESCRIPCION_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.descripcion))) {
                this.setState({ validation: { ...this.state.validation, DESCRIPCION_VACIO: true } });
                return;
            }

            let params = {
                horas_uso: this.state.frecuencia,
                activo: true,
                modelo: this.state.selectedProv.modelo,
                descripcion: this.state.descripcion,
            }

            axios({
                method: 'post',
                url: '/planM/crear',
                data: params,
            }).then((response) => {
                console.log(response.data);
                window.location.replace('/planes') //cambiar esto, que solo vuelva a listar los planes
            }).catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
        })
    }

    editarPlan = () => {
        this.setState({
            validation: {
                MODELO_VACIO: false,
                HORAS_VACIO: false,
                HORAS_MAL: false,
                DESCRIPCION_VACIO: false
            }
        }, () => {
            if (this.state.valueProv === null || this.state.valueProv === "") {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.valueProv))) {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (this.state.frecuencia === null || this.state.frecuencia === "") {
                this.setState({ validation: { ...this.state.validation, HORAS_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.frecuencia)) || isNaN(this.state.frecuencia) || this.state.frecuencia.includes("-") || this.state.frecuencia.includes(".")) {
                this.setState({ validation: { ...this.state.validation, HORAS_MAL: true } });
                return;
            }

            if (this.state.descripcion === null || this.state.descripcion === "") {
                this.setState({ validation: { ...this.state.validation, DESCRIPCION_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.descripcion))) {
                this.setState({ validation: { ...this.state.validation, DESCRIPCION_VACIO: true } });
                return;
            }

            let params = {
                id: this.state.planActual.id,
                horas_uso: this.state.frecuencia,
                activo: true,
                modelo: this.state.valueProv,
                descripcion: this.state.descripcion,
            }

            axios({
                method: 'post',
                url: '/planM/actualizar',
                data: params,
            }).then((response) => {
                console.log(response.data);
            }).catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
            window.location.replace('/planes')
        })
    }

    borrarPlan = () => {
        let params = {
            id: this.state.idActual,
        }
        console.log(params)
        axios({
            method: 'post',
            url: '/planM/borrar',
            data: params,
        })
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            })
            ;
        window.location.replace('/planes')
    }

    editFrec = (event, data) => {
        console.dir(data)
        this.setState({ planActual: { ...this.state.planActual, horas_uso: data.value }, frecuencia: data.value });
        //this.setState({ frecuencia: data.value });
    }

    editDesc = (event, data) => {
        console.log(data)
        this.setState({ planActual: { ...this.state.planActual, descripcion: data.value }, descripcion: data.value });
        //this.setState({ descripcion: data.value });

    }

    render() {

        const { isLoadingProv, valueProv, resultsProv } = this.state
        const { activePage } = this.state.activePage;
        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}>
                    <Grid.Column>
                        <h1>Planes de Mantenimiento</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button color='green' onClick={() => this.showModalCrear()}>Registrar nuevo plan</Button>
                    </Grid.Column>
                </Grid>
                <div style={{ paddingTop: 20 }}>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>ID</Table.HeaderCell>
                                <Table.HeaderCell>MODELO</Table.HeaderCell>
                                <Table.HeaderCell>FRECUENCIA HORAS</Table.HeaderCell>
                                <Table.HeaderCell>DESCRIPCIÓN</Table.HeaderCell>
                                <Table.HeaderCell>Acción</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.pageContent(this.state.activePage)}
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='5' style={{ textAlign: 'right' }}>
                                    <Pagination
                                        defaultActivePage={this.state.activePage}
                                        activePage={activePage}
                                        onPageChange={this.handlePaginationChange}
                                        totalPages={this.state.totalPages}
                                    />
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>

                    <Modal size={"small"} open={this.state.openCrear} closeOnDimmerClick={false} onClose={this.closeCrear}>
                        <Modal.Header>Registrar Nuevo Plan de Mantenimiento</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group inline>
                                    <div>
                                        <label required style={styles.label}>Modelo</label>
                                        <Search
                                            fluid label='Modelo*'
                                            loading={isLoadingProv}
                                            onResultSelect={this.handleResultSelectProv}
                                            onSearchChange={_.debounce(this.handleSearchChangeProv, 100, {
                                                loading: true,
                                            })}
                                            results={resultsProv}
                                            value={valueProv}
                                        />
                                        {this.state.validation.MODELO_VACIO && <Label basic color='red' pointing='top'>
                                            Campo obligatorio
                                        </Label>}
                                    </div>
                                    <div style={{ marginLeft: 45 }}></div>
                                    <Form.Input required fluid label='Frecuencia de mantenimiento (Hrs)*'
                                        placeholder='Hrs de uso de activo'
                                        onChange={(event) => this.setState({ frecuencia: event.target.value })}
                                        error={(this.state.validation.HORAS_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.HORAS_MAL && { content: 'Debe ser un número natural' })}
                                        maxLength={11}
                                    />

                                </Form.Group>
                                <Form.Group inline>
                                    <label style={{ ...styles.label, alignSelf: 'start' }}>Descripción*</label>
                                    <div style={{ width: '90%' }}>
                                        <Form.TextArea
                                            placeholder='Coloque lo que se realizará en el mantenimiento'
                                            onChange={(event) => this.setState({ descripcion: event.target.value })}
                                            error={(this.state.validation.DESCRIPCION_VACIO && { content: 'Campo obligatorio' })}
                                            maxLength={255}
                                        />
                                    </div>
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeCrear}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.crearPlan}
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"small"} open={this.state.openEditar} closeOnDimmerClick={false} onClose={this.closeEditar}>
                        <Modal.Header>Editar Plan de Mantenimiento</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group inline>
                                    <div>
                                        <label style={styles.label}>Modelo*</label>
                                        <Search
                                            fluid label='Modelo*'
                                            loading={isLoadingProv}
                                            onResultSelect={this.handleResultSelectProv}
                                            onSearchChange={_.debounce(this.handleSearchChangeProv, 500, {
                                                loading: true,
                                            })}
                                            results={resultsProv}
                                            value={valueProv}
                                        />
                                        {this.state.validation.MODELO_VACIO && <Label basic color='red' pointing='top'>
                                            Campo obligatorio
                                        </Label>}
                                    </div>
                                    <div style={{ marginLeft: 45 }}></div>
                                    <Form.Input required fluid label='Frecuencia de mantenimiento (Hrs)*'
                                        placeholder='Hrs de uso de activo'
                                        value={this.state.planActual == null ? null : this.state.planActual.horas_uso}
                                        onChange={(event, data) => this.editFrec(event, data)}
                                        error={(this.state.validation.HORAS_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.HORAS_MAL && { content: 'Debe ser un número natural' })}
                                        maxLength={11}
                                    />
                                </Form.Group>
                                <Form.Group inline>
                                    <label required style={{ ...styles.label, alignSelf: 'start' }}>Descripción</label>
                                    <div style={{ width: '90%' }}>
                                        <Form.TextArea
                                            placeholder='Coloque lo que se realizará en el mantenimiento'
                                            value={this.state.planActual == null ? null : this.state.planActual.descripcion}
                                            onChange={(event, data) => this.editDesc(event, data)}
                                            error={(this.state.validation.DESCRIPCION_VACIO && { content: 'Campo obligatorio' })}
                                            maxLength={255}
                                        />
                                    </div>
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeEditar}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.editarPlan}
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"mini"}
                        open={this.state.openEliminar}
                        closeOnEscape={true}
                        closeOnDimmerClick={false}
                        onClose={this.closeEliminar}>
                        <Modal.Header>Confirmación de Eliminación</Modal.Header>
                        <Modal.Content>
                            <p>¿Seguro que quiere eliminar este plan?</p>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button onClick={this.closeEliminar} negative>Cancelar</Button>
                            <Button
                                onClick={this.borrarPlan}
                                positive
                                labelPosition='right'
                                icon='checkmark'
                                content='Confirmar'
                            />
                        </Modal.Actions>
                    </Modal>
                </div>
            </div>
        );
    }
}

const styles = {
    label: {
        width: '10%',
        textAlign: 'right'
    },
    selectedDiv: {
        marginRight: 10,
        marginLeft: 10,
        width: '20%'
    }
}

export default PlanesManteminientoView;