import React, { Component } from 'react';
import { Button, Image, Transition } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
        }
    }

    componentDidMount() {
        this.setState({ visible: true });
    }

    render() {
        let imgUrl = 'https://oldschoolgrappling.com/wp-content/uploads/2018/08/Background-opera-speeddials-community-web-simple-backgrounds.jpg';
        return (
            <div>
                <div
                    style={{
                        height: '90vh',
                        width: '100vw',
                        backgroundImage: 'url(' + imgUrl + ')',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',
                        position: 'absolute',
                        opacity: 0.5,
                        zIndex: -1
                    }}>

                </div>
                <Transition visible={this.state.visible} animation='fade up' duration={2000}>
                    <div style={{ textAlign: 'center', paddingTop: '10vh' }}>
                        <Image size="medium" as={Link} to='/' src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Logo_of_X_%28company%29.svg/1200px-Logo_of_X_%28company%29.svg.png' />
                        <h1>¡Bienvenido a XMine!</h1>
                        <Button as={Link} to='/solicitud' color='green'>Ir a solicitudes</Button>
                    </div>
                </Transition >
            </div>


        )
    }
}
