import React, { Component } from 'react'
import CanvasJSReact from '../../assets/canvasjs.react';
import { Menu, Transition, Label, Grid, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from '../../context/axios';

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const dataMonth = [
    { value: 11, label: 'Noviembre 2019' },
    { value: 12, label: 'Diciembre 2019' },
]

export default class ReporteCapacidadView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            visible: false,
            response: [],
            activeItem: 0,
            consumedData: [],
            noConsumedData: [],
            tipoSol: {
                repNCons: 0,
                repCons: 0,
                mantNCons: 0,
                mantCons: 0
            },
            selectedMonth: 11
        }

        this.refresh(this.state.selectedMonth);
    }

    refresh = (month) => {
        axios({
            method: 'post',
            url: '/reporte/capacidadDeUso',
            data: {
                mes: month
            }
        }).then((response) => {
            if (response.status === 200) {
                this.setState({ response: response.data });
            }
        })
    }

    handleMonthClick = (e, { name }) => {
        this.setState({ selectedMonth: name });
        this.refresh(name);
    }

    componentDidMount() {
        this.setState({ visible: true });
        document.title = "Reporte de capacidad";
    }

    handleItemClick = (e, { name }) => {
        this.setState({ activeItem: name });
        let aux = this.state.response.filter(p => p.proveedor.id === name);

        let auxTipoSol = {
            repNCons: 0,
            repCons: 0,
            mantNCons: 0,
            mantCons: 0
        };

        let cons = aux[0].lstTipoActivo.map(a => {
            auxTipoSol.repCons += a.tipo_solicitud_repa_usado;
            auxTipoSol.repNCons += a.tipo_solicitud_repa_cap - a.tipo_solicitud_repa_usado;
            auxTipoSol.mantCons += a.tipo_solicitud_mant_usado;
            auxTipoSol.mantNCons += a.tipo_solicitud_mant_cap - a.tipo_solicitud_mant_usado;
            return {
                label: a.tpActivoModel.nombre_tipo,
                y: (a.tipo_solicitud_mant_cap + a.tipo_solicitud_repa_cap) * (a.porcentaje_usado_x_tipo)
            }
        });

        let nocons = aux[0].lstTipoActivo.map(a => {
            return {
                label: a.tpActivoModel.nombre_tipo,
                y: (a.tipo_solicitud_mant_cap + a.tipo_solicitud_repa_cap) * (1 - a.porcentaje_usado_x_tipo)
            }
        });

        this.setState({ consumedData: cons, noConsumedData: nocons, tipoSol: auxTipoSol });
        console.log(this.state.consumedData)
        console.log(this.state.noConsumedData)
    }

    render() {
        const options = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2",
            data: [
                {
                    type: "stackedColumn",
                    name: "Consumido",
                    showInLegend: true,
                    dataPoints: this.state.consumedData
                },
                {
                    type: "stackedColumn",
                    name: "No consumido",
                    showInLegend: true,
                    dataPoints: this.state.noConsumedData
                },
            ]
        }

        const options2 = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2",
            data: [
                {
                    type: "stackedColumn",
                    name: "Consumido",
                    showInLegend: true,
                    dataPoints: [
                        { label: 'Mantenimiento', y: this.state.tipoSol.mantCons },
                        { label: 'Reparación', y: this.state.tipoSol.repCons }
                    ]
                },
                {
                    type: "stackedColumn",
                    name: "No consumido",
                    showInLegend: true,
                    dataPoints: [
                        { label: 'Mantenimiento', y: this.state.tipoSol.mantNCons },
                        { label: 'Reparación', y: this.state.tipoSol.repNCons }
                    ]
                },
            ]
        }

        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}
                    style={{ marginBottom: 10, borderBottomWidth: 1, borderBottomStyle: 'solid', borderBottomColor: 'rgb(229,229,229)' }}>
                    <Grid.Column>
                        <h1>{'Capacidad por proveedor'}</h1>
                    </Grid.Column>
                    <Grid.Column textAlign='right'>
                        <Button as={Link} to='/dashboard'>Regresar</Button>
                    </Grid.Column>
                </Grid>


                <Grid>
                    <Grid.Column style={{ width: '20%' }}>
                        <h4>Mes:</h4>
                        <Menu vertical fluid>
                            {dataMonth.map(p =>
                                <Menu.Item name={p.value}
                                    active={this.state.selectedMonth === p.value}
                                    onClick={this.handleMonthClick}>
                                    {p.label}
                                </Menu.Item>
                            )}
                        </Menu>
                        <h4>Proveedor:</h4>
                        <Menu vertical fluid>
                            {this.state.response.map(p =>
                                <Menu.Item name={p.proveedor.id}
                                    active={this.state.activeItem === p.proveedor.id}
                                    onClick={this.handleItemClick}>
                                    {p.proveedor.razon_social}
                                </Menu.Item>
                            )}
                        </Menu>
                    </Grid.Column>
                    <Grid.Column style={{ width: '80%' }}>
                        <div style={{ paddingRight: 50, paddingLeft: 50, paddingBottom: 50, paddingTop: 20 }}>
                            <h3>Por Tipo de Vehiculo:</h3>
                            <Transition visible={this.state.visible} animation='browse' duration={750}>
                                <CanvasJSChart options={options} />
                            </Transition>
                        </div>
                        <div style={{ paddingRight: 50, paddingLeft: 50, paddingBottom: 50, paddingTop: 20 }}>
                            <h3>Por Tipo de Mantenimiento:</h3>
                            <Transition visible={this.state.visible} animation='browse' duration={750}>
                                <CanvasJSChart options={options2} />
                            </Transition>
                        </div>
                    </Grid.Column>
                </Grid>



            </div>
        );
    }
}
