import React, { Component } from 'react'
import CanvasJSReact from '../../assets/canvasjs.react';
import { Transition, Grid, Button, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from '../../context/axios';

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const dataMonth = [
    { value: 11, label: 'Noviembre 2019' },
    { value: 12, label: 'Diciembre 2019' },
]

export default class ReporteCapacidadProvsView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            visible: false,
            response: [],
            activeItem: 0,
            consumedData: [],
            noConsumedData: [],
            selectedMonth: 11
        }

        this.refresh(this.state.selectedMonth);
    }

    refresh = (month) => {
        axios({
            method: 'post',
            url: '/reporte/capacidadDeUso',
            data: {
                mes: month
            }
        }).then((response) => {
            if (response.status === 200) {
                this.setState({ response: response.data });

                let cons = response.data.map(p => {
                    let aux = 0;
                    p.lstTipoActivo.map(a => {
                        aux += a.tipo_solicitud_repa_usado + a.tipo_solicitud_mant_usado
                    })
                    return {
                        label: p.proveedor.razon_social,
                        y: aux
                    }
                });

                let nocons = response.data.map(p => {
                    let aux = 0;
                    p.lstTipoActivo.map(a => {
                        aux += a.tipo_solicitud_repa_cap - a.tipo_solicitud_repa_usado + a.tipo_solicitud_mant_cap - a.tipo_solicitud_mant_usado;
                    })
                    return {
                        label: p.proveedor.razon_social,
                        y: aux
                    }
                });

                this.setState({ consumedData: cons, noConsumedData: nocons });

            }
        })
    }

    handleMonthClick = (e, { name }) => {
        this.setState({ selectedMonth: name });
        this.refresh(name);
    }

    componentDidMount() {
        this.setState({ visible: true });
        document.title = "Reporte de capacidad";
    }

    render() {
        const options = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2",
            data: [
                {
                    type: "stackedColumn",
                    name: "Consumido",
                    showInLegend: true,
                    dataPoints: this.state.consumedData
                },
                {
                    type: "stackedColumn",
                    name: "No consumido",
                    showInLegend: true,
                    dataPoints: this.state.noConsumedData
                },
            ]
        }

        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}
                    style={{ marginBottom: 10, borderBottomWidth: 1, borderBottomStyle: 'solid', borderBottomColor: 'rgb(229,229,229)' }}>
                    <Grid.Column>
                        <h1>{'Capacidad de los proveedores'}</h1>
                    </Grid.Column>
                    <Grid.Column textAlign='right'>
                        <Button as={Link} to='/dashboard'>Regresar</Button>
                    </Grid.Column>
                </Grid>


                <Grid>
                    <Grid.Column style={{ width: '20%' }}>
                        <h4>Mes:</h4>
                        <Menu vertical fluid>
                            {dataMonth.map(p =>
                                <Menu.Item name={p.value}
                                    active={this.state.selectedMonth === p.value}
                                    onClick={this.handleMonthClick}>
                                    {p.label}
                                </Menu.Item>
                            )}
                        </Menu>
                    </Grid.Column>
                    <Grid.Column style={{ width: '80%' }}>
                        <div style={{ paddingRight: 50, paddingLeft: 50, paddingBottom: 50, paddingTop: 20 }}>
                            <Transition visible={this.state.visible} animation='browse' duration={750}>
                                <CanvasJSChart options={options} />
                            </Transition>
                        </div>
                    </Grid.Column>
                </Grid>

            </div>
        );
    }
}
