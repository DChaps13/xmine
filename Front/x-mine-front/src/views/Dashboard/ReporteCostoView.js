import React, { Component } from 'react'
import CanvasJSReact from '../../assets/canvasjs.react';
import { Card, Transition, Menu, Grid, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from '../../context/axios';


var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const dataMonth = [
    { value: 11, label: 'Noviembre 2019' },
    { value: 12, label: 'Diciembre 2019' },
]

export default class ReporteCostoView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            visible: false,
            response: [],
            activeItem: 0,
            perProv: [],
            perTipo: [],
            selectedMonth: 11
        }
        this.refresh(this.state.selectedMonth);
    }

    refresh = (month) => {
        axios({
            method: 'post',
            url: '/reporte/costoProveedores',
            data: {
                mes: month
            }
        }).then((response) => {
            if (response.status === 200) {
                this.setState({ response: response.data });
                let aux = response.data.map(p => {
                    return {
                        label: p.razon_social,
                        y: p.costoActual
                    }
                });
                this.setState({ perProv: aux });
            }
        })

        axios({
            method: 'post',
            url: '/reporte/costoTipoActivos',
            data: {
                mes: month
            }
        }).then((response) => {
            if (response.status === 200) {
                this.setState({ response: response.data });
                let aux = response.data.map(p => {
                    return {
                        label: p.nombre_tipo,
                        y: p.costoActual
                    }
                });
                this.setState({ perTipo: aux });
            }
        })
    }

    componentDidMount() {
        this.setState({ visible: true });
        document.title = "Reporte de costos";
    }

    handleMonthClick = (e, { name }) => {
        this.setState({ selectedMonth: name });
        this.refresh(name);
    }

    render() {
        const options = {
            animationEnabled: true,
            exportEnabled: false,
            theme: "light1",
            data: [{
                type: "column",
                dataPoints: this.state.perProv
            }]
        }

        const options2 = {
            animationEnabled: true,
            exportEnabled: false,
            theme: "light1",
            data: [{
                type: "column",
                dataPoints: this.state.perTipo
            }]
        }

        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}
                    style={{ marginBottom: 10, borderBottomWidth: 1, borderBottomStyle: 'solid', borderBottomColor: 'rgb(229,229,229)' }}>
                    <Grid.Column>
                        <h1>{'Costos por proveedor'}</h1>
                    </Grid.Column>
                    <Grid.Column textAlign='right'>
                        <Button as={Link} to='/dashboard'>Regresar</Button>
                    </Grid.Column>
                </Grid>

                <Grid>
                    <Grid.Column style={{ width: '20%' }}>
                        <h4>Mes:</h4>
                        <Menu vertical fluid>
                            {dataMonth.map(p =>
                                <Menu.Item name={p.value}
                                    active={this.state.selectedMonth === p.value}
                                    onClick={this.handleMonthClick}>
                                    {p.label}
                                </Menu.Item>
                            )}
                        </Menu>
                    </Grid.Column>
                    <Grid.Column style={{ width: '80%' }}>
                        <div style={{ paddingRight: 50, paddingLeft: 50, paddingBottom: 50, paddingTop: 20 }}>
                            <Transition visible={this.state.visible} animation='browse' duration={750}>
                                <CanvasJSChart options={options} />
                            </Transition>
                        </div>
                    </Grid.Column>
                </Grid>

                <Grid columns={1}
                    style={{ marginBottom: 10, borderBottomWidth: 1, borderBottomStyle: 'solid', borderBottomColor: 'rgb(229,229,229)' }}>
                    <Grid.Column>
                        <h1>{'Costos por tipo de vehículo'}</h1>
                    </Grid.Column>
                </Grid>

                <Grid>
                    <Grid.Column style={{ width: '20%' }}>
                        <h4>Mes:</h4>
                        <Menu vertical fluid>
                            {dataMonth.map(p =>
                                <Menu.Item name={p.value}
                                    active={this.state.selectedMonth === p.value}
                                    onClick={this.handleMonthClick}>
                                    {p.label}
                                </Menu.Item>
                            )}
                        </Menu>
                    </Grid.Column>
                    <Grid.Column style={{ width: '80%' }}>
                        <div style={{ paddingRight: 50, paddingLeft: 50, paddingBottom: 50, paddingTop: 20 }}>
                            <Transition visible={this.state.visible} animation='browse' duration={750}>
                                <CanvasJSChart options={options2} />
                            </Transition>
                        </div>
                    </Grid.Column>
                </Grid>

            </div>
        );
    }
}
