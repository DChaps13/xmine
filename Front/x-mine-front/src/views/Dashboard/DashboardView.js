import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
import { Card, Transition, Label, Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class DashboardView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            proveedores: []
        }
    }

    componentDidMount() {
        this.setState({ visible: true });
        document.title = "Dashboard";
    }

    render() {
        const options = {
            interactivityEnabled: false,
            exportEnabled: false,
            theme: "light1",
            data: [{
                type: "column",
                dataPoints: [
                    { label: "", y: 30 },
                    { label: "", y: 25 },
                    { label: "", y: 20 },
                    { label: "", y: 15 },
                    { label: "", y: 10 }
                ]
            }],
            height: 240
        }

        const options2 = {
            interactivityEnabled: false,
            exportEnabled: false,
            theme: "light2",
            data: [
                {
                    type: "stackedColumn",
                    name: "Consumido",
                    showInLegend: true,
                    yValueFormatString: "#,###k",
                    dataPoints: [
                        { label: "A", y: 14 },
                        { label: "B", y: 12 },
                        { label: "C", y: 18 },
                    ]
                },
                {
                    type: "stackedColumn",
                    name: "No consumido",
                    showInLegend: true,
                    yValueFormatString: "#,###k",
                    dataPoints: [
                        { label: "A", y: 14 },
                        { label: "B", y: 12 },
                        { label: "C", y: 18 },
                    ]
                },
            ],
            height: 240
        }

        const options3 = {
            interactivityEnabled: false,
            exportEnabled: false,
            theme: "light2",
            data: [
                {
                    type: "stackedColumn",
                    name: "Consumido",
                    showInLegend: true,
                    yValueFormatString: "#,###k",
                    dataPoints: [
                        { label: "Prov A", y: 14 },
                        { label: "Prov B", y: 12 },
                        { label: "Prov C", y: 11 },
                        { label: "Prov D", y: 10 },
                        { label: "Prov E", y: 9 },
                        { label: "Prov F", y: 7 },
                        { label: "Prov G", y: 6 },
                    ]
                },
                {
                    type: "stackedColumn",
                    name: "No consumido",
                    showInLegend: true,
                    yValueFormatString: "#,###k",
                    dataPoints: [
                        { label: "Prov A", y: 11 },
                        { label: "Prov B", y: 9 },
                        { label: "Prov C", y: 7 },
                        { label: "Prov D", y: 6 },
                        { label: "Prov E", y: 5 },
                        { label: "Prov F", y: 3 },
                        { label: "Prov G", y: 1 },
                    ]
                },
            ],
            height: 240
        }

        return (
            <div style={{ padding: 50 }}>
                <Grid columns={1}
                    style={{ marginBottom: 20, borderBottomWidth: 1, borderBottomStyle: 'solid', borderBottomColor: 'rgb(229,229,229)' }}>
                    <Grid.Column>
                        <h1>{'Reportes disponibles'}</h1>
                    </Grid.Column>
                </Grid>

                <Card.Group itemsPerRow={3}>
                    <Transition visible={this.state.visible} animation='browse' duration={750}>
                        <Card as={Link} to='/reporteCostos'>
                            <Card.Content>
                                <Card.Header>Reporte de costos por proveedor y tipo de vehículo</Card.Header>
                            </Card.Content>
                            <Card.Content>
                                <CanvasJSChart options={options} />
                            </Card.Content>
                        </Card>
                    </Transition>

                    <Transition visible={this.state.visible} animation='browse' duration={750}>
                        <Card as={Link} to='/reporteCapacidad'>
                            <Card.Content>
                                <Card.Header>Reporte de uso de capacidad usada por proveedor</Card.Header>
                            </Card.Content>
                            <Card.Content>
                                <CanvasJSChart options={options2} />
                            </Card.Content>
                        </Card>
                    </Transition>

                    <Transition visible={this.state.visible} animation='browse' duration={750}>
                        <Card as={Link} to='/reporteCapacidadProvs'>
                            <Card.Content>
                                <Card.Header>Reporte de uso de capacidad usada entre proveedores</Card.Header>
                            </Card.Content>
                            <Card.Content>
                                <CanvasJSChart options={options3} />
                            </Card.Content>
                        </Card>
                    </Transition>

                </Card.Group>
            </div>
        );
    }
}
