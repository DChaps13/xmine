import React, { Component } from 'react';
import { Table, Grid, Button, Modal, Form, Pagination } from 'semantic-ui-react';

import axios from '../../context/axios';

const tipos_usuario = [
    { key: '1', text: 'Administrador', value: 1 },
    { key: '2', text: 'Supervisor', value: 2 },
    { key: '4', text: 'Operario', value: 4 },
]


class UsuariosView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usuarios: [],
            perPage: 8,
            totalPages: 1,
            activePage: 1,
            openCrear: false,
            openEditar: false,
            openEliminar: false,
            userActual: null,
            admin: null,
            apellido_materno: null,
            apellido_paterno: null,
            nombre: null,
            rol: null,
            username: null,
            contrasena: null,
            repetir_contrasena: null,
            validation: {
                USERNAME_VACIO: false,
                ROL_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            }
        }

        axios({
            method: 'post',
            url: '/usuario/lista'
        }).then((response) => {
            this.setState({ usuarios: response.data })
        }).then(() => {
            this.setState({
                totalPages:
                    Math.ceil(this.state.usuarios.length / this.state.perPage)
            });
        });
    }

    componentDidMount() {
        document.title = "usuarios";
    }

    pageContent = (pageNumber) => {
        let offset = (pageNumber - 1) * this.state.perPage;
        return this.state.usuarios.slice(offset, offset + this.state.perPage)
            .map(u => <Table.Row>
                <Table.Cell>{u.id}</Table.Cell>
                <Table.Cell>{(u.nombre === null ? "-" : u.nombre)}</Table.Cell>
                <Table.Cell>{(u.apellido_paterno === null ? "-" : u.apellido_paterno)}</Table.Cell>
                <Table.Cell>{(u.apellido_materno === null ? "-" : u.apellido_materno)}</Table.Cell>
                <Table.Cell>{u.username}</Table.Cell>
                <Table.Cell>{u.rol === 1 ? "Administrador" : u.rol === 2 ? "Supervisor" : u.rol === 3 ? "Proveedor" : "Operario"}</Table.Cell>
                <Table.Cell style={{ textAlign: 'center' }}>
                    <span title={u.rol === 3 ? "Un usuario de rol proveedor se elimina a la vez que el proveedor" : "Eliminar usuario"}> <Button disabled={u.rol === 3} negative circular icon='trash alternate outline'
                        onClick={() => this.showModalEliminar(u.id)}></Button></span>

                </Table.Cell>
            </Table.Row>)
    }

    handlePaginationChange = (e, { activePage }) => this.setState({ activePage: activePage });

    showModalEliminar = (id) => this.setState({ idActual: id, openEliminar: true });

    showModalCrear = () => {
        this.setState({
            openCrear: true,
            userActual: null,
            admin: null,
            apellido_materno: null,
            apellido_paterno: null,
            nombre: null,
            rol: null,
            username: null,
            contrasena: null,
            repetir_contrasena: null,
            validation: {
                USERNAME_VACIO: false,
                ROL_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            },
        });
    }

    closeCrear = () => this.setState({ openCrear: false })
    closeEliminar = () => this.setState({ openEliminar: false })

    crearUsuario = () => {
        this.setState({
            validation: {
                USERNAME_VACIO: false,
                ROL_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            }
        }, () => {
            if (this.state.username === null || this.state.username === "") {
                this.setState({ validation: { ...this.state.validation, USERNAME_VACIO: true } });
                return;
            }

            if (this.state.rol === null || this.state.rol === "") {
                this.setState({ validation: { ...this.state.validation, ROL_VACIO: true } });
                return;
            }

            if (this.state.contrasena === null || this.state.contrasena === "") {
                this.setState({ validation: { ...this.state.validation, CONTRASENA_VACIO: true } });
                return;
            }

            if (this.state.contrasena.length < 5) {
                this.setState({ validation: { ...this.state.validation, CONTRASENA_CORTA: true } });
                return;
            }

            if (this.state.contrasena != this.state.repetir_contrasena) {
                this.setState({ validation: { ...this.state.validation, CONTRASENA_DIFERENTE: true } });
                return;
            }

            let params = {
                admin: 1,
                apellido_materno: this.state.apellido_materno == null ? '' : (this.state.apellido_materno).toUpperCase(),
                apellido_paterno: this.state.apellido_paterno == null ? '' : (this.state.apellido_paterno).toUpperCase(),
                nombre: this.state.nombre == null ? '' : (this.state.nombre).toUpperCase(),
                rol: this.state.rol,
                username: this.state.username,
                contrasena: this.state.contrasena
            }

            axios({
                method: 'post',
                url: '/usuario/crear',
                data: params
            }).then((response) => {
                this.setState({ openCrear: false });
                axios({
                    method: 'post',
                    url: '/usuario/lista'
                }).then((response) => {
                    this.setState({ usuarios: response.data })
                }).then(() => {
                    this.setState({
                        totalPages:
                            Math.ceil(this.state.usuarios.length / this.state.perPage)
                    });
                });
            })
        })
    }

    borrarusuario = () => {
        axios({
            method: 'post',
            url: '/usuario/eliminar',
            data: { idUsuario: this.state.idActual }
        }).then((response) => {
            this.setState({ openEliminar: false });
            axios({
                method: 'post',
                url: '/usuario/lista'
            }).then((response) => {
                this.setState({ usuarios: response.data })
            }).then(() => {
                this.setState({
                    totalPages:
                        Math.ceil(this.state.usuarios.length / this.state.perPage)
                });
            });
        })
    }

    render() {
        const { activePage } = this.state.activePage;
        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}>
                    <Grid.Column>
                        <h1>Usuarios</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button color='green' onClick={() => this.showModalCrear()}>Registrar nuevo usuario</Button>
                    </Grid.Column>
                </Grid>
                <div style={{ paddingTop: 20 }}>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>ID</Table.HeaderCell>
                                <Table.HeaderCell>Nombre</Table.HeaderCell>
                                <Table.HeaderCell>Apellido Paterno</Table.HeaderCell>
                                <Table.HeaderCell>Apellido Materno</Table.HeaderCell>
                                <Table.HeaderCell>Usuario</Table.HeaderCell>
                                <Table.HeaderCell>Rol</Table.HeaderCell>
                                <Table.HeaderCell>Acción</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.pageContent(this.state.activePage)}
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='7' style={{ textAlign: 'right' }}>
                                    <Pagination
                                        defaultActivePage={this.state.activePage}
                                        activePage={activePage}
                                        onPageChange={this.handlePaginationChange}
                                        totalPages={this.state.totalPages}
                                    />
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>

                    <Modal size={"small"} open={this.state.openCrear} closeOnDimmerClick={false} onClose={this.closeCrear}>
                        <Modal.Header>Registrar Nuevo Usuario</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group>
                                    <Form.Input fluid label='Nombre'
                                        placeholder='Gabriel'
                                        onChange={(event, data) => this.setState({ nombre: data.value })}
                                        width={5}
                                        maxLength={255} />
                                    <Form.Input fluid label='Apellido Paterno'
                                        placeholder='García'
                                        onChange={(event, data) => this.setState({ apellido_paterno: data.value })}
                                        width={5}
                                        maxLength={255} />
                                    <Form.Input fluid label='Apellido Materno'
                                        placeholder='Márquez'
                                        onChange={(event, data) => this.setState({ apellido_materno: data.value })}
                                        width={5}
                                        maxLength={255} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Input required fluid label='Nombre de usuario'
                                        placeholder='gmarquez100'
                                        onChange={(event, data) => this.setState({ username: data.value })}
                                        error={this.state.validation.USERNAME_VACIO && { content: 'Campo obligatorio' }}
                                        width={7}
                                        maxLength={255} />
                                    <Form.Select required fluid label='Rol'
                                        options={tipos_usuario}
                                        placeholder='Seleccione un rol'
                                        value={tipos_usuario.value}
                                        onChange={(event, data) => this.setState({ rol: data.value })}
                                        error={this.state.validation.ROL_VACIO && { content: 'Campo obligatorio' }}
                                        width={7} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Input required fluid label='Contraseña'
                                        type='password'
                                        placeholder='Contraseña100'
                                        onChange={(event, data) => this.setState({ contrasena: data.value })}
                                        error={(this.state.validation.CONTRASENA_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.CONTRASENA_CORTA && { content: 'Debe ser de por lo menos 5 caracteres' })}
                                        width={7}
                                        maxLength={255} />
                                    <Form.Input required fluid label='Confirmar contraseña'
                                        type='password'
                                        placeholder='Contraseña100'
                                        onChange={(event, data) => this.setState({ repetir_contrasena: data.value })}
                                        error={this.state.validation.CONTRASENA_DIFERENTE && { content: 'Debe ingresar la misma contraseña' }}
                                        width={7}
                                        maxLength={255} />
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeCrear}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.crearUsuario}
                            />
                        </Modal.Actions>
                    </Modal>
                    <Modal size={"mini"}
                        open={this.state.openEliminar}
                        closeOnEscape={true}
                        closeOnDimmerClick={false}
                        onClose={this.closeEliminar}>
                        <Modal.Header>Confirmación de Eliminación</Modal.Header>
                        <Modal.Content>
                            <p>¿Seguro que quiere eliminar este usuario?</p>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button onClick={this.closeEliminar} negative>Cancelar</Button>
                            <Button
                                onClick={this.borrarusuario}
                                positive
                                labelPosition='right'
                                icon='checkmark'
                                content='Confirmar'
                            />
                        </Modal.Actions>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default UsuariosView;