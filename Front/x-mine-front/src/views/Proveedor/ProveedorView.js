import React, { Component } from 'react';
import { Table, Grid, Button, Modal, Form, Pagination } from 'semantic-ui-react';
import Swal from 'sweetalert2';
import axios from '../../context/axios';

class ProveedorView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            proveedores: [],
            perPage: 8,
            totalPages: 1,
            activePage: 1,
            openCrear: false,
            openEditar: false,
            openEliminar: false,
            provActual: null,
            razon_social: null,
            ruc: null,
            direccion: null,
            ubigeo: null,
            nombre_contacto: null,
            telefono_contacto: null,
            isBlocked: false,
            validation: {
                RAZON_VACIO: false,
                RUC_NAN: false,
                RUC_VACIO: false,
                DIRECCION_VACIO: false,
                UBIGEO_VACIO: false,
                UBIGEO_NAN: false,
                NOMBRE_VACIO: false,
                TELEFONO_VACIO: false,
                TELEFONO_NAN: false
            }

        }


        axios({
            method: 'post',
            url: '/proveedor/lista'
        }).then((response) => {
            console.dir(response.data)
            this.setState({ proveedores: response.data })
        }).then(() => {
            this.setState({
                totalPages:
                    Math.ceil(this.state.proveedores.length / this.state.perPage)
            });
        });
    }

    componentDidMount() {
        document.title = "Proveedores";
    }

    pageContent = (pageNumber) => {
        let offset = (pageNumber - 1) * this.state.perPage;
        return this.state.proveedores.slice(offset, offset + this.state.perPage)
            .map(p => <Table.Row>
                <Table.Cell>{p.id}</Table.Cell>
                <Table.Cell>{p.ruc}</Table.Cell>
                <Table.Cell>{p.razon_social}</Table.Cell>
                <Table.Cell style={{ textAlign: 'center' }}>
                    <Button circular icon='pencil' onClick={() => this.showModalEditar(p)}></Button>
                    <Button negative circular icon='x' onClick={() => this.showModalEliminar(p.id)}></Button>
                </Table.Cell>
            </Table.Row>)
    }

    handlePaginationChange = (e, { activePage }) => this.setState({ activePage: activePage });

    showModalEliminar = (id) => this.setState({ idActual: id, openEliminar: true });

    showModalEditar = (p) => {
        this.setState({
            provActual: { ...p }, openEditar: true, validation: {
                RAZON_VACIO: false,
                RUC_NAN: false,
                RUC_VACIO: false,
                DIRECCION_VACIO: false,
                UBIGEO_VACIO: false,
                UBIGEO_NAN: false,
                NOMBRE_VACIO: false,
                TELEFONO_VACIO: false,
                TELEFONO_NAN: false
            }
        });

        let params = {
            idProveedor: p.id,
        }
        console.log(params)
        axios({
            method: 'post',
            url: '/proveedor/verEstadoBloqueo',
            data: params,
        }).then((response) => {
            this.setState({ isBlocked: response.data })
            console.log(response.data)
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });


    }

    showModalCrear = () => {
        this.setState({
            openCrear: true,
            provActual: null,
            razon_social: null,
            ruc: null,
            direccion: null,
            ubigeo: null,
            nombre_contacto: null,
            telefono_contacto: null,
            validation: {
                RAZON_VACIO: false,
                RUC_NAN: false,
                RUC_VACIO: false,
                DIRECCION_VACIO: false,
                UBIGEO_VACIO: false,
                UBIGEO_NAN: false,
                NOMBRE_VACIO: false,
                TELEFONO_VACIO: false,
                TELEFONO_NAN: false
            },
        });
    }

    closeCrear = () => this.setState({ openCrear: false })
    closeEditar = () => this.setState({ openEditar: false })
    closeEliminar = () => this.setState({ openEliminar: false })

    editRazon = (event, data) => {
        this.setState({ provActual: { ...this.state.provActual, razon_social: data.value } });
        this.setState({ razon_social: data.value });
    }

    editRUC = (event, data) => {
        this.setState({ provActual: { ...this.state.provActual, ruc: data.value } });
        this.setState({ ruc: data.value });
    }

    editDireccion = (event, data) => {
        this.setState({ provActual: { ...this.state.provActual, direccion: data.value } });
        this.setState({ direccion: data.value });
    }

    editUbigeo = (event, data) => {
        this.setState({ provActual: { ...this.state.provActual, ubigeo: data.value } });
        this.setState({ ubigeo: data.value });
    }

    editNombreContacto = (event, data) => {
        this.setState({ provActual: { ...this.state.provActual, nombre_contacto: data.value } });
        this.setState({ nombre_contacto: data.value });
    }

    editTelefonoContacto = (event, data) => {
        this.setState({ provActual: { ...this.state.provActual, telefono_contacto: data.value } });
        this.setState({ telefono_contacto: data.value });
    }

    crearProveedor = () => {
        this.setState({
            validation: {
                RAZON_VACIO: false,
                RUC_NAN: false,
                RUC_VACIO: false,
                DIRECCION_VACIO: false,
                UBIGEO_VACIO: false,
                UBIGEO_NAN: false,
                NOMBRE_VACIO: false,
                TELEFONO_VACIO: false,
                TELEFONO_NAN: false
            }
        }, () => {
            if (this.state.razon_social === null || this.state.razon_social === "") {
                this.setState({ validation: { ...this.state.validation, RAZON_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.razon_social))) {
                this.setState({ validation: { ...this.state.validation, RAZON_VACIO: true } });
                return;
            }

            if (this.state.ruc === null || this.state.ruc === "") {
                this.setState({ validation: { ...this.state.validation, RUC_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.ruc)) || isNaN(this.state.ruc) || this.state.ruc.length !== 11 || this.state.ruc.includes("-") || this.state.ruc.includes(".")) {
                this.setState({ validation: { ...this.state.validation, RUC_NAN: true } });
                return;
            }

            if (this.state.direccion === null || this.state.direccion === "") {
                this.setState({ validation: { ...this.state.validation, DIRECCION_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.direccion))) {
                this.setState({ validation: { ...this.state.validation, DIRECCION_VACIO: true } });
                return;
            }

            if (this.state.nombre_contacto === null || this.state.nombre_contacto === "") {
                this.setState({ validation: { ...this.state.validation, NOMBRE_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.nombre_contacto))) {
                this.setState({ validation: { ...this.state.validation, NOMBRE_VACIO: true } });
                return;
            }

            if (this.state.telefono_contacto === null || this.state.telefono_contacto === "") {
                this.setState({ validation: { ...this.state.validation, TELEFONO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.telefono_contacto)) || isNaN(this.state.telefono_contacto) || this.state.telefono_contacto.length !== 9 || this.state.telefono_contacto.includes("-") || this.state.telefono_contacto.includes(".")) {
                this.setState({ validation: { ...this.state.validation, TELEFONO_NAN: true } });
                return;
            }

            let params = {
                ruc: this.state.ruc,
                nombre_contacto: (this.state.nombre_contacto).toUpperCase(),
                telefono_contacto: this.state.telefono_contacto,
                direccion: (this.state.direccion).toUpperCase(),
                ubigeo: 1,
                razon_social: (this.state.razon_social).toUpperCase()
            }

            axios({
                method: 'post',
                url: '/proveedor/crear',
                data: params
            }).then((response) => {
                this.setState({ openCrear: false }, () => {
                    if (response.data) {
                        Swal.fire({
                            title: 'Éxito',
                            text: 'Proveedor creado',
                            icon: 'success',
                            timer: 2000
                        })
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Ocurrió un error en la creación del proveedor',
                            icon: 'error'
                        })
                    }
                });

                axios({
                    method: 'post',
                    url: '/proveedor/lista'
                }).then((response) => {
                    this.setState({ proveedores: response.data })
                }).then(() => {
                    this.setState({
                        totalPages:
                            Math.ceil(this.state.proveedores.length / this.state.perPage)
                    });
                });
            }).catch((error) => {
                console.log('Error', error.message);
            })
        })
    }

    editarProveedor = () => {
        this.setState({
            validation: {
                RAZON_VACIO: false,
                RUC_NAN: false,
                RUC_VACIO: false,
                DIRECCION_VACIO: false,
                UBIGEO_VACIO: false,
                UBIGEO_NAN: false,
                NOMBRE_VACIO: false,
                TELEFONO_VACIO: false,
                TELEFONO_NAN: false
            }
        }, () => {
            if (this.state.provActual.razon_social === null || this.state.provActual.razon_social === "") {
                this.setState({ validation: { ...this.state.validation, RAZON_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.provActual.razon_social))) {
                this.setState({ validation: { ...this.state.validation, RAZON_VACIO: true } });
                return;
            }

            if (this.state.provActual.ruc === null || this.state.provActual.ruc === "") {
                this.setState({ validation: { ...this.state.validation, RUC_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.provActual.ruc)) || isNaN(this.state.provActual.ruc) || this.state.provActual.ruc.length !== 11 || this.state.provActual.ruc.includes("-") || this.state.provActual.ruc.includes(".")) {
                this.setState({ validation: { ...this.state.validation, RUC_NAN: true } });
                return;
            }

            if (this.state.provActual.direccion === null || this.state.provActual.direccion === "") {
                this.setState({ validation: { ...this.state.validation, DIRECCION_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.provActual.direccion))) {
                this.setState({ validation: { ...this.state.validation, DIRECCION_VACIO: true } });
                return;
            }

            if (this.state.provActual.nombre_contacto === null || this.state.provActual.nombre_contacto === "") {
                this.setState({ validation: { ...this.state.validation, NOMBRE_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.provActual.nombre_contacto))) {
                this.setState({ validation: { ...this.state.validation, NOMBRE_VACIO: true } });
                return;
            }

            if (this.state.provActual.telefono_contacto === null || this.state.provActual.telefono_contacto === "") {
                this.setState({ validation: { ...this.state.validation, TELEFONO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.provActual.telefono_contacto)) || isNaN(this.state.provActual.telefono_contacto) || this.state.provActual.telefono_contacto.length !== 9 || this.state.provActual.telefono_contacto.includes("-") || this.state.provActual.telefono_contacto.includes(".")) {
                this.setState({ validation: { ...this.state.validation, TELEFONO_NAN: true } });
                return;
            }

            let params = {
                id: this.state.provActual.id,
                ruc: this.state.provActual.ruc,
                nombre_contacto: (this.state.provActual.nombre_contacto).toUpperCase(),
                telefono_contacto: this.state.provActual.telefono_contacto,
                direccion: (this.state.provActual.direccion).toUpperCase(),
                ubigeo: 1,
                razon_social: (this.state.provActual.razon_social).toUpperCase()
            }

            axios({
                method: 'post',
                url: '/proveedor/actualizar',
                data: params
            }).then((response) => {
                this.setState({ openEditar: false });
                axios({
                    method: 'post',
                    url: '/proveedor/lista'
                }).then((response) => {
                    this.setState({ proveedores: response.data })
                }).then(() => {
                    this.setState({
                        totalPages:
                            Math.ceil(this.state.proveedores.length / this.state.perPage)
                    });
                }).catch((error) => {
                    console.log('Error', error.message);
                });
            }).catch((error) => {
                console.log('Error', error.message);
            })
        })
    }

    bloquearProv = () => {
        let params = {
            idProveedor: this.state.provActual.id,
        }
        axios({
            method: 'post',
            url: '/proveedor/bloquear',
            data: params,
        }).then((response) => {
            this.setState({ isBlocked: !this.state.isBlocked })
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    desbloquearProv = () => {
        let params = {
            idProveedor: this.state.provActual.id,
        }

        axios({
            method: 'post',
            url: '/proveedor/desbloquear',
            data: params,
        }).then((response) => {
            this.setState({ isBlocked: !this.state.isBlocked })
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    borrarProveedor = () => {
        axios({
            method: 'post',
            url: '/proveedor/borrar',
            data: { idProveedor: this.state.idActual }
        }).then((response) => {
            if (response.data) {
                Swal.fire({
                    title: 'Éxito',
                    text: 'Proveedor eliminado',
                    icon: 'success',
                    timer: 2000
                })
            } else {
                Swal.fire({
                    title: 'Error',
                    text: 'No se pudo eliminar el proveedor. Es posible que tenga un contrato activo.',
                    icon: 'error'
                })
            }
            this.setState({ openEliminar: false });
            axios({
                method: 'post',
                url: '/proveedor/lista'
            }).then((response) => {
                this.setState({ proveedores: response.data })
            }).then(() => {
                this.setState({
                    totalPages:
                        Math.ceil(this.state.proveedores.length / this.state.perPage)
                });
            });
        })
    }



    render() {
        const { activePage } = this.state.activePage;
        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}>
                    <Grid.Column>
                        <h1>Proveedores</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button color='green' onClick={() => this.showModalCrear()}>Registrar nuevo proveedor</Button>
                    </Grid.Column>
                </Grid>
                <div style={{ paddingTop: 20 }}>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>ID</Table.HeaderCell>
                                <Table.HeaderCell>RUC</Table.HeaderCell>
                                <Table.HeaderCell>Razón Social</Table.HeaderCell>
                                <Table.HeaderCell>Acción</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.pageContent(this.state.activePage)}
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='4' style={{ textAlign: 'right' }}>
                                    <Pagination
                                        defaultActivePage={this.state.activePage}
                                        activePage={activePage}
                                        onPageChange={this.handlePaginationChange}
                                        totalPages={this.state.totalPages}
                                    />
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>

                    <Modal size={"small"} open={this.state.openCrear} closeOnDimmerClick={false} onClose={this.closeCrear}>
                        <Modal.Header>Registrar Nuevo Proveedor</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group>
                                    <Form.Input required fluid label='Razón social'
                                        placeholder='Proveedor SAC'
                                        onChange={(event, data) => this.setState({ razon_social: data.value })}
                                        error={this.state.validation.RAZON_VACIO && { content: 'Campo obligatorio' }}
                                        width={12}
                                        maxLength={255} />
                                    <Form.Input required fluid label='RUC'
                                        placeholder='123456789'
                                        onChange={(event, data) => this.setState({ ruc: data.value })}
                                        error={(this.state.validation.RUC_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.RUC_NAN && { content: 'Debe ser un número de 11 dígitos' })}
                                        width={4} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Input required fluid label='Dirección'
                                        placeholder='Av. Perú 12356, San Martín, Lima'
                                        onChange={(event, data) => this.setState({ direccion: data.value })}
                                        error={this.state.validation.DIRECCION_VACIO && { content: 'Campo obligatorio' }}
                                        width={16}
                                        maxLength={255} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Input required fluid label='Nombre del contacto'
                                        placeholder='Juan Perez'
                                        onChange={(event, data) => this.setState({ nombre_contacto: data.value })}
                                        error={this.state.validation.NOMBRE_VACIO && { content: 'Campo obligatorio' }}
                                        width={12}
                                        maxLength={255} />
                                    <Form.Input required fluid label='Número Celular'
                                        placeholder='987654321'
                                        onChange={(event, data) => this.setState({ telefono_contacto: data.value })}
                                        error={(this.state.validation.TELEFONO_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.TELEFONO_NAN && { content: 'Debe ser un número de 9 dígitos' })}
                                        width={4} />
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeCrear}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={() => this.crearProveedor()}
                            />
                        </Modal.Actions>
                    </Modal>
                    <Modal size={"small"} open={this.state.openEditar} closeOnDimmerClick={false} onClose={this.closeEditar}>
                        <Modal.Header>Editar Proveedor</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group>
                                    <Form.Input required fluid label='Razón social'
                                        placeholder='Proveedor SAC'
                                        onChange={(event, data) => this.editRazon(event, data)}
                                        error={this.state.validation.RAZON_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.provActual == null ? null : this.state.provActual.razon_social}
                                        width={12}
                                        maxLength={255} />
                                    <Form.Input required fluid label='RUC'
                                        placeholder='123456789'
                                        onChange={(event, data) => this.editRUC(event, data)}
                                        error={(this.state.validation.RUC_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.RUC_NAN && { content: 'Debe ser un número de 11 dígitos' })}
                                        value={this.state.provActual == null ? null : this.state.provActual.ruc}
                                        width={4} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Input required fluid label='Dirección'
                                        placeholder='Av. Perú 123 - San Martín'
                                        onChange={(event, data) => this.editDireccion(event, data)}
                                        error={this.state.validation.DIRECCION_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.provActual == null ? null : this.state.provActual.direccion}
                                        width={16}
                                        maxLength={255} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Input required fluid label='Nombre del contacto'
                                        placeholder='Juan Perez'
                                        onChange={(event, data) => this.editNombreContacto(event, data)}
                                        error={this.state.validation.NOMBRE_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.provActual == null ? null : this.state.provActual.nombre_contacto}
                                        width={12}
                                        maxLength={255} />
                                    <Form.Input required fluid label='Número Celular'
                                        placeholder='987654321'
                                        onChange={(event, data) => this.editTelefonoContacto(event, data)}
                                        error={(this.state.validation.TELEFONO_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.TELEFONO_NAN && { content: 'Debe ser un número de 9 dígitos' })}
                                        value={this.state.provActual == null ? null : this.state.provActual.telefono_contacto}
                                        width={4} />
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            {this.state.isBlocked !== true &&
                                <Button onClick={this.bloquearProv}>Bloquear</Button>
                            }
                            {this.state.isBlocked === true &&
                                <Button onClick={this.desbloquearProv}>Desbloquear</Button>
                            }
                            <Button negative onClick={this.closeEditar}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.editarProveedor}
                            />
                        </Modal.Actions>
                    </Modal>
                    <Modal size={"mini"}
                        open={this.state.openEliminar}
                        closeOnEscape={true}
                        closeOnDimmerClick={false}
                        onClose={this.closeEliminar}>
                        <Modal.Header>Confirmación de Eliminación</Modal.Header>
                        <Modal.Content>
                            <p>¿Seguro que quiere eliminar este proveedor?</p>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button onClick={this.closeEliminar} negative>Cancelar</Button>
                            <Button
                                onClick={this.borrarProveedor}
                                positive
                                labelPosition='right'
                                icon='checkmark'
                                content='Confirmar'
                            />
                        </Modal.Actions>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default ProveedorView;