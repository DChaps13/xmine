import React, { Component } from 'react';
import { Table, Pagination, Button, Grid, Form, Modal } from 'semantic-ui-react';
import axios from '../../context/axios';
import Swal from 'sweetalert2';

class ActivoView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activos: [],
            tipo_activos: [],
            perPage: 8,
            totalPages: 1,
            activePage: 1,
            activoActual: null,
            openCrear: false,
            openEditar: false,
            openEliminar: false,
            openAumentarHoras: false,
            placa: null,
            modelo: null,
            marca: null,
            peso_bruto: null,
            potencia: null,
            horas_uso: null,
            horas_nuevas: null,
            horas_finales: null,
            validation: {
                PLACA_VACIO: false,
                PLACA_MAL: false,
                MARCA_VACIO: false,
                MODELO_VACIO: false,
                TIPO_VACIO: false,
                PESO_VACIO: false,
                PESO_NAN: false,
                POTENCIA_VACIO: false,
                POTENCIA_NAN: false,
                HORAS_VACIO: false,
                HORAS_NAN: false
            }
        }

        axios({
            method: 'post',
            url: '/activo/lista'
        }).then((response) => {
            this.setState({ activos: response.data });
        }).then(() => {
            this.setState({
                totalPages:
                    Math.ceil(this.state.activos.length / this.state.perPage)
            });
        });

        axios({
            method: 'post',
            url: '/activo/lista/tipo_de_activo'
        }).then((response) => {
            let tipo_activos = [];

            for (let i = 0; i < response.data.length; i++) {
                tipo_activos[i] = {
                    key: i + 1,
                    text: response.data[i].nombre_tipo,
                    value: response.data[i].id
                }
            }
            this.setState({ tipo_activos: tipo_activos });
        });
    }

    componentDidMount() {
        document.title = "Activos";
    }

    showModalEliminar = (id) => this.setState({ idActual: id, openEliminar: true })
    showModalEditar = (p) => {
        this.setState({
            activoActual: { ...p },
            openEditar: true,
            placa: null,
            modelo: null,
            marca: null,
            peso_bruto: null,
            potencia: null,
            horas_uso: null,
            validation: {
                PLACA_VACIO: false,
                PLACA_MAL: false,
                MARCA_VACIO: false,
                MODELO_VACIO: false,
                TIPO_VACIO: false,
                PESO_VACIO: false,
                PESO_NAN: false,
                POTENCIA_VACIO: false,
                POTENCIA_NAN: false
            }
        })
    }

    showModalCrear = () => {
        this.setState({
            openCrear: true,
            placa: null,
            modelo: null,
            marca: null,
            peso_bruto: null,
            potencia: null,
            horas_uso: null,
            validation: {
                PLACA_VACIO: false,
                PLACA_MAL: false,
                MARCA_VACIO: false,
                MODELO_VACIO: false,
                TIPO_VACIO: false,
                PESO_VACIO: false,
                PESO_NAN: false,
                POTENCIA_VACIO: false,
                POTENCIA_NAN: false
            }
        })
    }

    showModalAumentarHoras = (p) => {
        this.setState({
            activoActual: { ...p },
            openAumentarHoras: true,
            placa: null,
            modelo: null,
            marca: null,
            peso_bruto: null,
            potencia: null,
            horas_uso: null,
            horas_nuevas: null,
            horas_finales: null,
            validation: {
                PLACA_VACIO: false,
                PLACA_MAL: false,
                MARCA_VACIO: false,
                MODELO_VACIO: false,
                TIPO_VACIO: false,
                PESO_VACIO: false,
                PESO_NAN: false,
                POTENCIA_VACIO: false,
                POTENCIA_NAN: false,
                HORAS_VACIO: false,
                HORAS_NAN: false
            }
        })
    }

    closeCrear = () => this.setState({ openCrear: false, activoActual: null })
    closeEditar = () => this.setState({ openEditar: false })
    closeEliminar = () => this.setState({ openEliminar: false })
    closeAumentarHoras = () => this.setState({ openAumentarHoras: false })

    editPlaca = (event, data) => {
        this.setState({ activoActual: { ...this.state.activoActual, placa: data.value } });
        this.setState({ placa: data.value });
    }

    editModelo = (event, data) => {
        this.setState({ activoActual: { ...this.state.activoActual, modelo: data.value } });
        this.setState({ modelo: data.value });
    }

    editMarca = (event, data) => {
        this.setState({ activoActual: { ...this.state.activoActual, marca: data.value } });
        this.setState({ marca: data.value });
    }

    editPeso = (event, data) => {
        this.setState({ activoActual: { ...this.state.activoActual, peso_bruto: data.value } });
        this.setState({ peso_bruto: data.value });
    }

    editPotencia = (event, data) => {
        this.setState({ activoActual: { ...this.state.activoActual, potencia: data.value } });
        this.setState({ potencia: data.value });
    }

    editHoras = (event, data) => {
        let horasAux = data.value;
        if (!(/\S/.test(horasAux)) || isNaN(horasAux) || String(horasAux).includes("-")) horasAux = 0;
        this.setState({ activoActual: { ...this.state.activoActual, horas_nuevas: data.value, horas_finales: parseInt(this.state.activoActual.horas_uso) + parseInt(horasAux) } });
        this.setState({ horas_nuevas: data.value, horas_finales: parseInt(this.state.activoActual.horas_uso) + parseInt(horasAux) });
    }

    editPrioridad = (event, data) => {
        this.setState({ activoActual: { ...this.state.activoActual, tipo_activo: { id: data.value } } });
        this.setState({ prioridad: data.value });
    }

    crearActivo = () => {
        this.setState({
            validation: {
                PLACA_VACIO: false,
                PLACA_MAL: false,
                MARCA_VACIO: false,
                MODELO_VACIO: false,
                TIPO_VACIO: false,
                PESO_VACIO: false,
                PESO_NAN: false,
                POTENCIA_VACIO: false,
                POTENCIA_NAN: false
            }
        }, () => {
            if (this.state.placa === null || this.state.placa === "") {
                this.setState({ validation: { ...this.state.validation, PLACA_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.placa)) || this.state.placa.trim().length !== 7 || this.state.placa[3] !== '-') {
                this.setState({ validation: { ...this.state.validation, PLACA_MAL: true } });
                return;
            }

            if (this.state.marca === null || this.state.marca === "") {
                this.setState({ validation: { ...this.state.validation, MARCA_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.marca))) {
                this.setState({ validation: { ...this.state.validation, MARCA_VACIO: true } });
                return;
            }

            if (this.state.modelo === null || this.state.modelo === "") {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.modelo))) {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (this.state.prioridad === null || this.state.prioridad === undefined) {
                this.setState({ validation: { ...this.state.validation, TIPO_VACIO: true } });
                return;
            }

            if (this.state.peso_bruto === null || this.state.peso_bruto === "") {
                this.setState({ validation: { ...this.state.validation, PESO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.peso_bruto)) || isNaN(this.state.peso_bruto) || this.state.peso_bruto.includes("-")) {
                this.setState({ validation: { ...this.state.validation, PESO_NAN: true } });
                return;
            }

            if (this.state.potencia === null || this.state.potencia === "") {
                this.setState({ validation: { ...this.state.validation, POTENCIA_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.potencia)) || isNaN(this.state.potencia) || this.state.potencia.includes("-")) {
                this.setState({ validation: { ...this.state.validation, POTENCIA_NAN: true } });
                return;
            }

            let params = {
                placa: (this.state.placa).toUpperCase(),
                tipo_de_activo: {
                    id: this.state.prioridad
                },
                peso_bruto: this.state.peso_bruto,
                modelo: (this.state.modelo).toUpperCase(),
                marca: (this.state.marca).toUpperCase(),
                potencia: this.state.potencia
            }

            axios({
                method: 'post',
                url: '/activo/crear',
                data: params
            }).then((response) => {
                this.setState({ openCrear: false, activoActual: null }, () => {
                    if (response.data) {
                        Swal.fire({
                            title: 'Éxito',
                            text: 'Activo creado',
                            icon: 'success',
                            timer: 1250
                        })
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Ocurrió un error en la creación del activo',
                            icon: 'error'
                        })
                    }
                });
                axios({
                    method: 'post',
                    url: '/activo/lista'
                }).then((response) => {
                    this.setState({ activos: response.data });
                }).then(() => {
                    this.setState({
                        totalPages:
                            Math.ceil(this.state.activos.length / this.state.perPage)
                    });
                });
            })
        })
    }

    editarActivo = () => {
        this.setState({
            validation: {
                PLACA_VACIO: false,
                PLACA_MAL: false,
                MARCA_VACIO: false,
                MODELO_VACIO: false,
                TIPO_VACIO: false,
                PESO_VACIO: false,
                PESO_NAN: false,
                POTENCIA_VACIO: false,
                POTENCIA_NAN: false
            }
        }, () => {
            if (this.state.activoActual.placa === null || this.state.activoActual.placa === "") {
                this.setState({ validation: { ...this.state.validation, PLACA_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.activoActual.placa)) || this.state.activoActual.placa.trim().length !== 7 || this.state.activoActual.placa[3] !== '-') {
                this.setState({ validation: { ...this.state.validation, PLACA_MAL: true } });
                return;
            }

            if (this.state.activoActual.marca === null || this.state.activoActual.marca === "") {
                this.setState({ validation: { ...this.state.validation, MARCA_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.activoActual.marca))) {
                this.setState({ validation: { ...this.state.validation, MARCA_VACIO: true } });
                return;
            }

            if (this.state.activoActual.modelo === null || this.state.activoActual.modelo === "") {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.activoActual.modelo))) {
                this.setState({ validation: { ...this.state.validation, MODELO_VACIO: true } });
                return;
            }

            if (this.state.activoActual.tipo_activo.id === null || this.state.activoActual.tipo_activo.id === undefined) {
                this.setState({ validation: { ...this.state.validation, TIPO_VACIO: true } });
                return;
            }

            if (this.state.activoActual.peso_bruto === null || this.state.activoActual.peso_bruto === "") {
                this.setState({ validation: { ...this.state.validation, PESO_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.activoActual.peso_bruto)) || isNaN(this.state.activoActual.peso_bruto) || String(this.state.activoActual.peso_bruto).includes("-")) {
                this.setState({ validation: { ...this.state.validation, PESO_NAN: true } });
                return;
            }

            if (this.state.activoActual.potencia === null || this.state.activoActual.potencia === "") {
                this.setState({ validation: { ...this.state.validation, POTENCIA_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.activoActual.potencia)) || isNaN(this.state.activoActual.potencia) || String(this.state.activoActual.potencia).includes("-")) {
                this.setState({ validation: { ...this.state.validation, POTENCIA_NAN: true } });
                return;
            }

            let params = {
                id: this.state.activoActual.id,
                placa: (this.state.activoActual.placa).toUpperCase(),
                tipo_de_activo: {
                    id: this.state.activoActual.tipo_activo.id
                },
                peso_bruto: this.state.activoActual.peso_bruto,
                modelo: (this.state.activoActual.modelo).toUpperCase(),
                marca: (this.state.activoActual.marca).toUpperCase(),
                potencia: this.state.activoActual.potencia,
                horas_uso: this.state.activoActual.horas_uso
            }

            axios({
                method: 'post',
                url: '/activo/actualizar',
                data: params
            }).then((response) => {
                this.setState({ openEditar: false, activoActual: null }, () => {
                    if (response.data) {
                        Swal.fire({
                            title: 'Éxito',
                            text: 'Activo actualizado',
                            icon: 'success',
                            timer: 1250
                        })
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Ocurrió un error en la edición del activo',
                            icon: 'error'
                        })
                    }
                });
                axios({
                    method: 'post',
                    url: '/activo/lista'
                }).then((response) => {
                    this.setState({ activos: response.data });
                }).then(() => {
                    this.setState({
                        totalPages:
                            Math.ceil(this.state.activos.length / this.state.perPage)
                    });
                });
            })
        })
    }

    borrarActivo = () => {
        axios({
            method: 'post',
            url: '/activo/borrar',
            data: { idActivo: this.state.idActual }
        }).then((response) => {
            this.setState({ openEliminar: false }, () => {
                if (response.data) {
                    Swal.fire({
                        title: 'Éxito',
                        text: 'Activo actualizado',
                        icon: 'success',
                        timer: 1250
                    });

                    axios({
                        method: 'post',
                        url: '/activo/lista'
                    }).then((response) => {
                        this.setState({ activos: response.data });
                    }).then(() => {
                        this.setState({
                            totalPages:
                                Math.ceil(this.state.activos.length / this.state.perPage)
                        });
                    });
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Ocurrió un error en la edición del activo. Es posible que tenga una solicitud asignada.',
                        icon: 'error'
                    })
                }
            });
        })
    }

    aumentarHorasActivo = () => {
        this.setState({
            validation: {
                HORAS_VACIO: false,
                HORAS_NAN: false
            }
        }, () => {
            if (this.state.activoActual.horas_nuevas === null || this.state.activoActual.horas_nuevas === "") {
                this.setState({ validation: { ...this.state.validation, HORAS_VACIO: true } });
                return;
            }

            if (!(/\S/.test(this.state.activoActual.horas_nuevas)) || isNaN(this.state.activoActual.horas_nuevas) || String(this.state.activoActual.horas_nuevas).includes("-")) {
                this.setState({ validation: { ...this.state.validation, HORAS_NAN: true } });
                return;
            }

            let params = {
                id: this.state.activoActual.id,
                horas: this.state.activoActual.horas_nuevas
            }

            axios({
                method: 'post',
                url: '/activo/actualizarHoras',
                data: params
            }).then((response) => {
                this.setState({ openAumentarHoras: false, activoActual: null });
                axios({
                    method: 'post',
                    url: '/activo/lista'
                }).then((response) => {
                    this.setState({ activos: response.data });
                }).then(() => {
                    this.setState({
                        totalPages:
                            Math.ceil(this.state.activos.length / this.state.perPage)
                    });
                });
            })
        })
    }

    pageContent = (pageNumber) => {
        let offset = (pageNumber - 1) * this.state.perPage;
        return this.state.activos.slice(offset, offset + this.state.perPage)
            .map(p => <Table.Row>
                <Table.Cell>{p.id}</Table.Cell>
                <Table.Cell>{p.placa}</Table.Cell>
                <Table.Cell>{p.marca}</Table.Cell>
                <Table.Cell>{p.modelo}</Table.Cell>
                <Table.Cell>{p.tipo_activo.nombre_tipo}</Table.Cell>
                <Table.Cell style={{ textAlign: 'center' }}>
                    <Button circular icon='pencil' onClick={() => this.showModalEditar(p)}></Button>
                    <Button circular icon onClick={() => this.showModalAumentarHoras(p)}>Hr+</Button>
                    <Button circular negative icon='trash alternate outline' onClick={() => this.showModalEliminar(p.id)}></Button>
                </Table.Cell>
            </Table.Row>)
    }

    handlePaginationChange = (e, { activePage }) => this.setState({ activePage: activePage });

    render() {
        const { activePage } = this.state.activePage;
        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}>
                    <Grid.Column>
                        <h1>Activos</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button color='green' onClick={() => this.showModalCrear()}>Registrar Activo</Button>
                    </Grid.Column>
                </Grid>
                <div style={{ paddingTop: 20 }}>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>ID</Table.HeaderCell>
                                <Table.HeaderCell>Placa</Table.HeaderCell>
                                <Table.HeaderCell>Marca</Table.HeaderCell>
                                <Table.HeaderCell>Modelo</Table.HeaderCell>
                                <Table.HeaderCell>Tipo de Activo</Table.HeaderCell>
                                <Table.HeaderCell>Acción</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.pageContent(this.state.activePage)}
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='6' style={{ textAlign: 'right' }}>
                                    <Pagination
                                        defaultActivePage={this.state.activePage}
                                        activePage={activePage}
                                        onPageChange={this.handlePaginationChange}
                                        totalPages={this.state.totalPages}
                                    />
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>

                    </Table>

                    <Modal size={"small"} open={this.state.openCrear} closeOnDimmerClick={false} onClose={this.closeCrear}>
                        <Modal.Header>Registrar Activo</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group widths='equal'>
                                    <Form.Input required fluid label='Placa'
                                        placeholder='ABC-123'
                                        onChange={(event, data) => this.setState({ placa: data.value })}
                                        error={(this.state.validation.PLACA_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.PLACA_MAL && { content: 'Debe tener el formato XXX-XXX' })} />
                                    <Form.Input required fluid label='Marca'
                                        placeholder='CATERPILLAR'
                                        onChange={(event, data) => this.setState({ marca: data.value })}
                                        error={this.state.validation.MARCA_VACIO && { content: 'Campo obligatorio' }}
                                        maxLength={255} />
                                    <Form.Input required fluid label='Modelo'
                                        placeholder='R2D2'
                                        onChange={(event, data) => this.setState({ modelo: data.value })}
                                        error={this.state.validation.MODELO_VACIO && { content: 'Campo obligatorio' }}
                                        maxLength={255} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Select
                                        required
                                        fluid
                                        label='Tipo de Vehículo'
                                        options={this.state.tipo_activos}
                                        placeholder='Tipo de Vehículo'
                                        value={this.state.tipo_activos.id}
                                        selection
                                        onChange={(event, data) => this.setState({ prioridad: data.value })}
                                        error={this.state.validation.TIPO_VACIO && { content: 'Campo obligatorio' }}
                                        width={8} />
                                    <Form.Input required fluid label='Peso bruto (kg)'
                                        placeholder='0'
                                        onChange={(event, data) => this.setState({ peso_bruto: data.value })}
                                        error={(this.state.validation.PESO_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.PESO_NAN && { content: 'Debe ser un número' })}
                                        width={4}
                                        maxLength={25} />
                                    <Form.Input required fluid label='Potencia (HP)'
                                        placeholder='0'
                                        onChange={(event, data) => this.setState({ potencia: data.value })}
                                        error={(this.state.validation.POTENCIA_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.POTENCIA_NAN && { content: 'Debe ser un número' })}
                                        width={4}
                                        maxLength={25} />
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeCrear}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.crearActivo}
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"small"} open={this.state.openEditar} closeOnDimmerClick={false} onClose={this.closeEditar}>
                        <Modal.Header>Editar Activo</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group widths='equal'>
                                    <Form.Input required fluid label='Placa'
                                        placeholder='ABC-123'
                                        onChange={(event, data) => this.editPlaca(event, data)}
                                        error={(this.state.validation.PLACA_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.PLACA_MAL && { content: 'Debe tener el formato XXX-XXX' })}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.placa} />
                                    <Form.Input required fluid label='Marca'
                                        placeholder='CATERPILLAR'
                                        onChange={(event, data) => this.editMarca(event, data)}
                                        error={this.state.validation.MARCA_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.marca}
                                        maxLength={255} />
                                    <Form.Input required fluid label='Modelo'
                                        placeholder='R2D2'
                                        onChange={(event, data) => this.editModelo(event, data)}
                                        error={this.state.validation.MODELO_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.modelo}
                                        maxLength={255} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Select
                                        required
                                        fluid
                                        label='Tipo de Vehículo'
                                        options={this.state.tipo_activos}
                                        placeholder='Tipo de Vehículo'
                                        value={this.state.activoActual == null ? null : this.state.activoActual.tipo_activo.id}
                                        selection
                                        onChange={(event, data) => this.editPrioridad(event, data)}
                                        error={this.state.validation.TIPO_VACIO && { content: 'Campo obligatorio' }}
                                        width={8}
                                    />
                                    <Form.Input required fluid label='Peso bruto (kg)'
                                        placeholder='0'
                                        onChange={(event, data) => this.editPeso(event, data)}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.peso_bruto}
                                        error={(this.state.validation.PESO_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.PESO_NAN && { content: 'Debe ser un número' })}
                                        width={4}
                                        maxLength={25} />
                                    <Form.Input required fluid label='Potencia (HP)'
                                        placeholder='0'
                                        onChange={(event, data) => this.editPotencia(event, data)}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.potencia}
                                        error={(this.state.validation.POTENCIA_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.POTENCIA_NAN && { content: 'Debe ser un número' })}
                                        width={4}
                                        maxLength={25} />
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeEditar}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.editarActivo}
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"mini"}
                        open={this.state.openEliminar}
                        closeOnEscape={true}
                        closeOnDimmerClick={false}
                        onClose={this.closeEliminar}>
                        <Modal.Header>Confirmación de Eliminación</Modal.Header>
                        <Modal.Content>
                            <p>¿Seguro que quiere eliminar este activo?</p>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button onClick={this.closeEliminar} negative>Cancelar</Button>
                            <Button
                                onClick={this.borrarActivo}
                                positive
                                labelPosition='right'
                                icon='checkmark'
                                content='Confirmar'
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"small"} open={this.state.openAumentarHoras} closeOnDimmerClick={false} onClose={this.closeAumentarHoras}>
                        <Modal.Header>Aumentar horas activo</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group widths='equal'>
                                    <Form.Input required fluid label='Placa'
                                        placeholder='ABC-123'
                                        onChange={(event, data) => this.editPlaca(event, data)}
                                        error={(this.state.validation.PLACA_VACIO && { content: 'Campo obligatorio' }) ||
                                            (this.state.validation.PLACA_MAL && { content: 'Debe tener el formato XXX-XXX' })}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.placa} />
                                    <Form.Input required fluid label='Marca'
                                        placeholder='CATERPILLAR'
                                        onChange={(event, data) => this.editMarca(event, data)}
                                        error={this.state.validation.MARCA_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.marca} />
                                    <Form.Input required fluid label='Modelo'
                                        placeholder='R2D2'
                                        onChange={(event, data) => this.editModelo(event, data)}
                                        error={this.state.validation.MODELO_VACIO && { content: 'Campo obligatorio' }}
                                        value={this.state.activoActual == null ? null : this.state.activoActual.modelo} />
                                </Form.Group>
                                <Form.Group inline>
                                    <label>Horas*</label>
                                    <Table celled>
                                        <Table.Header>
                                            <Table.Row>
                                                <Table.HeaderCell>Acumuladas</Table.HeaderCell>
                                                <Table.HeaderCell>+ Nuevas</Table.HeaderCell>
                                                <Table.HeaderCell>Finales</Table.HeaderCell>
                                            </Table.Row>
                                        </Table.Header>
                                        <Table.Body>
                                            <Table.Row>
                                                <Table.Cell class="centered-cell">
                                                    {this.state.activoActual == null ? null : this.state.activoActual.horas_uso}
                                                </Table.Cell>
                                                <Table.Cell>
                                                    <Form.Input placeholder='15'
                                                        onChange={(event, data) => this.editHoras(event, data)}
                                                        error={(this.state.validation.HORAS_VACIO && { content: 'Campo obligatorio' }) ||
                                                            (this.state.validation.HORAS_NAN && { content: 'Debe ser un número mayor a 0' })}
                                                        width={10}
                                                    />
                                                </Table.Cell>
                                                <Table.Cell>
                                                    {this.state.activoActual == null ? null : this.state.activoActual.horas_finales == null ? this.state.activoActual.horas_uso : this.state.activoActual.horas_finales}
                                                </Table.Cell>
                                            </Table.Row>
                                        </Table.Body>
                                    </Table>
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeAumentarHoras}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.aumentarHorasActivo}
                            />
                        </Modal.Actions>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default ActivoView;