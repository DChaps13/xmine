import React, { Component } from 'react';
import { Segment, Modal, Icon, Grid, Button, Header, Form, Image } from 'semantic-ui-react';
import axios from '../../context/axios';
import { withCookies } from 'react-cookie';
import Recaptcha from 'react-google-invisible-recaptcha'

class LoginView extends Component {
    constructor(props) {
        super(props);
        const { cookies } = props;
        cookies.set('logged', 'false');
        this.state = {
            username: '',
            password: '',
            logged: cookies.get('logged') || false,
            acceso: null,
            openError: false,
            messageSent: false,
            id: 1,
        }
    }

    login = () => {
        this.setState({ messageSent: true });

        const { cookies } = this.props;
        let params = {
            usuario: this.state.username,
            contrasena: this.state.password,
        };

        axios({
            method: 'post',
            url: '/login/',
            data: params,
        }).then((response) => {
            console.log(response.data);
            this.setState({ id: response.data.id })
            if (response.data && (response.data.rol > 0)) {
                cookies.set('usuario', {
                    id: response.data.id,
                    nombre: response.data.nombre,
                    rol: response.data.rol,
                    username: response.data.username,
                    contrasena: response.data.contrasena,
                    apellido_materno: response.data.apellido_materno,
                    apellido_paterno: response.data.apellido_paterno,
                    proveedor: {
                        id: response.data.proveedor ? response.data.proveedor.id : null
                    }
                });
                cookies.set('logged', 'true');
                console.log(cookies.get("usuario"));
            } else {
                this.setState({ openError: true })
            }
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    componentDidMount() {
        document.title = "Login";
    }


    closeError = () => {
        this.setState({ openError: false })
    }

    onResolved = () => {
        this.recaptcha.execute();
    }

    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as='h2' color='teal' textAlign='center'>
                        <Image src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Logo_of_X_%28company%29.svg/1200px-Logo_of_X_%28company%29.svg.png' size='massive' /> Log-in to X-Mine
                    </Header>
                    <Form size='large'>
                        <Segment stacked>
                            <Form.Input fluid icon='user' iconPosition='left' placeholder='Username'
                                onChange={(event) => this.setState({ username: event.target.value })} />
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='Password'
                                type='password'
                                onChange={(event) => this.setState({ password: event.target.value })}
                            />

                            <Button color='teal' fluid size='large' onClick={this.onResolved}>
                                Login
                            </Button>
                            <Recaptcha ref={ref => this.recaptcha = ref} sitekey={process.env.REACT_APP_API_KEY} onResolved={this.login}></Recaptcha>
                        </Segment>
                    </Form>
                    {/* <Message>
                        New to us? <a href='#'>Sign Up</a>
                    </Message> */}
                </Grid.Column>

                <Modal open={this.state.openError} basic size='tiny'>
                    <Header icon='cancel' content='Error de Login' />
                    <Modal.Content>
                        <p>
                            {this.state.id === 0 ? "Usuario y/o Contraseña incorrectos" : "Usuario eliminado"}
                        </p>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='green' inverted onClick={this.closeError}>
                            <Icon name='checkmark' /> Ok
                        </Button>
                    </Modal.Actions>
                </Modal>
            </Grid>

        );
    }
}

export default withCookies(LoginView);