import _ from 'lodash'
import React, { Component } from 'react';
import { Table, Grid, Button, Modal, Form, Pagination, Search, Label, Input } from 'semantic-ui-react';
import { DateInput } from 'semantic-ui-calendar-react';
import axios from '../../context/axios';

var moment = require('moment');

const initialProv = {
    isLoadingProv: false,
    resultsProv: [],
    valueProv: ''
}

class ContratoView extends Component {

    handleResultSelectProv = (e, { result }) => {
        console.dir("LEER ACA")
        console.dir(result);
        result.title = result.razon_social;
        this.setState({
            valueProv: result.title, idProv: result.id, nombProveedor: result.razon_social,
            selectedProv: {
                razon_social: result.razon_social, ruc: result.ruc
            }
        })
    }

    handleSearchChangeProv = (e, { value }) => {
        this.setState({ isLoadingProv: true, valueProv: value })

        setTimeout(() => {
            if (this.state.valueProv.length < 1) return this.setState(initialProv)
            console.dir(this.state);
            const re = new RegExp(_.escapeRegExp(this.state.valueProv), 'i')
            const isMatch = (result) => re.test(result.razon_social);

            this.setState({
                isLoadingProv: false,
                resultsProv: _.filter(this.state.proveedores, isMatch),
            })
        }, 300)
    }

    constructor(props) {
        super(props);
        this.state = {
            contratos: [],
            tipoActivo: [],
            idActual: null,
            perPage: 8,
            totalPages: 1,
            activePage: 1,
            openCrear: false,
            openEliminar: false,
            openEditar: false,
            contratoActual: null,
            fechaIni: '',
            fechaFin: '',
            proveedor: null,
            ...initialProv,
            selectedProv: {
                razon_social: '-',
                ruc: '-'
            },
            idProv: '',
            dateIni: '',
            dateFin: '',
            observaciones: '',
            totRep: 0,
            totMant: 0,
            validation: {
                NO_PROV: false,
                NO_DTINI: false,
                NO_DTFIN: false,
                DT_MAL: false,
                NO_CAPACIDAD: false,
                NO_CHCK: false
            }
        }

        axios({
            method: 'post',
            url: '/activo/lista/tipo_de_activo'
        })
            .then((response) => {
                console.log("Tipos de Activos:")
                for (let i = 0; i < response.data.length; i++) {
                    response.data[i].checked = false;
                    response.data[i].calificacion_tipo = 10;
                    response.data[i].tarifa = 0;
                    response.data[i].capacidad_mantenimiento = 0;
                    response.data[i].capacidad_reparacion = 0;
                    response.data[i].tiempo_respuesta_est = 0;
                    response.data[i].id_tipo_de_activo = { id: response.data[i].id };
                    response.data[i].activo = true;
                }
                this.setState({ tipoActivo: response.data });
                console.log(this.state.tipoActivo)

            })
            .catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            })
            ;

        axios({
            method: 'post',
            url: '/contrato/lista'
        })
            .then((response) => {
                this.setState({ contratos: response.data })
                console.log("Lista de contratos:")
                console.log(response.data);
            })
            .then(() => {
                this.setState({
                    totalPages:
                        Math.ceil(this.state.contratos.length / this.state.perPage)
                });
            })
            .catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            })
            ;

        axios({
            method: 'post',
            url: '/proveedor/lista'
        })
            .then((response) => {
                console.log(response.data);
                let aux = response.data.map(prov => {
                    return { ...prov, title: prov.razon_social, description: prov.ruc }
                });
                this.setState({ proveedores: aux })
            })
            .catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            })
            ;
    }

    componentDidMount() {
        document.title = "Contratos";
    }

    pageContent = (pageNumber) => {
        let offset = (pageNumber - 1) * this.state.perPage;
        return this.state.contratos.slice(offset, offset + this.state.perPage)
            .map(p => <Table.Row>
                <Table.Cell>{p.id}</Table.Cell>
                <Table.Cell>{p.proveedor.razon_social}</Table.Cell>
                <Table.Cell>{p.fecha_inicio.substring(0, 10)}</Table.Cell>
                <Table.Cell>{p.fecha_fin.substring(0, 10)}</Table.Cell>
                <Table.Cell>{p.observaciones}</Table.Cell>
                <Table.Cell>{Math.round(p.calificacion_contrato)}</Table.Cell>
                <Table.Cell style={{ textAlign: 'center' }}>
                    <Button circular icon='eye' onClick={() => this.showModalVer(p)}></Button>
                    <Button negative circular icon='x' onClick={() => this.showModalEliminar(p.id)}></Button>
                </Table.Cell>
            </Table.Row>)
    }

    funVerificar = (id) => {
        let aux = this.state.tipoActivo;
        for (let i = 0; i < aux.length; i++) {
            if (aux[i].id === id) {
                aux[i].checked = !aux[i].checked;
                this.setState({ tipoActivo: aux });
            }
        }
    }

    setCapRep = (data, id) => {
        let aux = this.state.tipoActivo;
        for (let i = 0; i < aux.length; i++) {
            if (aux[i].id === id) {
                aux[i].capacidad_reparacion = data;
                this.setState({ tipoActivo: aux });
            }
        }
    }

    setCapMan = (data, id) => {
        let aux = this.state.tipoActivo;
        for (let i = 0; i < aux.length; i++) {
            if (aux[i].id === id) {
                aux[i].capacidad_mantenimiento = data;
                this.setState({ tipoActivo: aux });
            }
        }
    }
    setTarif = (data, id) => {
        let aux = this.state.tipoActivo;
        for (let i = 0; i < aux.length; i++) {
            if (aux[i].id === id) {
                aux[i].tarifa = data;
                this.setState({ tipoActivo: aux });
            }
        }
    }
    setTiempoRpta = (data, id) => {
        let aux = this.state.tipoActivo;
        for (let i = 0; i < aux.length; i++) {
            if (aux[i].id === id) {
                aux[i].tiempo_respuesta_est = data;
                this.setState({ tipoActivo: aux });
            }
        }
        console.log("LEER ACA:::")
        console.log(this.state.tipoActivo)
    }

    pageContent2 = () => {
        return this.state.tipoActivo
            .map(p =>
                <Table.Row>
                    <Table.Cell>
                        {p.nombre_tipo}
                    </Table.Cell>
                    <Table.Cell>
                        <Form.Radio checked={p.checked}
                            fluid toggle onChange={() => this.funVerificar(p.id)} />
                    </Table.Cell>
                    <Table.Cell>
                        <Form.Input fluid placeholder='15'
                            disabled={!p.checked}
                            error={(p.CANT_REP_VACIO && { content: 'Campo obligatorio' }) ||
                                (p.CANT_REP_MAL && { content: 'Valor inválido' })}
                            onChange={(event, data) => this.setCapRep(data.value, p.id)}
                        />
                    </Table.Cell>
                    <Table.Cell>
                        <Form.Input fluid placeholder='15'
                            disabled={!p.checked}
                            error={(p.CANT_MANT_VACIO && { content: 'Campo obligatorio' }) ||
                                (p.CANT_MANT_MAL && { content: 'Valor inválido' })}
                            onChange={(event, data) => this.setCapMan(data.value, p.id)}
                        />
                    </Table.Cell>
                    <Table.Cell>
                        <Form.Input fluid placeholder='15'
                            disabled={!p.checked}
                            error={(p.TARIFA_VACIO && { content: 'Campo obligatorio' }) ||
                                (p.TARIFA_MAL && { content: 'Valor inválido' })}
                            onChange={(event, data) => this.setTarif(data.value, p.id)}
                        />
                    </Table.Cell>
                    <Table.Cell>
                        <Form.Input fluid placeholder='15'
                            disabled={!p.checked}
                            error={(p.HORA_MAL && { content: 'Campo obligatorio' }) ||
                                (p.HORAS_VACIO && { content: 'Valor inválido' })}
                            onChange={(event, data) => this.setTiempoRpta(data.value, p.id)}
                        />
                    </Table.Cell>
                </Table.Row>
            )
    }

    pageContent3 = () => {
        return this.state.contratoActual == null ? null : this.state.contratoActual.contratoDetalle
            .map(p =>
                <Table.Row>
                    <Table.Cell>
                        {p.tipoActivo.nombre_tipo}
                    </Table.Cell>
                    <Table.Cell>
                        {p.capacidad_reparacion}
                    </Table.Cell>
                    <Table.Cell>
                        {p.capacidad_mantenimiento}
                    </Table.Cell>
                    <Table.Cell>
                        {p.tarifa}
                    </Table.Cell>
                    <Table.Cell>
                        {p.tiempo_respuesta_est}
                    </Table.Cell>
                </Table.Row>
            )
    }

    handlePaginationChange = (e, { activePage }) => this.setState({ activePage: activePage });
    showModalVer = (p) => {
        this.state.contratoActual = p;
        let params = {
            id: this.state.contratoActual.id,
        }
        axios({
            method: 'post',
            url: '/contrato/obtenerContrato',
            data: params,

        }).then((response) => {
            this.setState({ contratoActual: response.data, openEditar: true, valueProv: String(p.proveedor.razon_social) })
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });





    }
    closeVer = () => this.setState({ openEditar: false });
    showModalEliminar = (id) => this.setState({ idActual: id, openEliminar: true });
    showModalCrear = () => {
        this.setState({ openCrear: true, contratoActual: null });
    }
    closeCrear = () => {
        this.setState({ openCrear: false })
        window.location.replace('/contrato');
    }
    closeEliminar = () => this.setState({ openEliminar: false })

    handleChangeIni = (event, { name, value }) => {
        this.setState({ dateIni: value });
    }
    handleChangeFin = (event, { name, value }) => {
        this.setState({ dateFin: value });
    }

    confirmarSolicitud = () => {
        this.setState({
            validation: {
                NO_PROV: false,
                NO_DTINI: false,
                NO_DTFIN: false,
                DT_MAL: false,
                NO_CAPACIDAD: false,
                NO_CHCK: false
            }
        }, () => {
            if (this.state.selectedProv.ruc == '-') {
                this.setState({ validation: { ...this.state.validation, NO_PROV: true } });
                return;
            }

            if (!(moment(this.state.dateIni, "DD-MM-YYYY", true).isValid())) {
                this.setState({ validation: { ...this.state.validation, NO_DTINI: true } });
                return;
            }

            if (!(moment(this.state.dateFin, "DD-MM-YYYY", true).isValid())) {
                this.setState({ validation: { ...this.state.validation, NO_DTFIN: true } });
                return;
            }

            var dt_ini = moment(this.state.dateIni, 'DD-MM-YYYY')
            var dt_fin = moment(this.state.dateFin, 'DD-MM-YYYY')

            if (dt_ini.diff(dt_fin) >= 0) {
                this.setState({ validation: { ...this.state.validation, DT_MAL: true } });
                return;
            }
            let NumChecked = 0;
            let tipoActivo = [...this.state.tipoActivo];

            for (let i = 0; i < this.state.tipoActivo.length; i++) {
                let item = {
                    ...tipoActivo[i],
                    CANT_REP_VACIO: false,
                    CANT_REP_MAL: false,
                    CANT_MANT_VACIO: false,
                    CANT_MANT_MAL: false,
                    TARIFA_VACIO: false,
                    TARIFA_MAL: false,
                    HORAS_VACIO: false,
                    HORAS_MAL: false
                };
                tipoActivo[i] = item;
            }

            this.setState({ tipoActivo });

            for (let i = 0; i < this.state.tipoActivo.length; i++) {
                if (this.state.tipoActivo[i].checked) {
                    NumChecked++;

                    if (this.state.tipoActivo[i].capacidad_reparacion === null || this.state.tipoActivo[i].capacidad_reparacion === "") {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], CANT_REP_VACIO: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (!(/\S/.test(this.state.tipoActivo[i].capacidad_reparacion)) || isNaN(this.state.tipoActivo[i].capacidad_reparacion) || String(this.state.tipoActivo[i].capacidad_reparacion).includes("-") || String(this.state.tipoActivo[i].capacidad_reparacion).includes(".")) {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], CANT_REP_MAL: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (this.state.tipoActivo[i].capacidad_mantenimiento === null || this.state.tipoActivo[i].capacidad_mantenimiento === "") {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], CANT_MANT_VACIO: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (!(/\S/.test(this.state.tipoActivo[i].capacidad_mantenimiento)) || isNaN(this.state.tipoActivo[i].capacidad_mantenimiento) || String(this.state.tipoActivo[i].capacidad_mantenimiento).includes("-") || String(this.state.tipoActivo[i].capacidad_mantenimiento).includes(".")) {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], CANT_MANT_MAL: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (this.state.tipoActivo[i].tarifa === null || this.state.tipoActivo[i].tarifa == "" || this.state.tipoActivo[i].tarifa == 0 || this.state.tipoActivo[i].tarifa === undefined) {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], TARIFA_VACIO: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (!(/\S/.test(this.state.tipoActivo[i].tarifa)) || isNaN(this.state.tipoActivo[i].tarifa) || String(this.state.tipoActivo[i].tarifa).includes("-")) {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], TARIFA_MAL: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (this.state.tipoActivo[i].tiempo_respuesta_est === null || this.state.tipoActivo[i].tiempo_respuesta_est == "" || this.state.tipoActivo[i].tiempo_respuesta_est == 0 || this.state.tipoActivo[i].tiempo_respuesta_est === undefined) {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], HORAS_VACIO: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (!(/\S/.test(this.state.tipoActivo[i].tiempo_respuesta_est)) || isNaN(this.state.tipoActivo[i].tiempo_respuesta_est) || String(this.state.tipoActivo[i].tiempo_respuesta_est).includes("-")) {
                        let tipoActivo = [...this.state.tipoActivo];
                        let item = { ...tipoActivo[i], HORA_MAL: true };
                        tipoActivo[i] = item;
                        this.setState({ tipoActivo });
                        return;
                    }

                    if (Number(this.state.tipoActivo[i].capacidad_reparacion) + Number(this.state.tipoActivo[i].capacidad_reparacion) == 0) {
                        this.setState({ validation: { ...this.state.validation, NO_CAPACIDAD: true } });
                        return;
                    }

                }

                if ((NumChecked == 0) && (i == this.state.tipoActivo.length - 1)) {
                    this.setState({ validation: { ...this.state.validation, NO_CHCK: true } });
                    return;
                }
            }

            let j = 0;
            let listaTipo = [];
            let auxMant = 0;
            let auxRep = 0;
            for (let i = 0; i < this.state.tipoActivo.length; i++) {
                if (this.state.tipoActivo[i].checked == true) {
                    auxMant = auxMant + parseInt(this.state.tipoActivo[i].capacidad_mantenimiento);
                    auxRep = auxRep + parseInt(this.state.tipoActivo[i].capacidad_reparacion);
                    listaTipo[j] = this.state.tipoActivo[i];
                    j++;
                }
            }

            this.setState({ totMant: auxMant, totRep: auxRep });

            let params = {
                calificacion_contrato: 10,
                fecha_inicio: this.state.dateIni,
                fecha_fin: this.state.dateFin,
                capacidad_total_mant: this.state.totMant,
                capacidad_total_rep: this.state.totRep,
                activo: true,
                observaciones: this.state.observaciones,
                proveedor: {
                    id: this.state.idProv
                },
                contratos_x_tipo_de_activos: listaTipo,
            }

            axios({
                method: 'post',
                url: '/contrato/crear',
                data: params,
            }).then((response) => {
                if (response.status === 200) window.location.replace('/contrato');
            }).catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log("Response ERROR");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log("Request ERROR");
                    console.log(error.request);
                    console.log('Error', error.message);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
        })
    }

    borrarContrato = () => {
        this.closeEliminar();
        axios({
            method: 'post',
            url: '/contrato/borrar',
            data: {
                id: this.state.idActual
            }
        }).then((response) => {
            if (response.status === 200) window.location.replace('/contrato');
        }).catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("Response ERROR");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("Request ERROR");
                console.log(error.request);
                console.log('Error', error.message);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    render() {
        const { activePage } = this.state.activePage;
        const { isLoadingProv, valueProv, resultsProv, closeOnEscape, closeOnDimmerClick } = this.state
        return (
            <div style={{ padding: 50 }}>
                <Grid columns={2}>
                    <Grid.Column>
                        <h1>Contratos SLA</h1>
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button color='green' onClick={() => this.showModalCrear()}>Registrar nuevo Contrato</Button>
                    </Grid.Column>
                </Grid>
                <div style={{ paddingTop: 20 }}>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>ID</Table.HeaderCell>
                                <Table.HeaderCell>PROVEEDOR</Table.HeaderCell>
                                <Table.HeaderCell>FECHA INICIO</Table.HeaderCell>
                                <Table.HeaderCell>FECHA FIN</Table.HeaderCell>
                                <Table.HeaderCell>OBSERVACIONES</Table.HeaderCell>
                                <Table.HeaderCell>CALIFICACIÓN</Table.HeaderCell>
                                <Table.HeaderCell>ACCIONES</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.pageContent(this.state.activePage)}
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='7' style={{ textAlign: 'right' }}>
                                    <Pagination
                                        defaultActivePage={this.state.activePage}
                                        activePage={activePage}
                                        onPageChange={this.handlePaginationChange}
                                        totalPages={this.state.totalPages}
                                    />
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>

                    <Modal size={"large"} open={this.state.openCrear} onClose={this.closeCrear}>
                        <Modal.Header>Nuevo Contrato SLA</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group inline>
                                    <label style={styles.label}>Proveedor*</label>
                                    <Search
                                        fluid label='Proveedor*'
                                        loading={isLoadingProv}
                                        onResultSelect={this.handleResultSelectProv}
                                        onSearchChange={_.debounce(this.handleSearchChangeProv, 500, {
                                            leading: true,
                                        })}
                                        results={resultsProv}
                                        value={valueProv}
                                    />
                                    {this.state.validation.NO_PROV &&
                                        <Label basic color='red' pointing='left'>
                                            Debe seleccionar un proveedor
                                        </Label>
                                    }
                                    <div style={{ ...styles.label, paddingLeft: 15 }}>
                                        <b>Razón Social: </b>
                                    </div>
                                    <div style={{ paddingLeft: 15 }}>
                                        {this.state.selectedProv.razon_social}
                                    </div>
                                    <div style={{ ...styles.label }}>
                                        <b>RUC: </b>
                                    </div>
                                    <div style={{ paddingLeft: 15 }}>
                                        {this.state.selectedProv.ruc}
                                    </div>
                                </Form.Group>
                                <Form.Group inline>
                                    <label style={styles.label}>Fecha Inicio*</label>
                                    <DateInput
                                        name="date"
                                        placeholder="Fecha Inicio*"
                                        value={this.state.dateIni}
                                        iconPosition="left"
                                        onChange={this.handleChangeIni}
                                    />
                                    {this.state.validation.NO_DTINI &&
                                        <Label basic color='red' pointing='left'>
                                            Fecha inválida
                                        </Label>
                                    }
                                    <label style={styles.label}>Fecha Fin*</label>
                                    <DateInput
                                        name="date"
                                        placeholder="Fecha Fin*"
                                        value={this.state.dateFin}
                                        iconPosition="left"
                                        onChange={this.handleChangeFin}
                                    />
                                    {this.state.validation.NO_DTFIN &&
                                        <Label basic color='red' pointing='left'>
                                            Fecha inválida
                                        </Label>
                                    }
                                    {this.state.validation.DT_MAL &&
                                        <Label basic color='red' pointing='left'>
                                            Rango de fechas inválido
                                        </Label>
                                    }
                                </Form.Group>
                                <Form.Group inline>
                                    <label style={styles.label}>Contenido*</label>
                                    {this.state.validation.NO_CAPACIDAD &&
                                        <Label basic color='red' pointing='above'>
                                            Uno de los tipos seleccionados tiene capacidad nula en ambos servicios
                                        </Label>
                                    }
                                    {this.state.validation.NO_CHCK &&
                                        <Label basic color='red' pointing='left'>
                                            Debe seleccionar al menos 1 tipo
                                        </Label>
                                    }
                                    <Table celled>
                                        <Table.Header>
                                            <Table.Row>
                                                <Table.HeaderCell>Tipo Activo</Table.HeaderCell>
                                                <Table.HeaderCell>Incluido</Table.HeaderCell>
                                                <Table.HeaderCell>Capacidad Reparacion</Table.HeaderCell>
                                                <Table.HeaderCell>Capacidad Mantenimiento</Table.HeaderCell>
                                                <Table.HeaderCell>Tarifa/Hora(PEN)</Table.HeaderCell>
                                                <Table.HeaderCell>Tiempo Rpta(H)</Table.HeaderCell>
                                            </Table.Row>
                                        </Table.Header>

                                        <Table.Body>
                                            {this.pageContent2()}
                                        </Table.Body>
                                    </Table>
                                </Form.Group>

                                <Form.Group inline>
                                    <label style={{ ...styles.label, alignSelf: 'start' }}>Observaciones</label>
                                    <div style={{ width: '90%' }}>
                                        <textarea
                                            placeholder='Coloque observaciones en caso hubieran...'
                                            maxLength={255}
                                            onChange={(event) => this.setState({ observaciones: event.target.value })}
                                        />
                                    </div>
                                </Form.Group>

                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeCrear}>Cancelar</Button>
                            <Button
                                positive
                                icon='checkmark'
                                labelPosition='right'
                                content='Confirmar'
                                onClick={this.confirmarSolicitud}
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"mini"}
                        open={this.state.openEliminar}
                        closeOnEscape={closeOnEscape}
                        closeOnDimmerClick={closeOnDimmerClick}
                        onClose={this.closeEliminar}>
                        <Modal.Header>Confirmación de Eliminación</Modal.Header>
                        <Modal.Content>
                            <p>¿Seguro que quiere eliminar este contrato?</p>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button onClick={this.closeEliminar} negative>Cerrar</Button>
                            <Button
                                onClick={this.borrarContrato}
                                positive
                                labelPosition='right'
                                icon='checkmark'
                                content='Confirmar'
                            />
                        </Modal.Actions>
                    </Modal>

                    <Modal size={"large"} open={this.state.openEditar} onClose={this.closeVer}>
                        <Modal.Header>Visualizar Contrato SLA</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Group inline>
                                    <label style={styles.label}>Proveedor: </label>
                                    <Input
                                        disabled
                                        value={this.state.contratoActual == null ? null : this.state.contratoActual.proveedor.razon_social}
                                    />
                                    <label style={styles.label}>RUC: </label>
                                    <Input
                                        disabled
                                        value={this.state.contratoActual == null ? null : this.state.contratoActual.proveedor.ruc}
                                    />
                                </Form.Group>
                                <Form.Group inline>
                                    <label style={styles.label}>Fecha Inicio: </label>
                                    <Input
                                        disabled
                                        value={this.state.contratoActual == null ? null : this.state.contratoActual.fecha_inicio.substring(0, 10)}
                                    />
                                    <label style={styles.label}>Fecha Fin: </label>
                                    <Input
                                        disabled
                                        value={this.state.contratoActual == null ? null : this.state.contratoActual.fecha_fin.substring(0, 10)}
                                    />
                                </Form.Group>
                                <Form.Group inline>
                                    <label style={styles.label}>Contenido*</label>
                                    {this.state.validation.NO_CAPACIDAD &&
                                        <Label basic color='red' pointing='above'>
                                            Uno de los tipos seleccionados tiene capacidad nula en ambos servicios
                                        </Label>
                                    }
                                    {this.state.validation.NO_CHCK &&
                                        <Label basic color='red' pointing='left'>
                                            Debe seleccionar al menos 1 tipo
                                        </Label>
                                    }
                                    <Table celled>
                                        <Table.Header>
                                            <Table.Row>
                                                <Table.HeaderCell>Tipo Activo</Table.HeaderCell>
                                                <Table.HeaderCell>Capacidad Reparacion</Table.HeaderCell>
                                                <Table.HeaderCell>Capacidad Mantenimiento</Table.HeaderCell>
                                                <Table.HeaderCell>Tarifa/Hora(PEN)</Table.HeaderCell>
                                                <Table.HeaderCell>Tiempo Rpta(H)</Table.HeaderCell>
                                            </Table.Row>
                                        </Table.Header>

                                        <Table.Body>
                                            {this.pageContent3()}
                                        </Table.Body>
                                    </Table>
                                </Form.Group>

                                <Form.Group inline>
                                    <label style={{ ...styles.label, alignSelf: 'start' }}>Observaciones</label>
                                    <div style={{ width: '90%' }}>
                                        <textarea
                                            disabled
                                            value={this.state.contratoActual == null ? null : this.state.contratoActual.observaciones}
                                        />
                                    </div>
                                </Form.Group>
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button negative onClick={this.closeVer}>Cerrar</Button>
                        </Modal.Actions>
                    </Modal>
                </div>
            </div>
        )
    }


}

const styles = {
    label: {
        width: '10%',
        textAlign: 'right'
    },
    selectedDiv: {
        marginRight: 10,
        marginLeft: 10,
        width: '20%'
    }
}
export default ContratoView;