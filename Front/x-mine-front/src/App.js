import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';
import Layout from './layout/Layout';
import MainContext from './context/MainContext';
import DashboardView from './views/Dashboard/DashboardView';
import ReporteCostoView from './views/Dashboard/ReporteCostoView';
import ReporteCapacidadView from './views/Dashboard/ReporteCapacidadView';
import ReporteCapacidadProvsView from './views/Dashboard/ReporteCapacidadProvsView';
import ProveedorView from './views/Proveedor/ProveedorView';
import ActivoView from './views/Activo/ActivoView';
import ContratoView from './views/Contrato/ContratoView';
import LoginView from './views/Login/LoginView';
import SolicitudSwitch from './views/Solicitud/SolicitudSwitch';
import Home from './views/Home';
import PlanesManteminientoView from './views/PlanesMantenimiento/PlanesMantenimientoView';
import UsuariosView from './views/Usuarios/UsuariosView';


class App extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        const { cookies } = props;

        this.state = {
            logged: cookies.get('logged') || false
        };
        console.log(this.state.logged)
    }


    render() {
        require('dotenv').config();
        const { cookies } = this.props;
        if (cookies.get('logged') != 'true') {
            return (
                <Router>
                    <Route exact path="/">
                        <LoginView></LoginView>
                    </Route>
                </Router>
            );
        } else if (cookies.get('logged') == 'true') {
            return (
                <MainContext>
                    <Router>
                        <Layout>
                            <Switch>
                                <Route exact path="/">
                                    <Home />
                                </Route>
                                <Route path="/solicitud">
                                    <SolicitudSwitch></SolicitudSwitch>
                                </Route>
                                <Route path="/proveedor">
                                    <ProveedorView></ProveedorView>
                                </Route>
                                <Route path="/activo">
                                    <ActivoView></ActivoView>
                                </Route>
                                <Route path="/contrato">
                                    <ContratoView></ContratoView>
                                </Route>
                                <Route path="/planes">
                                    <PlanesManteminientoView></PlanesManteminientoView>
                                </Route>
                                <Route path="/usuarios">
                                    <UsuariosView></UsuariosView>
                                </Route>
                                <Route path="/dashboard">
                                    <DashboardView></DashboardView>
                                </Route>
                                <Route path="/reporteCostos">
                                    <ReporteCostoView></ReporteCostoView>
                                </Route>
                                <Route path="/reporteCapacidad">
                                    <ReporteCapacidadView></ReporteCapacidadView>
                                </Route>
                                <Route path="/reporteCapacidadProvs">
                                    <ReporteCapacidadProvsView></ReporteCapacidadProvsView>
                                </Route>
                            </Switch>




                        </Layout>
                    </Router>
                </MainContext>
            );
        }
    }
}

export default withCookies(App);
