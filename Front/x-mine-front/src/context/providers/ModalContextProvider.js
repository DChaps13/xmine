import React, { Component, createContext } from 'react';

export const ModalContext = createContext();

export default class ModalContextProvider extends Component {
    state = {
        modal: 'NONE'
    }

    toggleModal = (modalCOD) => {
        console.log(modalCOD);
        this.setState({modal: modalCOD});
        
    }

    render() {
        return (
            <ModalContext.Provider value={{...this.state, toggleModal: this.toggleModal}}>
                {this.props.children}
            </ModalContext.Provider>
        )
    }
}
