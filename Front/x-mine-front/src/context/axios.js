import axios from 'axios';

export default axios.create({
    baseURL: 'http://ec2-3-218-41-109.compute-1.amazonaws.com:5002',
    timeout: 5000,
    headers:{
        'X-Requested-With': 'XMLHttpRequest'
    },
});