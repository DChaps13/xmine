import React, { Component } from 'react'

import ModalContextProvider from './providers/ModalContextProvider';

export default class MainContext extends Component {
    render() {
        return (
            <ModalContextProvider>
                {this.props.children}
            </ModalContextProvider>
        )
    }
}
