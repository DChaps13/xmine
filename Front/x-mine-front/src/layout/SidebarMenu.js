import React, { Component } from 'react';
import { Icon, Menu, Sidebar } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

class SidebarMenu extends Component {
    
    state={
        activeItem: window.location.pathname
    }

    changeActiveItem = () => {
        this.setState({activeItem: window.location.pathname});
    }

    render() {
        return (
            <Sidebar.Pushable>
                <Sidebar
                    as={Menu}
                    animation='push'
                    icon='labeled'
                    inverted
                    vertical
                    visible={this.props.menuIsOpen}
                    width='thin'>
                    
                    <Menu.Item as={NavLink} to='/solicitud'
                        style={{ marginTop: '10vh' }}>
                        <Icon name='file alternate' />
                        Solicitudes
                    </Menu.Item>
                    
                    <Menu.Item as={NavLink} to='/proveedor'>
                        <Icon name='user secret' />
                        Proveedores
                    </Menu.Item>

                    <Menu.Item as={NavLink} to='/activo'>
                        <Icon name='truck' />
                        Activos
                    </Menu.Item>

                    <Menu.Item as={NavLink} to='/contrato'>
                        <Icon name='handshake' />
                        Contratos
                    </Menu.Item>

                    <Menu.Item as={NavLink} to='/planes'>
                        <Icon name='wrench' />
                        Planes de Mantenimiento
                    </Menu.Item>

                    <Menu.Item as={NavLink} to='/usuarios'>
                        <Icon name='users' />
                        Usuarios
                    </Menu.Item>

                    <Menu.Item as={NavLink} to='/dashboard'>
                        <Icon name='line graph' />
                        Reportes
                    </Menu.Item>

                </Sidebar>

                <Sidebar.Pusher>
                    {this.props.children}
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
}

export default SidebarMenu;
