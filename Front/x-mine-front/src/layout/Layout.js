import React, { Component, Fragment } from 'react';

import Topbar from './Topbar';
import SidebarMenu from './SidebarMenu';
import ModalSelector from './ModalSelector';


class Layout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuIsOpen: false
        }
        this.switchMenu = this.switchMenu.bind(this);
    }

    switchMenu() {
        this.setState({ menuIsOpen: !this.state.menuIsOpen });
    }

    render() {
        return (
            <Fragment>
                <Topbar updateMenuState={this.switchMenu} menuIsOpen={this.state.menuIsOpen} />
                <SidebarMenu menuIsOpen={this.state.menuIsOpen}>
                    <div style={{ marginTop: '10vh', minHeight: '90vh', marginRight: this.state.menuIsOpen ? '150px' : 0 }}>
                        {this.props.children}
                    </div>
                </SidebarMenu>
                <ModalSelector></ModalSelector>
            </Fragment>
        );
    }
}

export default Layout;
