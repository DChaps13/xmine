import React, { Component } from 'react';
import { Menu, Icon, Image, Modal, Button, Form } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { withCookies } from 'react-cookie';

import { ModalContext } from '../context/providers/ModalContextProvider';
import axios from '../context/axios';

const tipos_usuario = [
    { key: '1', text: 'Administrador', value: 1 },
    { key: '2', text: 'Supervisor', value: 2 },
    { key: '3', text: 'Proveedor', value: 3 },
    { key: '4', text: 'Operario', value: 4 },
]

class TopBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openLogout: false,
            openUserInfo: false,
            userActual: this.props.cookies.get("usuario"),
            cambiarContraseña: false,
            contrasena_actual: null,
            contrasena_nueva: null,
            repetir_contrasena_nueva: null,
            validation: {
                USERNAME_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_ACTUAL: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            }
        }

        if (!this.props.cookies.get("usuario")) {
            console.log("Logout por innacceso por cookies")
            alert("Logout por innacceso por cookies");
            this.logout();
        }
    }

    showModalUserInfo = (p) => {
        this.setState({
            validation: {
                USERNAME_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_ACTUAL: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            },
            openUserInfo: true
        });

    }

    closeUserInfo = () => {
        this.setState({
            validation: {
                USERNAME_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_ACTUAL: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            }
        });
        this.setState({ cambiarContraseña: false });
        this.setState({ openUserInfo: false });
    }

    editNombre = (event, data) => {
        this.setState({ userActual: { ...this.state.userActual, nombre: data.value } });
        this.setState({ nombre: data.value });
    }

    editApellidoMaterno = (event, data) => {
        this.setState({ userActual: { ...this.state.userActual, apellido_materno: data.value } });
        this.setState({ apellido_materno: data.value });
    }

    editApellidoPaterno = (event, data) => {
        this.setState({ userActual: { ...this.state.userActual, apellido_paterno: data.value } });
        this.setState({ apellido_paterno: data.value });
    }

    editUsername = (event, data) => {
        this.setState({ userActual: { ...this.state.userActual, username: data.value } });
        this.setState({ username: data.value });
    }

    editContrasena = (event, data) => {
        this.setState({ userActual: { ...this.state.userActual, contrasena: data.value } });
        this.setState({ contrasena: data.value });
    }

    editarusuario = () => {
        this.setState({
            validation: {
                USERNAME_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_ACTUAL: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            }
        }, () => {
            if (this.state.userActual.username === null || this.state.userActual.username === "") {
                this.setState({ validation: { ...this.state.validation, USERNAME_VACIO: true } });
                return;
            }


            var contrasenaNueva;
            if (this.state.cambiarContraseña) {
                if (this.state.userActual.contrasena != this.state.contrasena_actual) {
                    this.setState({ validation: { ...this.state.validation, CONTRASENA_ACTUAL: true } });
                    return;
                }


                if (this.state.contrasena_nueva === null || this.state.contrasena_nueva === "") {
                    this.setState({ validation: { ...this.state.validation, CONTRASENA_VACIO: true } });
                    return;
                }

                if (this.state.contrasena_nueva.length < 5) {
                    this.setState({ validation: { ...this.state.validation, CONTRASENA_CORTA: true } });
                    return;
                }

                if (this.state.contrasena_nueva != this.state.repetir_contrasena_nueva) {
                    this.setState({ validation: { ...this.state.validation, CONTRASENA_DIFERENTE: true } });
                    return;
                }
                contrasenaNueva = this.state.contrasena_nueva;
            } else {
                contrasenaNueva = this.state.userActual.contrasena;
            }

            let params = {
                id: this.state.userActual.id,
                admin: 1,
                apellido_materno: this.state.userActual.apellido_materno,
                apellido_paterno: this.state.userActual.apellido_paterno,
                nombre: this.state.userActual.nombre,
                rol: this.state.userActual.rol,
                activo: true,
                username: this.state.userActual.username,
                contrasena: contrasenaNueva
            };

            axios({
                method: 'post',
                url: '/usuario/actualizar',
                data: params
            }).then((response) => {
                this.setState({ openUserInfo: false });
            })

            var prov = this.state.userActual.proveedor;
            this.props.cookies.set('usuario', {
                id: this.state.userActual.id,
                nombre: this.state.userActual.nombre,
                rol: this.state.userActual.rol,
                username: this.state.userActual.username,
                contrasena: contrasenaNueva,
                apellido_materno: this.state.userActual.apellido_materno,
                apellido_paterno: this.state.userActual.apellido_paterno,
                proveedor: prov
            });
        })
    }

    funVerificar = () => {
        this.setState({
            validation: {
                USERNAME_VACIO: false,
                CONTRASENA_VACIO: false,
                CONTRASENA_ACTUAL: false,
                CONTRASENA_CORTA: false,
                CONTRASENA_DIFERENTE: false
            }
        });
        this.setState(({ cambiarContraseña }) => ({ cambiarContraseña: !cambiarContraseña }))
    }

    logout = () => {
        const { cookies } = this.props;
        cookies.set('logged', 'false')
        window.location.replace('/');
    }
    render() {
        return (
            <ModalContext.Consumer>{(modalContext) => {
                const { toggleModal } = modalContext;
                return (
                    <Menu fixed='top' inverted style={{ height: '10vh' }}>
                        {this.props.cookies.get("usuario").rol !== 3 &&
                            <Menu.Item name='topbar' onClick={this.props.updateMenuState}>
                                <Icon name='align justify' />
                            </Menu.Item>
                        }

                        <Menu.Item as={Link} to='/'>
                            <Image src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Logo_of_X_%28company%29.svg/1200px-Logo_of_X_%28company%29.svg.png' size='mini' />
                            <div style={{ fontWeight: 'bold', fontSize: 16, marginLeft: 10 }}>
                                {this.props.cookies.get("usuario").rol === 3 ? 'Provider' : 'Supervisor'}
                            </div>

                        </Menu.Item>

                        <Menu.Item position='right' as={Link} to='/solicitud'
                            name='solicitudes'>
                            <div style={{ fontWeight: 'bold', fontSize: 16 }}>Solicitudes</div>
                        </Menu.Item>

                        {this.props.cookies.get("usuario").rol !== 3 &&
                            <Menu.Item
                                name='nuevaSolicitud'
                                onClick={() => toggleModal('NEW_SOLICITUD')}>
                                <div style={{ fontWeight: 'bold', fontSize: 16 }}>+ Nueva Solicitud</div>
                            </Menu.Item>
                        }

                        <Menu.Item onClick={() => this.setState({ openUserInfo: true })}
                            name='user'>
                            <Image src='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlK7C8QKWakDOTBVjBPeJ-XitjrkIEGva8J3c-1sNn5VO6nt-7' size='mini' circular />
                        </Menu.Item>

                        <Menu.Item onClick={() => this.setState({ openLogout: true })}
                            name='logout'>
                            <Icon name='sign-out alternate' size='large' />
                        </Menu.Item>


                        <Modal size={"mini"}
                            open={this.state.openLogout}
                            closeOnEscape={true}
                            closeOnDimmerClick={false}
                            onClose={this.closeEliminar}>
                            <Modal.Header>Confirmación de salida</Modal.Header>
                            <Modal.Content>
                                <p>¿Seguro que quiere salir de X-Mine?</p>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button onClick={() => this.setState({ openLogout: false })} negative>Cancelar</Button>
                                <Button
                                    onClick={this.logout}
                                    positive
                                    labelPosition='right'
                                    icon='checkmark'
                                    content='Confirmar'
                                />
                            </Modal.Actions>
                        </Modal>


                        <Modal size={"small"} open={this.state.openUserInfo} closeOnDimmerClick={false} onClose={this.closeUserInfo}>
                            <Modal.Header>Información de mi cuenta</Modal.Header>
                            <Modal.Content>
                                <Form>
                                    <Form.Group>
                                        <Form.Input fluid label='Nombre'
                                            placeholder='Gabriel'
                                            onChange={(event, data) => this.editNombre(event, data)}
                                            value={this.state.userActual == null ? null : this.state.userActual.nombre}
                                            width={5} />
                                        <Form.Input fluid label='Apellido Paterno'
                                            placeholder='García'
                                            onChange={(event, data) => this.editApellidoPaterno(event, data)}
                                            value={this.state.userActual == null ? null : this.state.userActual.apellido_paterno}
                                            width={5} />
                                        <Form.Input fluid label='Apellido Materno'
                                            placeholder='Márquez'
                                            onChange={(event, data) => this.editApellidoMaterno(event, data)}
                                            value={this.state.userActual == null ? null : this.state.userActual.apellido_materno}
                                            width={5} />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Input required fluid label='Nombre de usuario'
                                            placeholder='gmarquez100'
                                            onChange={(event, data) => this.editUsername(event, data)}
                                            error={this.state.validation.USERNAME_VACIO && { content: 'Campo obligatorio' }}
                                            value={this.state.userActual == null ? null : this.state.userActual.username}
                                            width={7} />
                                        <Form.Input fluid label='Rol'
                                            readOnly={true}
                                            placeholder={tipos_usuario[this.state.userActual.rol - 1].text}
                                            width={7} />
                                    </Form.Group>
                                    <Form.Group inline>
                                        <label>Cambiar contraseña:</label>
                                        <Form.Radio
                                            toggle onChange={() => this.funVerificar()} />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Input required fluid label='Contraseña actual'
                                            disabled={!this.state.cambiarContraseña}
                                            type='password'
                                            placeholder='Contraseña100'
                                            onChange={(event, data) => this.setState({ contrasena_actual: data.value })}
                                            error={this.state.validation.CONTRASENA_ACTUAL && { content: 'Debe ingresar su contraseña actual' }}
                                            width={7} />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Input required fluid label='Contraseña nueva'
                                            disabled={!this.state.cambiarContraseña}
                                            type='password'
                                            placeholder='ContraseñaNueva100'
                                            onChange={(event, data) => this.setState({ contrasena_nueva: data.value })}
                                            error={(this.state.validation.CONTRASENA_VACIO && { content: 'Campo obligatorio' }) ||
                                                (this.state.validation.CONTRASENA_CORTA && { content: 'Debe ser de por lo menos 5 caracteres' })}
                                            width={7} />
                                        <Form.Input required fluid label='Confirmar contraseña nueva'
                                            disabled={!this.state.cambiarContraseña}
                                            type='password'
                                            placeholder='ContraseñaNueva100'
                                            onChange={(event, data) => this.setState({ repetir_contrasena_nueva: data.value })}
                                            error={this.state.validation.CONTRASENA_DIFERENTE && { content: 'Debe ingresar la misma contraseña' }}
                                            width={7} />
                                    </Form.Group>
                                </Form>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button negative onClick={this.closeUserInfo}>Regresar</Button>
                                <Button
                                    positive
                                    icon='checkmark'
                                    labelPosition='right'
                                    content='Actualizar'
                                    onClick={this.editarusuario}
                                />
                            </Modal.Actions>
                        </Modal>
                    </Menu>
                )
            }}</ModalContext.Consumer>
        )

    }
}

export default withCookies(TopBar);