import React, { Component, Fragment } from 'react'

import { ModalContext } from '../context/providers/ModalContextProvider';

import ModalNuevaSolicitud from '../components/Solicitud/ModalNuevaSolicitud';

export default class ModalSelector extends Component {
    
    render() {
        return (
            <ModalContext.Consumer>{(modalContext) => {
                switch (modalContext.modal) {
                    case 'NEW_SOLICITUD':
                        return <ModalNuevaSolicitud></ModalNuevaSolicitud>
                    default:
                        return <Fragment></Fragment>
                }
            }}
            </ModalContext.Consumer>
        )
    }
}
