TRUNCATE TABLE solicitud;
TRUNCATE TABLE plan_de_mantenimiento;
TRUNCATE TABLE activo;
TRUNCATE TABLE contrato_x_tipo_de_activo;
TRUNCATE TABLE contrato;
TRUNCATE TABLE proveedor;
TRUNCATE TABLE estado;
TRUNCATE TABLE tipo_de_activo;
TRUNCATE TABLE usuario;
TRUNCATE TABLE log_de_estados;

INSERT INTO tipo_de_activo (criticidad,nombre_tipo)
VALUES (2,'transporte ejecutivo');
INSERT INTO tipo_de_activo (criticidad,nombre_tipo)
VALUES (1,'transporte planta');
INSERT INTO tipo_de_activo (criticidad,nombre_tipo)
VALUES (2,'transporte terceros');
INSERT INTO tipo_de_activo (criticidad,nombre_tipo)
VALUES (3,'transporte minerales');
INSERT INTO tipo_de_activo (criticidad,nombre_tipo)
VALUES (4,'explotacion minerales');

INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Toyota','Rav4',1600,'AFK-365',160,1);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Toyota','Rav4',1600,'ABC-365',160,1);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Mercedes-Benz','Sprinter',5000,'AFK-366',160,2);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Mercedes-Benz','Sprinter',5000,'ABC-366',160,2);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Toyota','Corolla',1350,'AFK-367',160,3);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Toyota','Corolla',1350,'ABC-367',160,3);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Mercedes-Benz','Actros',18000,'AFK-368',310,4);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'Mercedes-Benz','Actros',18000,'ABC-368',310,4);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'CATERPILLAR','907M AG HANDLER ',6000,'AFK-369',160,5);
INSERT INTO activo (estado,horas_uso,marca,modelo,peso_bruto,placa,potencia,id_tipo_de_activo)
VALUES (1,0,'CATERPILLAR','907M AG HANDLER ',6000,'ABC-369',160,5);

INSERT INTO proveedor (calificacion,direccion,estado,nombre_contacto,razon_social,ruc,telefono_contacto,ubigeo,bloqueado,cantidad_calificado)
VALUES (10,'av. tacna 231',1,'Luis Arana','Cuellos Blancos','12345678912','944441181','Lima-Lima',0,0);
INSERT INTO proveedor (calificacion,direccion,estado,nombre_contacto,razon_social,ruc,telefono_contacto,ubigeo,bloqueado,cantidad_calificado)
VALUES (10,'av. tacna 232',1,'Karla Pedraza','El matriarcado','12345678913','944441182','Lima-Lima',0,0);
INSERT INTO proveedor (calificacion,direccion,estado,nombre_contacto,razon_social,ruc,telefono_contacto,ubigeo,bloqueado,cantidad_calificado)
VALUES (10,'av. tacna 233',1,'Gina Bustamante','Los intocables','12345678914','944441183','Lima-Lima',0,0);
INSERT INTO proveedor (calificacion,direccion,estado,nombre_contacto,razon_social,ruc,telefono_contacto,ubigeo,bloqueado,cantidad_calificado)
VALUES (10,'av. tacna 234',1,'Fabricio Monsalve','Los mineros de Sipan','12345678915','944441184','Lima-Lima',0,0);

INSERT INTO estado (nombre_estado)
VALUES ('REGISTRADA');
INSERT INTO estado (nombre_estado)
VALUES ('ASIGNADA');
INSERT INTO estado (nombre_estado)
VALUES ('EN ATENCION');
INSERT INTO estado (nombre_estado)
VALUES ('ATENDIDA');
INSERT INTO estado (nombre_estado)
VALUES ('RECHAZADA');
INSERT INTO estado (nombre_estado)
VALUES ('CANCELADA');
INSERT INTO estado (nombre_estado)
VALUES ('CERRADA');


INSERT INTO usuario (activo,admin,apellido_paterno, apellido_materno,nombre,contrasena,rol,username)
VALUES (1,1,'Chapi','Alejo','Daniel','123456',1,'admin01');
INSERT INTO usuario (activo,admin,apellido_paterno, apellido_materno,nombre,contrasena,rol,username)
VALUES (1,1,'Paucarpoma','Silva','Carlos','123456',2,'sup01');
INSERT INTO usuario (activo,admin,apellido_paterno, apellido_materno,nombre,contrasena,rol,username)
VALUES (1,1,'Carrillo','Martinez','Carlos','123456',4,'op01');
INSERT INTO usuario(activo,admin,contrasena,rol,username,id_proveedor)
VALUES (1,0,'123456',3,'12345678912',1);
INSERT INTO usuario(activo,admin,contrasena,rol,username,id_proveedor)
VALUES (1,0,'123456',3,'12345678913',2);
INSERT INTO usuario(activo,admin,contrasena,rol,username,id_proveedor)
VALUES (1,0,'123456',3,'12345678914',3);
INSERT INTO usuario(activo,admin,contrasena,rol,username,id_proveedor)
VALUES (1,0,'123456',3,'12345678915',4);

insert into contrato (calificacion_contrato,capacidad_total_mant,capacidad_total_rep,fecha_fin,fecha_inicio,observaciones,id_proveedor,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,40,50,'2019-12-30','2019-10-01','no observaciones',1,40,50,90,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,15,25,3000,10,1,2,10,15,25,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,10,25,2000,5,1,1,15,10,25,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,5,15,2000,5,1,3,5,15,20,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,10,10,5000,2,1,4,10,10,20,1,0);

insert into contrato (calificacion_contrato,capacidad_total_mant,capacidad_total_rep,fecha_fin,fecha_inicio,observaciones,id_proveedor,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,100,150,'2019-12-30','2019-10-01','no observaciones',2,100,150,250,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,20,45,3000,20,2,2,20,45,65,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,15,25,2000,10,2,1,15,25,40,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,30,30,2000,5,2,3,30,30,60,1,0);
insert into contrato_x_tipo_de_activo (calificacion_tipo,capacidad_mantenimiento,capacidad_reparacion,tarifa,tiempo_estimado_est,id_contrato,id_tipo_de_activo,capacidad_mantenimiento_inicial,capacidad_reparacion_inicial, capacidad_total_inicial,activo,cantidad_calificado)
values (10,35,50,5000,1,2,5,35,50,85,1,0);

