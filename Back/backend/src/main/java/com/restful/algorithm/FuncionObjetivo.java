package com.restful.algorithm;

import java.util.ArrayList;
import java.util.HashMap;

import com.restful.model.Contrato_x_tipo_de_activoModel;
import com.restful.model.SolicitudModel;
public class FuncionObjetivo {
//    public ArrayList<Solicitud> LstSol;
//    public ArrayList<Contrato> LstCont;
//    public char tipo;

    private class ClonCantidad {
        int cantidad_mantenimiento;
        int cantidad_reparacion;

        public ClonCantidad(int cantidad_mantenimiento, int cantidad_reparacion) {
            this.cantidad_mantenimiento = cantidad_mantenimiento;
            this.cantidad_reparacion = cantidad_reparacion;
        }
    }


    public double gain(ArrayList<SolicitudModel> LstSol, ArrayList<Contrato_x_tipo_de_activoModel> LstCont, char tipo){
        ArrayList<Contrato_x_tipo_de_activoModel> LstClone = (ArrayList<Contrato_x_tipo_de_activoModel>)LstCont.clone();

        HashMap<String,ClonCantidad> cantidades = new HashMap<>();
        for (Contrato_x_tipo_de_activoModel c : LstCont) {
            String key = String.valueOf(c.getId())+ String.valueOf(c.getTipoActivo().getId());
            ClonCantidad cc = new ClonCantidad(c.getCapacidad_mantenimiento(), c.getCapacidad_reparacion());
            cantidades.put(key, cc);
        }
        
        double gain = 0;
        for (int i = 0; i < LstSol.size(); i++) {
            String key = String.valueOf(LstClone.get(i).getId()) + String.valueOf(LstClone.get(i).getTipoActivo().getId());

            int f_critic = LstSol.get(i).getComplejidad();
            int f_cant_disp = 0;
            if (tipo == 'R') f_cant_disp = cantidades.get(key).cantidad_reparacion;
            else f_cant_disp = cantidades.get(key).cantidad_mantenimiento;
            
            double f_calif = LstClone.get(i).getCalificacion_tipo();
            double f_tiempo = LstClone.get(i).getTiempo_respuesta_est();
            double tarifa = LstClone.get(i).getTarifa();
            
            gain += (f_critic/100.0) * f_cant_disp * (f_calif/10) / (f_tiempo/10 * tarifa/100);
            
            
            ClonCantidad temp = cantidades.get(key);
            if (tipo == 'R') temp.cantidad_reparacion--;
            else temp.cantidad_mantenimiento--;

            cantidades.put(key, temp);
            
        }
        return gain;
    }
}
