package com.restful.algorithm;

// Java program to implement Simulated Annealing 
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.restful.model.Contrato_x_tipo_de_activoModel;
import com.restful.model.SolicitudModel;
import com.restful.model.Tipo_de_activoModel;
import com.restful.repo.ActivoRepo;
import com.restful.service.ActivoService;
import com.restful.entity.Contrato_x_tipo_de_activo;
import com.restful.model.ActivoModel;
@Configurable
public class SimulatedAnnealing {
	//@Autowired
    //Qualifier("activoRepo")
    //private ActivoRepo activoRepo;
    // Initial and final temperature 
    public static double T = 1; 
    // Temperature at which iteration terminates 
    static final double Tmin = .0001; 
    // Decrease in temperature 
    static final double alpha = 0.9; 
    
    static final int numIterations = 100;
    
    private char tipo_mantenimiento;
    
    private FuncionObjetivo funcionObjetivo;
    

    public char getTipo_mantenimiento() {
		return tipo_mantenimiento;
	}

	public void setTipo_mantenimiento(char tipo_mantenimiento) {
		this.tipo_mantenimiento = tipo_mantenimiento;
	}

	public FuncionObjetivo getFuncionObjetivo() {
		return funcionObjetivo;
	}

	public void setFuncionObjetivo(FuncionObjetivo funcionObjetivo) {
		this.funcionObjetivo = funcionObjetivo;
	}

	public SimulatedAnnealing() {
    	
    }
    
    public SimulatedAnnealing(char tipo_mantenimiento) {
        this.tipo_mantenimiento = tipo_mantenimiento;
        this.funcionObjetivo = new FuncionObjetivo();
    }


    public ArrayList<Contrato_x_tipo_de_activoModel> solve(ArrayList<SolicitudModel> LstSol, ArrayList<Contrato_x_tipo_de_activoModel> LstCont,
    		ActivoRepo activoRepo){
        ArrayList<Contrato_x_tipo_de_activoModel> solution = new ArrayList<>();

        Random random = new Random();
        int len = LstCont.size();
        int num_sol = LstSol.size();

        for (int i = 0; i < num_sol; i++) {
            ArrayList<SolicitudModel> aux_solicitud = new ArrayList<>();
            ArrayList<Contrato_x_tipo_de_activoModel> aux_contrato = new ArrayList<>();
            aux_solicitud.add(LstSol.get(i));

       
            ActivoModel activo = new ActivoModel(activoRepo.findByIdAndEstado(LstSol.get(i).getId_activo(), true));
            Tipo_de_activoModel tipoVehiculo = activo.getTipo_activo();
            //System.out.println(tipoVehiculo);

            ArrayList<Contrato_x_tipo_de_activoModel> aux = new ArrayList<Contrato_x_tipo_de_activoModel>();
            for(Contrato_x_tipo_de_activoModel c : LstCont){
                if(c.getTipoActivo().getId()==tipoVehiculo.getId()){
                    aux.add(c);
                }
            }
            
            int choice = 0;
            //System.out.println("Procesando  1");
            
            choice = (int)(Math.random()*(aux.size()-1));

            Contrato_x_tipo_de_activoModel currentSol;
            if (aux.size()!=0){
                while(tipoVehiculo.getId() != aux.get(choice).getTipoActivo().getId()){
                    choice = Math.abs(random.nextInt()) % aux.size();
                }

                currentSol = aux.get(choice);

            }else{
                currentSol = new Contrato_x_tipo_de_activoModel();
                solution.add(currentSol);
                continue;
            }
            

            while (T > Tmin) {
                for (int j=0; j<numIterations; j++){
                	//System.out.println(j);
                    aux_contrato.add(currentSol);
                    double gain_currentSol = this.funcionObjetivo.gain(aux_solicitud,aux_contrato, this.tipo_mantenimiento);
                    aux_contrato.clear();

                    choice = (int)(Math.random()*(aux.size()-1));
                    while(tipoVehiculo.getId() != aux.get(choice).getTipoActivo().getId()){
                        choice = Math.abs(random.nextInt()) % aux.size();
                    }
                    Contrato_x_tipo_de_activoModel newSolution = aux.get(choice);

                    aux_contrato.add(newSolution);
                    double gain_newSolution = this.funcionObjetivo.gain(aux_solicitud,aux_contrato, this.tipo_mantenimiento);
                    aux_contrato.clear();

                    double ap = Math.pow(Math.E, (gain_newSolution - gain_currentSol)/T);
                    if (ap > Math.random()) currentSol = newSolution;
                }

                T *= alpha; // Decreases T, cooling phase
            }
            T = 1;

            /*System.out.print("Solicitud #");
            System.out.println(LstSol.get(i).id);
            System.out.print("Asignado al proveedor #");
            System.out.println(currentSol.id);*/

            solution.add(currentSol);
            //LstCont.remove(currentSol);
        }


        return solution;
    }

} 

