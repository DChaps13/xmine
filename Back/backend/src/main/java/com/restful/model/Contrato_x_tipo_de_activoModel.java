package com.restful.model;

import javax.persistence.Column;
import com.restful.entity.Contrato_x_tipo_de_activo;

public class Contrato_x_tipo_de_activoModel {
	private int id;
	
	private ContratoModel contrato;
	private double tarifa;
	
	private int capacidad_mantenimiento;
	
	private int capacidad_reparacion;
	
	private int tiempo_respuesta_est;
	
	private int cantidad_calificado;

	private double calificacion_tipo; 
	
	private Tipo_de_activoModel tipo_vehiculo;
	
	private boolean activo;

	public Contrato_x_tipo_de_activoModel(Contrato_x_tipo_de_activo p) {
		this.id = p.getId();
		this.contrato= new ContratoModel(p.getContrato());
		this.tarifa = p.getTarifa();
		this.calificacion_tipo = p.getCalificacion_tipo();
		this.capacidad_mantenimiento = p.getCapacidad_mantenimiento();
		this.capacidad_reparacion = p.getCapacidad_reparacion();
		this.tiempo_respuesta_est = p.getTiempo_respuesta_est();
		this.tipo_vehiculo = new Tipo_de_activoModel(p.getTipo_de_activo());
		this.activo = p.isActivo();
		this.cantidad_calificado = p.getCantidad_calificado();
	}

	public Contrato_x_tipo_de_activoModel(){
		
	}

	public Tipo_de_activoModel getTipoActivo(){
		return this.tipo_vehiculo;
	}

	public int getId(){
		return this.id;
	}

	public int getCapacidad_mantenimiento(){
		return this.capacidad_mantenimiento;
	}

	public int getCapacidad_reparacion(){
		return this.capacidad_reparacion;
	}

	public double getCalificacion_tipo(){
		return this.calificacion_tipo;
	}

	public int getTiempo_respuesta_est(){
		return this.tiempo_respuesta_est;
	}
	public double getTarifa(){
		return this.tarifa;
	}

	public ContratoModel getContrato(){
		return this.contrato;
	}

	public boolean isActivo(){
		return this.activo;
	}

	public int getCantidad_calificado() {
		return cantidad_calificado;
	}

	public void setCantidad_calificado(int cantidad_calificado) {
		this.cantidad_calificado = cantidad_calificado;
	}
}
