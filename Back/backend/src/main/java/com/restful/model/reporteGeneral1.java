package com.restful.model;

import java.util.ArrayList;
import java.util.List;

public class reporteGeneral1 {
	
	private ProveedorModel proveedor;
	
	private List<reporteCapacidad> lstTipoActivo = new ArrayList<reporteCapacidad>();
	
	private double porcentaje_uso_proveedor=0;

	public reporteGeneral1() {
		
	}
	
	public void addTipoActivo(reporteCapacidad r) {
		lstTipoActivo.add(r);
	}
	
	
	
	public void actualizarPorcentajeUsoProveedor() {
		int auxCap=0,auxUso=0;
		
		for (reporteCapacidad reporteCapacidadx : lstTipoActivo) {
			auxCap = auxCap + reporteCapacidadx.getTipo_solicitud_mant_cap() + reporteCapacidadx.getTipo_solicitud_repa_cap();
			auxUso = auxUso + reporteCapacidadx.getTipo_solicitud_mant_usado() + reporteCapacidadx.getTipo_solicitud_repa_usado();
		}
		//System.out.println(auxUso +","+auxCap);
		porcentaje_uso_proveedor = (double)auxUso/ auxCap;
	}
	
	
	public void addReporteCapacidad(reporteCapacidad r) {
		lstTipoActivo.add(r);
		
	}
	
	public ProveedorModel getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorModel proveedor) {
		this.proveedor = proveedor;
	}

	public List<reporteCapacidad> getLstTipoActivo() {
		return lstTipoActivo;
	}

	public void setLstTipoActivo(List<reporteCapacidad> lstTipoActivo) {
		this.lstTipoActivo = lstTipoActivo;
	}

	public double getPorcentaje_uso_proveedor() {
		return porcentaje_uso_proveedor;
	}

	public void setPorcentaje_uso_proveedor(double porcentaje_uso_proveedor) {
		this.porcentaje_uso_proveedor = porcentaje_uso_proveedor;
	}
	
	
	

}
