package com.restful.model;

import com.restful.entity.Estado;

public class EstadoModel {
	private int id;
	
	
	private String nombre;


	public EstadoModel() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	@Override
	public String toString() {
		return "EstadoModel [id=" + id + ", nombre=" + nombre + "]";
	}


	public EstadoModel(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	
	public EstadoModel(Estado e) {
		
		this.id = e.getId();
		this.nombre = e.getNombre();
	}

	public boolean equals(EstadoModel object2) {
		return this.id==object2.getId();
	}
	
}
