package com.restful.model;


import com.restful.entity.Usuario;

public class UsuarioModel {
	private int id;
	
	private String nombre;
	
	private String apellido_paterno;

	private String apellido_materno;
	
	private String username;

	private String contrasena;

	private boolean activo;

	private boolean admin;

	//1 -> admin ; 2 -> supervisor; 3 -> proveedor; 4 -> operario
	private int rol;
	
	private ProveedorModel proveedor;
	
	public UsuarioModel(Usuario u) {
		id = u.getId();
		nombre = u.getNombre();
		apellido_paterno = u.getApellido_paterno();
		apellido_materno = u.getApellido_materno();
		rol = u.getRol();
		activo = u.isActivo();
		username = u.getUsername();
		contrasena = u.getContrasena();
		if(u.getProveedor() != null)this.proveedor = new ProveedorModel(u.getProveedor());

	}
	
	public UsuarioModel() {

		// TODO Auto-generated constructor stub
	}

	public UsuarioModel(int id, String nombre, String apellido_paterno, String apellido_materno, String username,
			String contrasena, boolean activo, boolean admin, int rol, ProveedorModel proveedor) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		this.username = username;
		this.contrasena = contrasena;
		this.activo = activo;
		this.admin = admin;
		this.rol = rol;
		this.proveedor = proveedor;
	}

	@Override
	public String toString() {
		return "UsuarioModel [id=" + id + ", nombre=" + nombre + ", apellido_paterno=" + apellido_paterno
				+ ", apellido_materno=" + apellido_materno + ", username=" + username + ", contrasena=" + contrasena
				+ ", activo=" + activo + ", admin=" + admin + ", rol=" + rol + ", proveedor=" + proveedor
				+ "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getRol() {
		return rol;
	}

	public void setRol(int rol) {
		this.rol = rol;
	}

	public ProveedorModel getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorModel proveedor) {
		this.proveedor = proveedor;
	}


	
}
