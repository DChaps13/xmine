package com.restful.model;

import com.restful.entity.Persona;

public class MPersona {
	
		private int id;
		private String nombre;
		private String apellido;
		
		
		public MPersona() {
			
		}
		public MPersona(Persona p) {
			this.id = p.getId();
			this.nombre = p.getNombre();
			this.apellido = p.getApellido();
		}
		
		public MPersona(int id, String nombre, String apellido) {
			
			this.id = id;
			this.nombre = nombre;
			this.apellido = apellido;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellido() {
			return apellido;
		}
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}
	

}
