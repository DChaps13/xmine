package com.restful.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.restful.entity.Contrato;
import com.restful.entity.Proveedor;
import com.restful.entity.Contrato_x_tipo_de_activo;

public class ContratoModel {
	private int id;

	private Date fecha_inicio;

	private Date fecha_fin;

	private String observaciones;

	private double calificacion_contrato;

	private int capacidad_total_mant;

	private int capacidad_total_rep;

	private int cantidad_calificado;

	private List<Contrato_x_tipo_de_activoModel> contratoDetalle;
	
	public int getCapacidad_mantenimiento_inicial() {
		return capacidad_mantenimiento_inicial;
	}

	public void setCapacidad_mantenimiento_inicial(int capacidad_mantenimiento_inicial) {
		this.capacidad_mantenimiento_inicial = capacidad_mantenimiento_inicial;
	}

	public int getCapacidad_reparacion_inicial() {
		return capacidad_reparacion_inicial;
	}

	public void setCapacidad_reparacion_inicial(int capacidad_reparacion_inicial) {
		this.capacidad_reparacion_inicial = capacidad_reparacion_inicial;
	}

	public int getCapacidad_total_inicial() {
		return capacidad_total_inicial;
	}

	public void setCapacidad_total_inicial(int capacidad_total_inicial) {
		this.capacidad_total_inicial = capacidad_total_inicial;
	}

	private int capacidad_mantenimiento_inicial;
	private int capacidad_reparacion_inicial;
	private int capacidad_total_inicial;

	public int getCantidad_calificado() {
		return cantidad_calificado;
	}

	public List<Contrato_x_tipo_de_activoModel> getContratoDetalle() {
		return contratoDetalle;
	}

	public void setContratoDetalle(List<Contrato_x_tipo_de_activoModel> contratoDetalle) {
		this.contratoDetalle = contratoDetalle;
	}

	public void setCantidad_calificado(int cantidad_calificado) {
		this.cantidad_calificado = cantidad_calificado;
	}

	private ProveedorModel proveedor;

	private boolean activo;

	public ContratoModel(Contrato p) {
		this.id = p.getId();
		this.setFecha_fin(p.getFecha_fin());
		this.setFecha_inicio(p.getFecha_inicio());
		this.setObservaciones(p.getObservaciones());
		this.setCalificacion_contrato(p.getCalificacion_contrato());
		this.proveedor = new ProveedorModel(p.getProveedor());
		this.activo = p.isActivo();
		this.cantidad_calificado = p.getCantidad_calificado();
		this.capacidad_total_mant= p.getCapacidad_total_mant();
		this.capacidad_total_rep = p.getCapacidad_total_rep();
		this.capacidad_mantenimiento_inicial= p.getCapacidad_mantenimiento_inicial();
		this.capacidad_reparacion_inicial = p.getCapacidad_reparacion_inicial();
		this.capacidad_total_inicial = p.getCapacidad_total_inicial();
	}

	public int getCapacidad_total_rep() {
		return capacidad_total_rep;
	}

	public void setCapacidad_total_rep(int capacidad_total_rep) {
		this.capacidad_total_rep = capacidad_total_rep;
	}

	public int getCapacidad_total_mant() {
		return capacidad_total_mant;
	}

	public void setCapacidad_total_mant(int capacidad_total_mant) {
		this.capacidad_total_mant = capacidad_total_mant;
	}

	public double getCalificacion_contrato() {
		return calificacion_contrato;
	}

	public void setCalificacion_contrato(double calificacion_contrato) {
		this.calificacion_contrato = calificacion_contrato;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public ProveedorModel getProveedor() {
		return this.proveedor;
	}

	public boolean isActivo(){
		return this.activo;
	}

	public int getId(){
		return this.id;
	}


}
