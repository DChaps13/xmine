package com.restful.model;

import com.restful.entity.Proveedor;
import com.restful.entity.Solicitud;

public class SolicitudModel {
	private int id;

	private int tipo;

	private boolean activo = true;

	private double tiempo_respuesta;

	private double calificacion;

	private double costo_total;

	private String asunto;

	private String descripcion;

	private int prioridad;

	private int complejidad;

	private byte[] documento;

	private EstadoModel estado;

	private int id_plan_mantenimiento;

	private int id_usuario;

	private int id_activo;

	private int id_proveedor = 0;

	private String razon_social;
	private String placa_activo;
	private String ruc;

	public String getMarca() {
		return marca;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	private String marca;
	private String modelo;

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public String getPlaca_activo() {
		return placa_activo;
	}

	public void setPlaca_activo(String placa_activo) {
		this.placa_activo = placa_activo;
	}

	public SolicitudModel(int id, int tipo, boolean activo, double tiempo_respuesta, double calificacion,
			double costo_total, String asunto, String descripcion, int prioridad, int complejidad, byte[] documento) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.activo = activo;
		this.tiempo_respuesta = tiempo_respuesta;
		this.calificacion = calificacion;
		this.costo_total = costo_total;
		this.asunto = asunto;
		this.descripcion = descripcion;
		this.prioridad = prioridad;
		this.complejidad = complejidad;
		this.documento = documento;
	}
	
	public SolicitudModel(Solicitud s) {
		this.id = s.getId();
		this.tipo = s.getTipo();
		this.activo = s.isActivo();
		this.tiempo_respuesta = s.getTiempo_respuesta();
		this.calificacion = s.getCalificacion();
		this.costo_total = s.getCosto_total();
		this.asunto = s.getAsunto();
		this.descripcion = s.getDescripcion();
		this.prioridad = s.getPrioridad();
		this.complejidad = s.getComplejidad();
		this.documento = s.getDocumento();
		this.estado = new EstadoModel(s.getEstado()) ;
		this.id_usuario = s.getUsuario().getId();
		this.id_activo = s.getActivo_movil().getId();
		this.placa_activo = s.getActivo_movil().getPlaca();
		this.marca = s.getActivo_movil().getMarca();
		this.modelo= s.getActivo_movil().getModelo_motor();
		if(s.getProveedor() != null) this.razon_social = s.getProveedor().getRazon_social();
		if(s.getProveedor() != null) this.id_proveedor = s.getProveedor().getId();
		if(s.getPlan_de_mantenimiento()!= null) this.id_plan_mantenimiento = s.getPlan_de_mantenimiento().getId();
		if(s.getProveedor() != null) this.ruc = s.getProveedor().getRuc();
	}
	public SolicitudModel() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public double getTiempo_respuesta() {
		return tiempo_respuesta;
	}

	public void setTiempo_respuesta(double tiempo_respuesta) {
		this.tiempo_respuesta = tiempo_respuesta;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public double getCosto_total() {
		return costo_total;
	}

	public void setCosto_total(double costo_total) {
		this.costo_total = costo_total;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public int getComplejidad() {
		return complejidad;
	}

	public void setComplejidad(int complejidad) {
		this.complejidad = complejidad;
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}

	public EstadoModel getEstado() {
		return estado;
	}

	public void setEstado(EstadoModel estado) {
		this.estado = estado;
	}

	public int getId_plan_mantenimiento() {
		return id_plan_mantenimiento;
	}

	public void setId_plan_mantenimiento(int id_plan_mantenimiento) {
		this.id_plan_mantenimiento = id_plan_mantenimiento;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public int getId_activo() {
		return id_activo;
	}

	public void setId_activo(int id_activo) {
		this.id_activo = id_activo;
	}

	public int getId_proveedor() {
		return id_proveedor;
	}

	public void setId_proveedor(int id_proveedor) {
		this.id_proveedor = id_proveedor;
	}

	public int compareTo(SolicitudModel sol) {
        return sol.getComplejidad() - this.getComplejidad();
    }
}
