package com.restful.model;

import java.util.Date;

import com.restful.entity.Log_de_estados;

public class Log_de_estadosModel {
	private int id;
	
	private String estado_inicial;

	private String estado_final;

	private int id_usuario;
	
	private Date fecha;

	
	
	@Override
	public String toString() {
		return "Log_de_estadosModel [id=" + id + ", estado_inicial=" + estado_inicial + ", estado_final=" + estado_final
				+ ", id_usuario=" + id_usuario + ", fecha=" + fecha + "]";
	}

	@SuppressWarnings("deprecation")
	public Log_de_estadosModel(Log_de_estados lg) {
		this.id = lg.getId();
		this.estado_inicial = lg.getEstado_inicial();
		this.estado_final = lg.getEstado_final();
		this.id_usuario = lg.getId_usuario();
		this.fecha = lg.getFecha();
		int aux = fecha.getHours();
		fecha.setHours(aux);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstado_inicial() {
		return estado_inicial;
	}

	public void setEstado_inicial(String estado_inicial) {
		this.estado_inicial = estado_inicial;
	}

	public String getEstado_final() {
		return estado_final;
	}

	public void setEstado_final(String estado_final) {
		this.estado_final = estado_final;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
	
}
