package com.restful.model;

import java.util.List;

import com.restful.entity.Proveedor;

public class ProveedorModel {
	private int id;

	private String ruc;
	
	private String razon_social;
	
	private double  calificacion;
	
	private String direccion;
	
	private String telefono_contacto;
	
	private String nombre_contacto;
	
	private boolean estado;
	
	private String ubigeo;
	
	private int id_usuario;
	
	private boolean bloqueado;

	private double costoActual;
	
	
	
	private int cantidad_calificado;
	public int getCantidad_calificado() {
		return cantidad_calificado;
	}

	public void setCantidad_calificado(int cantidad_calificado) {
		this.cantidad_calificado = cantidad_calificado;
	}
	
	public double getCostoActual(){
		return costoActual;
	}
	public void setCostoActual(double costoActual){
		this.costoActual = costoActual;
	}
	@Override
	public String toString() {
		return "ProveedorModel [id=" + id + ", ruc=" + ruc + ", razon_social=" + razon_social + ", calificacion="
				+ calificacion + ", direccion=" + direccion + ", telefono_contacto=" + telefono_contacto
				+ ", nombre_contacto=" + nombre_contacto + ", estado=" + estado + ", ubigeo=" + ubigeo + ", id_usuario="
				+ id_usuario + "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getRuc() {
		return ruc;
	}


	public void setRuc(String ruc) {
		this.ruc = ruc;
	}


	public String getRazon_social() {
		return razon_social;
	}


	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}


	public double getCalificacion() {
		return calificacion;
	}


	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getTelefono_contacto() {
		return telefono_contacto;
	}


	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}


	public String getNombre_contacto() {
		return nombre_contacto;
	}


	public void setNombre_contacto(String nombre_contacto) {
		this.nombre_contacto = nombre_contacto;
	}


	public boolean isEstado() {
		return estado;
	}


	public void setEstado(boolean estado) {
		this.estado = estado;
	}


	public String getUbigeo() {
		return ubigeo;
	}


	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}


	public int getId_usuario() {
		return id_usuario;
	}


	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}


	public ProveedorModel(int id, String ruc, String razon_social, double calificacion, String direccion,
			String telefono_contacto, String nombre_contacto, boolean estado, String ubigeo) {
		super();
		this.id = id;
		this.ruc = ruc;
		this.razon_social = razon_social;
		this.calificacion = calificacion;
		this.direccion = direccion;
		this.telefono_contacto = telefono_contacto;
		this.nombre_contacto = nombre_contacto;
		this.estado = estado;
		this.ubigeo = ubigeo;
	}


	public ProveedorModel(Proveedor p) {
		this.id = p.getId();
		this.ruc = p.getRuc();
		this.razon_social = p.getRazon_social();
		this.calificacion = p.getCalificacion();
		this.direccion = p.getDireccion();
		this.telefono_contacto = p.getTelefono_contacto();
		this.nombre_contacto = p.getNombre_contacto();
		this.estado = p.isEstado();
		this.ubigeo = p.getUbigeo();	
		this.bloqueado = p.isBloqueado();
		this.cantidad_calificado = p.getCantidad_calificado();
		this.costoActual=0;
		if (p.getUsuario() != null) this.id_usuario = p.getUsuario().getId();
	}


	public ProveedorModel() {
		super();
		// TODO Auto-generated constructor stub
	}


	public boolean isBloqueado() {
		return bloqueado;
	}


	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	
	
}
