package com.restful.model;

public class reporteCapacidad {

	private Tipo_de_activoModel tpActivoModel;

	private int tipo_solicitud_mant_cap=0;
	private int tipo_solicitud_mant_usado=0;
	private double tipo_solicitud_mant_usado_porc;
	private int tipo_solicitud_repa_cap=0;
	private int tipo_solicitud_repa_usado=0;
	private double tipo_solicitud_repa_usado_porc;
	private double porcentaje_usado_x_tipo=0;
	
	public void actualizarPorcentajegeneral() {
		int capacidad = tipo_solicitud_mant_cap+tipo_solicitud_repa_cap;
		int usado = tipo_solicitud_mant_usado+ tipo_solicitud_repa_usado;
		this.porcentaje_usado_x_tipo = (double)usado/capacidad;
	}
	
	public reporteCapacidad() {
		
	}
	
	public Tipo_de_activoModel getTpActivoModel() {
		return tpActivoModel;
	}
	public void setTpActivoModel(Tipo_de_activoModel tpActivoModel) {
		this.tpActivoModel = tpActivoModel;
	}
	public int getTipo_solicitud_mant_cap() {
		return tipo_solicitud_mant_cap;
	}
	public void setTipo_solicitud_mant_cap(int tipo_solicitud_mant_cap) {
		this.tipo_solicitud_mant_cap = tipo_solicitud_mant_cap;
	}
	public int getTipo_solicitud_mant_usado() {
		return tipo_solicitud_mant_usado;
	}
	public void setTipo_solicitud_mant_usado(int tipo_solicitud_mant_usado) {
		this.tipo_solicitud_mant_usado = tipo_solicitud_mant_usado;
	}
	public double getTipo_solicitud_mant_usado_porc() {
		return tipo_solicitud_mant_usado_porc;
	}
	public void setTipo_solicitud_mant_usado_porc(double tipo_solicitud_mant_usado_porc) {
		this.tipo_solicitud_mant_usado_porc = tipo_solicitud_mant_usado_porc;
	}
	public int getTipo_solicitud_repa_cap() {
		return tipo_solicitud_repa_cap;
	}
	public void setTipo_solicitud_repa_cap(int tipo_solicitud_repa_cap) {
		this.tipo_solicitud_repa_cap = tipo_solicitud_repa_cap;
	}
	public int getTipo_solicitud_repa_usado() {
		return tipo_solicitud_repa_usado;
	}
	public void setTipo_solicitud_repa_usado(int tipo_solicitud_repa_usado) {
		this.tipo_solicitud_repa_usado = tipo_solicitud_repa_usado;
	}
	public double getTipo_solicitud_repa_usado_porc() {
		return tipo_solicitud_repa_usado_porc;
	}
	public void setTipo_solicitud_repa_usado_porc(double tipo_solicitud_repa_usado_porc) {
		this.tipo_solicitud_repa_usado_porc = tipo_solicitud_repa_usado_porc;
	}
	public double getPorcentaje_usado_x_tipo() {
		return porcentaje_usado_x_tipo;
	}
	public void setPorcentaje_usado_x_tipo(double porcentaje_usado_x_tipo) {
		this.porcentaje_usado_x_tipo = porcentaje_usado_x_tipo;
	}
	
	
	
}
