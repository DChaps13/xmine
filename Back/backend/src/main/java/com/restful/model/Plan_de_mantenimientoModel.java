package com.restful.model;

import com.restful.entity.Plan_de_mantenimiento;

public class Plan_de_mantenimientoModel implements Comparable<Plan_de_mantenimientoModel>{
	private int id;
	private int horas_uso;
	private boolean activo;
	private String modelo;
	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHoras_uso() {
		return horas_uso;
	}

	public void setHoras_uso(int horas_uso) {
		this.horas_uso = horas_uso;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Plan_de_mantenimientoModel(Plan_de_mantenimiento p){
		this.activo=p.isActivo();
		this.descripcion=p.getDescripcion();
		this.horas_uso=p.getHoras_uso();
		this.id=p.getId();
		this.modelo=p.getModelo();
	}
	public Plan_de_mantenimientoModel(boolean activo, String desc, int horas_uso, int id, String modelo){
		this.activo= activo;
		this.descripcion=desc;
		this.horas_uso=horas_uso;
		this.id=id;
		this.modelo=modelo;
	}
	@Override
  	public int compareTo(Plan_de_mantenimientoModel p) {
    	if (getHoras_uso() == 0 || p.getHoras_uso() == 0) {
      		return 0;
    	}
    	return (new Integer(getHoras_uso())).compareTo(new Integer(p.getHoras_uso()));
  	}
}
