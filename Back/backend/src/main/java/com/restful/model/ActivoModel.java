package com.restful.model;

import com.restful.entity.Activo;

public class ActivoModel {
	private int id;
	
	private String placa;
	
	private double potencia;
	
	private double peso_bruto;
	
	private String modelo;
	
	private boolean estado = true;
	
	private int horas_uso;
	
	private String marca;
	
	private Tipo_de_activoModel tipo_activo;

	
	
	
	public ActivoModel(int id, String placa, double potencia, double peso_bruto, String modelo, boolean estado,
			int horas_uso, String marca) {
		super();
		this.id = id;
		this.placa = placa;
		this.potencia = potencia;
		this.peso_bruto = peso_bruto;
		this.modelo = modelo;
		this.estado = estado;
		this.horas_uso = horas_uso;
		this.marca = marca;
	}
	
	public ActivoModel (Activo a) {
		this.id = a.getId();
		this.placa = a.getPlaca();
		this.potencia = a.getPotencia();
		this.peso_bruto = a.getPeso_bruto();
		this.modelo = a.getModelo_motor();
		this.estado = a.isEstado();
		this.horas_uso = a.gethoras_uso();
		this.marca = a.getMarca();
		this.tipo_activo = new Tipo_de_activoModel(a.getTipo_de_activo());
	}

	public ActivoModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public double getPotencia() {
		return potencia;
	}

	public void setPotencia(double potencia) {
		this.potencia = potencia;
	}

	public double getPeso_bruto() {
		return peso_bruto;
	}

	public void setPeso_bruto(double peso_bruto) {
		this.peso_bruto = peso_bruto;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getHoras_uso() {
		return horas_uso;
	}

	public void setHoras_uso(int horas_uso) {
		this.horas_uso = horas_uso;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Tipo_de_activoModel getTipo_activo() {
		return tipo_activo;
	}

	public void setTipo_activo(Tipo_de_activoModel tipo_activo) {
		this.tipo_activo = tipo_activo;
	}
	
	
}
