package com.restful.model;

import com.restful.entity.Tipo_de_activo;

public class Tipo_de_activoModel {
	private int id;

	private String nombre_tipo;

	private int criticidad;

	private double costoActual;

	public int getId() {
		return id;
	}

	public double getCostoActual() {
		return costoActual;
	}

	public void setCostoActual(double costoActual) {
		this.costoActual = costoActual;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre_tipo() {
		return nombre_tipo;
	}

	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}

	public int getCriticidad() {
		return criticidad;
	}

	public void setCriticidad(int criticidad) {
		this.criticidad = criticidad;
	}

	public Tipo_de_activoModel(int id, String nombre_tipo, int criticidad) {
		super();
		this.id = id;
		this.nombre_tipo = nombre_tipo;
		this.criticidad = criticidad;
	}

	public Tipo_de_activoModel(Tipo_de_activo t) {
		this.id = t.getId();
		this.nombre_tipo = t.getNombre_tipo();
		this.criticidad = t.getCriticidad();		
		// TODO Auto-generated constructor stub
	}

	public String toString() { 
        return this.nombre_tipo; 
    }


	
}
