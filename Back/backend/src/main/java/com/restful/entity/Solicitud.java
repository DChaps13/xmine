package com.restful.entity;




import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "SOLICITUD")
@Entity
public class Solicitud {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_SOLICITUD")
	private int id;
	
	@Column(name  = "TIPO")
	private int tipo;
	
	@Column(name = "ACTIVO")
	private boolean activo = true;
	
	@Column(name = "TIEMPO_RESPUESTA")
	private double tiempo_respuesta;
	

	@Column(name = "CALIFICACION")
	private double calificacion;
	
	@Column(name = "COSTO_TOTAL")
	private double costo_total;
	
	@Column(name = "ASUNTO")
	private String asunto;
	
	@Column(name= "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "PRIORIDAD")
	private int prioridad;
	
	@Column(name = "COMPLEJIDAD")
	private int complejidad;
	
	@Column(name = "DOCUMENTO")
	@Lob
	private byte[] documento;
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ESTADO")
	private Estado estado = new Estado(1,"Registrada");
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PLAN_MANTENIMIENTO")
	private Plan_de_mantenimiento plan_de_mantenimiento;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_USUARIO")
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ACTIVO")
	private Activo activo_movil;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROVEEDOR")
	private Proveedor proveedor = null;

	@OneToMany(mappedBy = "solicitud",cascade = CascadeType.ALL)
	private List<Log_de_estados> log_de_estados = new ArrayList<>();
	
	@OneToMany(mappedBy = "solicitud",cascade = CascadeType.ALL)
	private List<Auditoria> auditorias = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public double getTiempo_respuesta() {
		return tiempo_respuesta;
	}

	public void setTiempo_respuesta(double tiempo_respuesta) {
		this.tiempo_respuesta = tiempo_respuesta;
	}



	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public double getCosto_total() {
		return costo_total;
	}

	public void setCosto_total(double costo_total) {
		this.costo_total = costo_total;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public int getComplejidad() {
		return complejidad;
	}

	public void setComplejidad(int criticidad) {
		this.complejidad = criticidad;
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Plan_de_mantenimiento getPlan_de_mantenimiento() {
		return plan_de_mantenimiento;
	}

	public void setPlan_de_mantenimiento(Plan_de_mantenimiento plan_de_mantenimiento) {
		this.plan_de_mantenimiento = plan_de_mantenimiento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Activo getActivo_movil() {
		return activo_movil;
	}

	public void setActivo_movil(Activo activo_movil) {
		this.activo_movil = activo_movil;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public List<Log_de_estados> getLog_de_estados() {
		return log_de_estados;
	}

	public void setLog_de_estados(List<Log_de_estados> log_de_estados) {
		this.log_de_estados = log_de_estados;
	}

	public List<Auditoria> getAuditorias() {
		return auditorias;
	}

	public void setAuditorias(List<Auditoria> auditorias) {
		this.auditorias = auditorias;
	}


	
	public Solicitud(int id, int tipo, boolean activo, double tiempo_respuesta, double calificacion, double costo_total,
			String asunto, String descripcion, int prioridad, int criticidad, byte[] documento, Estado estado,
			Plan_de_mantenimiento plan_de_mantenimiento, Usuario usuario, Activo activo_movil, Proveedor proveedor,
			List<Log_de_estados> log_de_estados, List<Auditoria> auditorias) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.activo = activo;
		this.tiempo_respuesta = tiempo_respuesta;
		this.calificacion = calificacion;
		this.costo_total = costo_total;
		this.asunto = asunto;
		this.descripcion = descripcion;
		this.prioridad = prioridad;
		this.complejidad = criticidad;
		this.documento = documento;
		this.estado = estado;
		this.plan_de_mantenimiento = plan_de_mantenimiento;
		this.usuario = usuario;
		this.activo_movil = activo_movil;
		this.proveedor = proveedor;
		this.log_de_estados = log_de_estados;
		this.auditorias = auditorias;
	}

	@Override
	public String toString() {
		return "Solicitud [id=" + id + ", tipo=" + tipo + ", activo=" + activo + ", tiempo_respuesta="
				+ tiempo_respuesta + ", calificacion=" + calificacion + ", costo_total=" + costo_total + ", asunto="
				+ asunto + ", descripcion=" + descripcion + ", prioridad=" + prioridad + ", complejidad=" + complejidad
				+ ", documento=" + Arrays.toString(documento) + ", estado=" + estado + ", plan_de_mantenimiento="
				+ plan_de_mantenimiento + ", usuario=" + usuario + ", activo_movil=" + activo_movil + ", proveedor="
				+ proveedor.getId() + ", log_de_estados=" + log_de_estados + ", auditorias=" + auditorias + "]";
	}

	public Solicitud() {
		// TODO Auto-generated constructor stub
	}
	
}
