package com.restful.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Table(name = "CONTRATO")
@Entity

public class Contrato {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_CONTRATO")
	private int id;
	
	@Column(name = "FECHA_INICIO")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	private Date fecha_inicio;
	
	@Column(name = "FECHA_FIN")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	private Date fecha_fin;
	
	@Column(name = "OBSERVACIONES")
	private String observaciones;
	
	@Column(name = "CALIFICACION_CONTRATO")
	private double calificacion_contrato;
	
	@Column(name = "CANTIDAD_CALIFICADO")
	private int cantidad_calificado;

	@Column(name = "CAPACIDAD_MANTENIMIENTO_INICIAL")
	private int capacidad_mantenimiento_inicial;
	
	@Column(name = "CAPACIDAD_REPARACION_INICIAL")
	private int capacidad_reparacion_inicial;
	
	@Column(name = "CAPACIDAD_TOTAL_INICIAL")
	private int capacidad_total_inicial;
	
	public int getCapacidad_mantenimiento_inicial() {
		return capacidad_mantenimiento_inicial;
	}
	public void setCapacidad_mantenimiento_inicial(int capacidad_mantenimiento_inicial) {
		this.capacidad_mantenimiento_inicial = capacidad_mantenimiento_inicial;
	}
	public int getCapacidad_reparacion_inicial() {
		return capacidad_reparacion_inicial;
	}
	public void setCapacidad_reparacion_inicial(int capacidad_reparacion_inicial) {
		this.capacidad_reparacion_inicial = capacidad_reparacion_inicial;
	}
	public int getCapacidad_total_inicial() {
		return capacidad_total_inicial;
	}
	public void setCapacidad_total_inicial(int capacidad_total_inicial) {
		this.capacidad_total_inicial = capacidad_total_inicial;
	}

	
	public void setCantidad_calificado(int cantidad_calificado) {
		this.cantidad_calificado = cantidad_calificado;
	}
	public int getCantidad_calificado() {
		return cantidad_calificado;
	}

	@Column(name = "CAPACIDAD_TOTAL_MANT")
	private int capacidad_total_mant;
	
	@Column(name = "CAPACIDAD_TOTAL_REP")
	private int capacidad_total_rep;
	
	@Column(name  = "ACTIVO")
	private boolean activo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROVEEDOR")
	@JsonIgnoreProperties({"contratos","tipo_de_activo"})
	private Proveedor proveedor;
	
	@OneToMany(mappedBy = "contrato", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("contrato")
	private List<Contrato_x_tipo_de_activo> contratos_x_tipo_de_activos = new ArrayList<>();

	
	
	public int getCapacidad_total_mant() {
		return capacidad_total_mant;
	}

	public void setCapacidad_total_mant(int capacidad_total_mant) {
		this.capacidad_total_mant = capacidad_total_mant;
	}

	public int getCapacidad_total_rep() {
		return capacidad_total_rep;
	}

	public void setCapacidad_total_rep(int capacidad_total_rep) {
		this.capacidad_total_rep = capacidad_total_rep;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}



	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public double getCalificacion_contrato() {
		return calificacion_contrato;
	}

	public void setCalificacion_contrato(double calificacion_contrato) {
		this.calificacion_contrato = calificacion_contrato;
	}



	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public List<Contrato_x_tipo_de_activo> getContratos_x_tipo_de_activos() {
		return contratos_x_tipo_de_activos;
	}

	public void setContratos_x_tipo_de_activos(List<Contrato_x_tipo_de_activo> contratos_x_tipo_de_activos) {
		this.contratos_x_tipo_de_activos = contratos_x_tipo_de_activos;
	}

	
	public Contrato(int id, Date fecha_inicio, Date fecha_fin, String observaciones, double calificacion_contrato,
			int capacidad_total_mant, int capacidad_total_rep, Proveedor proveedor,
			List<Contrato_x_tipo_de_activo> contratos_x_tipo_de_activos) {
		super();
		this.id = id;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.observaciones = observaciones;
		this.calificacion_contrato = calificacion_contrato;
		this.capacidad_total_mant = capacidad_total_mant;
		this.capacidad_total_rep = capacidad_total_rep;
		this.proveedor = proveedor;
		this.contratos_x_tipo_de_activos = contratos_x_tipo_de_activos;
	}

	@Override
	public String toString() {
		return "Contrato [id=" + id + ", fecha_inicio=" + fecha_inicio + ", fecha_fin=" + fecha_fin + ", observaciones="
				+ observaciones + ", calificacion_contrato=" + calificacion_contrato + ", capacidad_total_mant="
				+ capacidad_total_mant + ", capacidad_total_rep=" + capacidad_total_rep + ", proveedor=" + proveedor.getId()
				+ ", contratos_x_tipo_de_activos=" + contratos_x_tipo_de_activos + "]";
	}

	public Contrato() {
		// TODO Auto-generated constructor stub
	}
	
}
