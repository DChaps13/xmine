package com.restful.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "PLAN_DE_MANTENIMIENTO")
@Entity
public class Plan_de_mantenimiento {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name= "ID_PLAN_MANTENIMIENTO")
	private int id;
	
	@Column(name="HORAS_USO")
	private int horas_uso;
	
	@Column(name = "ACTIVO")
	private boolean activo;
	
	@Column(name= "MODELO")
	private String modelo;
	
	@Column(name= "DESCRIPCION")
	private String descripcion;
	


	@OneToMany(mappedBy = "plan_de_mantenimiento",cascade = CascadeType.ALL)
	private List<Solicitud> solicitudes = new ArrayList<>();
	
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	
	public List<Solicitud> getSolicitudes() {
		return solicitudes;
	}
	public void setSolicitudes(List<Solicitud> solicitudes) {
		this.solicitudes = solicitudes;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHoras_uso() {
		return horas_uso;
	}

	public void setHoras_uso(int horas_uso) {
		this.horas_uso = horas_uso;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}



	@Override
	public String toString() {
		return "Plan_de_mantenimiento [id=" + id + ", horas_uso=" + horas_uso + ", activo=" + activo + ", modelo="
				+ modelo + ",descripcion="+descripcion+", solicitudes=" + solicitudes + "]";
	}
	public Plan_de_mantenimiento(int id, int horas_uso, boolean activo, String modelo, String descripcion) {

		this.id = id;
		this.horas_uso = horas_uso;
		this.activo = activo;
		this.modelo = modelo;
		this.descripcion = descripcion;
	}
	
	public Plan_de_mantenimiento() {
		// TODO Auto-generated constructor stub
	}
	
	
}
