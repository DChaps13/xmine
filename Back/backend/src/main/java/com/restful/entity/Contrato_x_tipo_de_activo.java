package com.restful.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "CONTRATO_X_TIPO_DE_ACTIVO")
@Entity
public class Contrato_x_tipo_de_activo {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID")
	private int id;
	
	@Column(name = "TARIFA")
	private double tarifa;
	
	
	@Column(name = "CAPACIDAD_MANTENIMIENTO")
	private int capacidad_mantenimiento;
	
	@Column(name = "CAPACIDAD_REPARACION")
	private int capacidad_reparacion;
	
	@Column(name = "TIEMPO_ESTIMADO_EST")
	private int tiempo_respuesta_est;
	
	@Column(name = "CALIFICACION_TIPO")
	private double calificacion_tipo; 
	
	@Column(name = "ACTIVO")
	private boolean activo;
	

	public int getCapacidad_mantenimiento_inicial() {
		return capacidad_mantenimiento_inicial;
	}
	public void setCapacidad_mantenimiento_inicial(int capacidad_mantenimiento_inicial) {
		this.capacidad_mantenimiento_inicial = capacidad_mantenimiento_inicial;
	}
	public int getCapacidad_reparacion_inicial() {
		return capacidad_reparacion_inicial;
	}
	public void setCapacidad_reparacion_inicial(int capacidad_reparacion_inicial) {
		this.capacidad_reparacion_inicial = capacidad_reparacion_inicial;
	}
	public int getCapacidad_total_inicial() {
		return capacidad_total_inicial;
	}
	public void setCapacidad_total_inicial(int capacidad_total_inicial) {
		this.capacidad_total_inicial = capacidad_total_inicial;
	}
	@Column(name = "CAPACIDAD_MANTENIMIENTO_INICIAL")
	private int capacidad_mantenimiento_inicial;
	
	@Column(name = "CAPACIDAD_REPARACION_INICIAL")
	private int capacidad_reparacion_inicial;
	
	@Column(name = "CAPACIDAD_TOTAL_INICIAL")
	private int capacidad_total_inicial;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_DE_ACTIVO")
	private Tipo_de_activo id_tipo_de_activo; 
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CONTRATO")
	private Contrato contrato;

	@Column(name = "CANTIDAD_CALIFICADO")
	private int cantidad_calificado;
	
	public void setCantidad_calificado(int cantidad_calificado) {
		this.cantidad_calificado = cantidad_calificado;
	}
	public int getCantidad_calificado() {
		return cantidad_calificado;
	}
	
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public Tipo_de_activo getId_tipo_de_activo() {
		return id_tipo_de_activo;
	}
	public void setId_tipo_de_activo(Tipo_de_activo id_tipo_de_activo) {
		this.id_tipo_de_activo = id_tipo_de_activo;
	}
	public void setCalificacion_tipo(double calificacion_tipo) {
		this.calificacion_tipo = calificacion_tipo;
	}
	public double getCalificacion_tipo() {
		return calificacion_tipo;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getTarifa() {
		return tarifa;
	}

	public void setTarifa(double tarifa) {
		this.tarifa = tarifa;
	}
	

	public int getCapacidad_mantenimiento() {
		return capacidad_mantenimiento;
	}

	public void setCapacidad_mantenimiento(int capacidad_mantenimiento) {
		this.capacidad_mantenimiento = capacidad_mantenimiento;
	}

	public int getCapacidad_reparacion() {
		return capacidad_reparacion;
	}

	public void setCapacidad_reparacion(int capacidad_reparacion) {
		this.capacidad_reparacion = capacidad_reparacion;
	}

	public int getTiempo_respuesta_est() {
		return tiempo_respuesta_est;
	}

	public void setTiempo_respuesta_est(int tiempo_estimado) {
		this.tiempo_respuesta_est = tiempo_estimado;
	}

	public Tipo_de_activo getTipo_de_activo() {
		return id_tipo_de_activo;
	}

	public void setTipo_de_activo(Tipo_de_activo tipo_de_activo) {
		this.id_tipo_de_activo = tipo_de_activo;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}


	
	public Contrato_x_tipo_de_activo(int id, double tarifa, int capacidad_mantenimiento, int capacidad_reparacion,
			int tiempo_respuesta_est, double calificacion_tipo, Tipo_de_activo tipo_de_activo, Contrato contrato) {
		super();
		this.id = id;
		this.tarifa = tarifa;
		this.capacidad_mantenimiento = capacidad_mantenimiento;
		this.capacidad_reparacion = capacidad_reparacion;
		this.tiempo_respuesta_est = tiempo_respuesta_est;
		this.calificacion_tipo = calificacion_tipo;
		this.id_tipo_de_activo = tipo_de_activo;
		this.contrato = contrato;
	}
	@Override
	public String toString() {
		return "Contrato_x_tipo_de_activo [id=" + id + ", tarifa=" + tarifa + ", capacidad_mantenimiento="
				+ capacidad_mantenimiento + ", capacidad_reparacion=" + capacidad_reparacion + ", tiempo_respuesta_est="
				+ tiempo_respuesta_est + ", calificacion_tipo=" + calificacion_tipo + ", tipo_de_activo="
				+ id_tipo_de_activo + ", contrato=" + contrato + "]";
	}
	public Contrato_x_tipo_de_activo() {
		// TODO Auto-generated constructor stub
	}
	
}
