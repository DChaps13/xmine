package com.restful.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "ALGORITMO")
@Entity
public class Algoritmo {
	@Id
	@Column(name = "ID_ALGORITMO")
	private int id;
	
	@Column(name= "NOMBRE")
	private String nombre;
	
	@Column(name= "FLAG")
	private int flag;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public Algoritmo() {
		
		// TODO Auto-generated constructor stub
	}
	
	
	
}
