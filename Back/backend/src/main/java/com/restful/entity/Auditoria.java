package com.restful.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "AUDITORIA")
@Entity
public class Auditoria {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_AUDITORIA")
	private int id;
	
	@Column(name = "ID_USUARIO_CREADOR")
	private int id_usuario_creador;
	
	@Column(name = "ID_USUARIO_MODIFICACION")
	private int id_usuario_modificacion;
	
	@Column(name = "FECHA_CREACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion = new Date();
	
	@Column(name = "FECHA_MODIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion ;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOLICITUD")
	private Solicitud solicitud;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_usuario_creador() {
		return id_usuario_creador;
	}

	public void setId_usuario_creador(int id_usuario_creador) {
		this.id_usuario_creador = id_usuario_creador;
	}

	public int getId_usuario_modificacion() {
		return id_usuario_modificacion;
	}

	public void setId_usuario_modificacion(int id_usuario_modificacion) {
		this.id_usuario_modificacion = id_usuario_modificacion;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public Auditoria(int id, int id_usuario_creador, int id_usuario_modificacion, Date fecha_creacion,
			Date fecha_modificacion, Solicitud solicitud) {
		this.id = id;
		this.id_usuario_creador = id_usuario_creador;
		this.id_usuario_modificacion = id_usuario_modificacion;
		this.fecha_creacion = fecha_creacion;
		this.fecha_modificacion = fecha_modificacion;
		this.solicitud = solicitud;
	}

	@Override
	public String toString() {
		return "Auditoria [id=" + id + ", id_usuario_creador=" + id_usuario_creador + ", id_usuario_modificacion="
				+ id_usuario_modificacion + ", fecha_creacion=" + fecha_creacion + ", fecha_modificacion="
				+ fecha_modificacion + ", solicitud=" + solicitud + "]";
	}
	
	public Auditoria() {
		// TODO Auto-generated constructor stub
	}
	
	
}
