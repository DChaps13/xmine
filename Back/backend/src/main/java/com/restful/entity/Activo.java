package com.restful.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name = "ACTIVO")
@Entity
public class Activo {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_ACTIVO")
	private int id;
	
	@Column(name = "PLACA")
	private String placa;
	
	@Column(name = "POTENCIA")
	private double potencia;
	
	@Column(name = "PESO_BRUTO")
	private double peso_bruto;
	

	@Column(name = "MODELO")
	private String modelo;
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	@Column(name = "ESTADO")
	private boolean estado = true;
	
	@Column(name = "HORAS_USO")
	private int horas_uso;
	
	@Column(name = "MARCA")
	private String marca; 

	@OneToMany(mappedBy = "activo_movil",cascade = CascadeType.ALL)
	//@JsonIgnoreProperties({"activo_movil"})
	private List<Solicitud> solicitudes = new ArrayList<>();
	
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "ID_TIPO_DE_ACTIVO")

	private Tipo_de_activo tipo_de_activo;
	


	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getMarca() {
		return marca;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public double getPotencia() {
		return potencia;
	}

	public void setPotencia(double potencia) {
		this.potencia = potencia;
	}

	public double getPeso_bruto() {
		return peso_bruto;
	}

	public void setPeso_bruto(double peso_bruto) {
		this.peso_bruto = peso_bruto;
	}



	public String getModelo_motor() {
		return modelo;
	}

	public void setModelo_motor(String modelo) {
		this.modelo = modelo;
	}


	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}


	public int gethoras_uso() {
		return horas_uso;
	}

	public void sethoras_uso(int horas_uso) {
		this.horas_uso = horas_uso;
	}

	public Tipo_de_activo getTipo_de_activo() {
		return tipo_de_activo;
	}

	public void setTipo_de_activo(Tipo_de_activo tipo_de_activo) {
		this.tipo_de_activo = tipo_de_activo;
	}

	public List<Solicitud> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(List<Solicitud> solicitudes) {
		this.solicitudes = solicitudes;
	}




	public Activo(int id, String placa, double potencia, double peso_bruto, String modelo, boolean estado,
			int horas_uso, String marca, Tipo_de_activo tipo_de_activo, List<Solicitud> solicitudes) {
		super();
		this.id = id;
		this.placa = placa;
		this.potencia = potencia;
		this.peso_bruto = peso_bruto;
		this.modelo = modelo;
		this.estado = estado;
		this.horas_uso = horas_uso;
		this.marca = marca;
		this.tipo_de_activo = tipo_de_activo;
		this.solicitudes = solicitudes;
	}
	@Override
	public String toString() {
		return "Activo [id=" + id + ", placa=" + placa + ", potencia=" + potencia + ", peso_bruto=" + peso_bruto
				+ ", modelo=" + modelo + ", estado=" + estado + ", horas_uso=" + horas_uso + ", marca=" + marca
				+ ", tipo_de_activo=" + tipo_de_activo  ;
	}
	public Activo() {
		// TODO Auto-generated constructor stub
	}
	
}
