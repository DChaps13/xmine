package com.restful.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "LOG_DE_ESTADOS")
@Entity
public class Log_de_estados implements Comparable<Log_de_estados> {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_LOG_ESTADO")
	private int id;
	
	@Column(name = "ESTADO_INICIAL")
	private String estadoInicial;
	
	@Column(name  = "ESTADO_FINAL")
	private String estadoFinal;
	
	@Column(name = "ID_USUARIO")
	private int id_usuario;
	
	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha =  new Date();
	

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOLICITUD")
	private Solicitud solicitud;
	
	public Solicitud getSolicitud() {
		return solicitud;
	}
	
	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstado_inicial() {
		return estadoInicial;
	}

	public void setEstado_inicial(String estado_inicial) {
		this.estadoInicial = estado_inicial;
	}

	public String getEstado_final() {
		return estadoFinal;
	}

	public void setEstado_final(String estado_final) {
		this.estadoFinal = estado_final;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}




	public Log_de_estados(int id, String estado_inicial, String estado_final, int id_usuario, Date fecha, Solicitud solicitud) {
		this.id = id;
		this.estadoInicial = estado_inicial;
		this.estadoFinal = estado_final;
		this.id_usuario = id_usuario;
		this.fecha = fecha;
		this.solicitud = solicitud;
	}

	public Log_de_estados() {
		// TODO Auto-generated constructor stub
	}
	public Log_de_estados(String estadoI, String estadoF,int idU,int idS) {
		this.estadoInicial = estadoI;
		this.estadoFinal = estadoF;
		this.id_usuario=idU;
		Solicitud s = new Solicitud();
		s.setId(idS);
		this.solicitud= s;
	}
	
	public Log_de_estados(Log_de_estados lg,String nuevoEstado) {
		this.estadoInicial = lg.getEstado_final();
		this.id_usuario = lg.getId_usuario();
		this.solicitud = lg.getSolicitud();
		this.estadoFinal = nuevoEstado;
	}
	@Override
  	public int compareTo(Log_de_estados l) {
    	return (getFecha()).compareTo(l.getFecha());
  	}
	
}
