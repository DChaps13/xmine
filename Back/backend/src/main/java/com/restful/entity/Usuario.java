package com.restful.entity;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="USUARIO")
@Entity
public class Usuario  {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_USUARIO")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "APELLIDO_PATERNO")
	private String apellido_paterno;
	
	@Column(name = "APELLIDO_MATERNO")
	private String apellido_materno;
	
	@Column(name = "USERNAME")
	private String username;
	
	@Column(name  = "CONTRASENA")
	private String contrasena;
	
	@Column(name = "ACTIVO")
	private boolean activo;
	
	@Column(name = "ADMIN")
	private boolean admin;
	
	@Column(name = "ROL")
	//1 -> admin ; 2 -> supervisor; 3 -> proveedor; 4 -> operario
	private int rol;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "ID_PROVEEDOR",referencedColumnName ="ID_PROVEEDOR" )
	private Proveedor proveedor;
	
	@OneToMany(mappedBy = "usuario",cascade = CascadeType.ALL)
	private List<Solicitud> solicitudes = new ArrayList<>();
	
	public Map<String, String> toJson(){
		Map<String,String> d = new HashMap<String, String>();
		d.put("id", Integer.toString(id) );
		d.put("nombre", nombre);
		d.put("apellido_paterno", apellido_paterno);
		d.put("apellido_paterno", apellido_materno);
		d.put("activo", Boolean.toString(activo) );
		d.put("admin", Boolean.toString(admin) );
		
		return d;
	}
	
	public void setRol(int rol) {
		this.rol = rol;
	}
	public int getRol() {
		return rol;
	}
	public void setSolicitudes(List<Solicitud> solicitudes) {
		this.solicitudes = solicitudes;
	}
	
	public List<Solicitud> getSolicitudes() {
		return solicitudes;
	}
	
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public Proveedor getProveedor() {
		return proveedor;
	}
	
	public boolean isAdmin() {
		return admin;
	}
	
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	
	
	public Usuario(int id, String nombre, String apellido_paterno, String apellido_materno, String username,
			String contrasena, boolean activo, boolean admin, int rol, Proveedor proveedor,
			List<Solicitud> solicitudes) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		this.username = username;
		this.contrasena = contrasena;
		this.activo = activo;
		this.admin = admin;
		this.rol = rol;
		this.proveedor = proveedor;
		this.solicitudes = solicitudes;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nombre=" + nombre + ", apellido_paterno=" + apellido_paterno
				+ ", apellido_materno=" + apellido_materno + ", username=" + username + ", contrasena=" + contrasena
				+ ", activo=" + activo + ", admin=" + admin + ", rol=" + rol + ", proveedor=" + proveedor
				+ ", solicitudes=" + solicitudes + "]";
	}

	public Usuario() {
		
	}
}
