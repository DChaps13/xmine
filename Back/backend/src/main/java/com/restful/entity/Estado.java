package com.restful.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "ESTADO")
@Entity
public class Estado {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_ESTADO")
	private int id;
	
	@Column(name = "NOMBRE_ESTADO")
	private String nombre;

	@OneToMany(mappedBy = "estado",cascade = CascadeType.ALL)
	private List<Solicitud> solicitudes = new ArrayList<>();
	
	public void setSolicitudes(List<Solicitud> solicitudes) {
		this.solicitudes = solicitudes;
	}
	public List<Solicitud> getSolicitudes() {
		return solicitudes;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	@Override
	public String toString() {
		return "Estado [id=" + id + ", nombre=" + nombre + ", solicitudes=" + solicitudes + "]";
	}
	public Estado(int id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	public Estado() {
		// TODO Auto-generated constructor stub
	}
}
