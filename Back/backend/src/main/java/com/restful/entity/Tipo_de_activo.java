package com.restful.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "TIPO_DE_ACTIVO")
@Entity
public class Tipo_de_activo {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_ACTIVO")
	private int id;
	
	@Column(name = "NOMBRE_TIPO")
	private String nombre_tipo;
	
	@Column(name = "CRITICIDAD")
	private int criticidad;
	
	@OneToMany(mappedBy = "tipo_de_activo",cascade = CascadeType.ALL)
	private List<Activo> activos = new ArrayList<>();
	
	
	@OneToMany(mappedBy = "id_tipo_de_activo",cascade = CascadeType.ALL)
	private List<Contrato_x_tipo_de_activo> contratos_x_tipo_de_activos = new ArrayList<>();
	
	public Tipo_de_activo() {
		// TODO Auto-generated constructor stub
	}	

	
	public Tipo_de_activo(int id, String nombre_tipo, int complejidad, List<Activo> activos,
			List<Contrato_x_tipo_de_activo> contratos_x_tipo_de_activos) {
		super();
		this.id = id;
		this.nombre_tipo = nombre_tipo;
		this.criticidad = complejidad;
		this.activos = activos;
		this.contratos_x_tipo_de_activos = contratos_x_tipo_de_activos;
	}


	@Override
	public String toString() {
		return "Tipo_de_activo [id=" + id + ", nombre_tipo=" + nombre_tipo + ", criticidad=" + criticidad
				+ ", activos=" + activos + ", contratos_x_tipo_de_activos=" + contratos_x_tipo_de_activos + "]";
	}


	public int getCriticidad() {
		return criticidad;
	}


	public void setCriticidad(int complejidad) {
		this.criticidad = complejidad;
	}


	public void setContratos_x_tipo_de_activos(List<Contrato_x_tipo_de_activo> contratos_x_tipo_de_activos) {
		this.contratos_x_tipo_de_activos = contratos_x_tipo_de_activos;
	}
	public List<Contrato_x_tipo_de_activo> getContratos_x_tipo_de_activos() {
		return contratos_x_tipo_de_activos;
	}
	
	public void setActivos(List<Activo> activos) {
		this.activos = activos;
	}
	public List<Activo> getActivos() {
		return activos;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre_tipo() {
		return nombre_tipo;
	}

	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}

	
	public Tipo_de_activo(int id, String nombre_tipo) {
		this.id = id;
		this.nombre_tipo = nombre_tipo;
	}
	
	
}
