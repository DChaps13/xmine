package com.restful.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Table(name = "PROVEEDOR")
@Entity
public class Proveedor {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_PROVEEDOR")
	private int id;
	
	@Column(name = "RUC")
	private String ruc;
	
	@Column(name = "RAZON_SOCIAL")
	private String razon_social;
	
	@Column(name = "CALIFICACION")
	private double  calificacion;
	
	@Column(name = "DIRECCION")
	private String direccion;
	
	@Column(name  = "TELEFONO_CONTACTO")
	private String telefono_contacto;
	
	@Column(name = "NOMBRE_CONTACTO")
	private String nombre_contacto;
	
	@Column(name = "ESTADO")
	private boolean estado=true;
	
	@Column(name = "UBIGEO")
	private String ubigeo;

	@Column(name = "CANTIDAD_CALIFICADO")
	private int cantidad_calificado;
	
	public void setCantidad_calificado(int cantidad_calificado) {
		this.cantidad_calificado = cantidad_calificado;
	}
	public int getCantidad_calificado() {
		return cantidad_calificado;
	}
	
	@Column(name = "BLOQUEADO")
	private boolean bloqueado = false;
	
	public boolean isBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	@OneToOne(mappedBy = "proveedor")
	private Usuario usuario;
	
	@OneToMany(mappedBy = "proveedor",cascade = CascadeType.ALL)
	@JsonIgnoreProperties("proveedor")
	private List<Solicitud> solicitudes = new ArrayList<>();
	
	@OneToMany(mappedBy = "proveedor",cascade = CascadeType.ALL)
	@JsonIgnoreProperties({"proveedor", "contratos_x_tipo_de_activos"})
	private List<Contrato> contratos = new ArrayList<>();
	
	public void setSolicitudes(List<Solicitud> solicitudes) {
		this.solicitudes = solicitudes;
	}
	public List<Solicitud> getSolicitudes() {
		return solicitudes;
	}
	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}
	public List<Contrato> getContratos() {
		return contratos;
	}
	
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}
	public String getRazon_social() {
		return razon_social;
	}
	
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public String getNombre_contacto() {
		return nombre_contacto;
	}

	public void setNombre_contacto(String nombre_contacto) {
		this.nombre_contacto = nombre_contacto;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	
	

	public Proveedor(int id, String ruc, String razon_social, double calificacion, String direccion,
			String telefono_contacto, String nombre_contacto, boolean estado, String ubigeo) {
		super();
		this.id = id;
		this.ruc = ruc;
		this.razon_social = razon_social;
		this.calificacion = calificacion;
		this.direccion = direccion;
		this.telefono_contacto = telefono_contacto;
		this.nombre_contacto = nombre_contacto;
		this.estado = estado;
		this.ubigeo = ubigeo;
	}
	@Override
	public String toString() {
		return "Proveedor [id=" + id + ", ruc=" + ruc + ", razon_social=" + razon_social + ", calificacion="
				+ calificacion + ", direccion=" + direccion + ", telefono_contacto=" + telefono_contacto
				+ ", nombre_contacto=" + nombre_contacto + ", estado=" + estado + ", ubigeo=" + ubigeo + ", usuario="
				+ usuario + ", solicitudes=" + solicitudes + ", contratos=" + contratos + "]";
	}
	public Proveedor() {
		// TODO Auto-generated constructor stub
	}

	
	
	
}
