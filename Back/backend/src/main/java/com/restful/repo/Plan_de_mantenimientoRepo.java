package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Plan_de_mantenimiento;

@Repository("plan_de_mantenimientoRepo")
public interface Plan_de_mantenimientoRepo extends JpaRepository<Plan_de_mantenimiento, Integer>{

}
