package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Persona;


import java.util.List;

@Repository("repositorio")
public interface IPersona  extends JpaRepository<Persona, Integer>{
	public abstract Persona findByNombre(String nombre);
	
	public abstract List<Persona> findByApellido(String apellido);
	
	public abstract Persona findByNombreAndApellido(String nombre, String apellido);
	
	public abstract Persona findByNombreAndId(String nombre,int id);
}
 