package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Proveedor;

@Repository("proveedorRepo")
public interface ProveedorRepo extends JpaRepository<Proveedor, Integer>{
	
}
