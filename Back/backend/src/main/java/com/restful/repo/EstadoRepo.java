package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Estado;

@Repository("estadoRepo")
public interface EstadoRepo extends JpaRepository<Estado, Integer>{

}
