package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Contrato;

@Repository("contratoRepo")
public interface ContratoRepo extends JpaRepository<Contrato, Integer>{
    public abstract Contrato findByIdAndActivo(int id,boolean activo);
}
