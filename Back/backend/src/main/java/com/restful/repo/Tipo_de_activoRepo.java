package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Tipo_de_activo;

@Repository("tipo_de_activoRepo")
public interface Tipo_de_activoRepo extends JpaRepository<Tipo_de_activo, Integer>{

}
