package com.restful.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.restful.entity.Log_de_estados;
import com.restful.entity.Solicitud;

@Repository("log_de_estadosRepo")
public interface Log_de_estadosRepo  extends JpaRepository<Log_de_estados, Integer>{
	
	public abstract List<Log_de_estados> findBySolicitudAndEstadoFinal(Solicitud F,String e);
	
	
	//public abstract List<Log_de_estados> findBySolicitudAndEstadoFinal(Solicitud F,String e);
	/*
	@Query(value = "SELECT * FROM pruebas8.ACTIVO A WHERE A.ESTADO = ?1", nativeQuery = true)
	public abstract List<Activo> findByEstado(boolean estado);*/
	
	@Query(value = "SELECT * FROM log_de_estados WHERE estado_final= ?1 AND MONTH(FECHA) = ?2",nativeQuery = true)
	public abstract List<Log_de_estados> findByEstadoFinalAndFechaBetween(String estadoFinal,int mes);
}
