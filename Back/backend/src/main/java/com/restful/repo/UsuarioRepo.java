package com.restful.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Usuario;

@Repository("UsuarioRepo")
public interface UsuarioRepo extends JpaRepository<Usuario, Integer>{
	
	public abstract Usuario findByUsernameAndContrasena(String username, String contrasena);
	public abstract List<Usuario> findByActivo(boolean activo);
}
