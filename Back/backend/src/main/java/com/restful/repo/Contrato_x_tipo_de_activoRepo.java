package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Contrato_x_tipo_de_activo;

@Repository("contrato_x_tipo_de_activoRepo")
public interface Contrato_x_tipo_de_activoRepo  extends JpaRepository<Contrato_x_tipo_de_activo, Integer>{

}
