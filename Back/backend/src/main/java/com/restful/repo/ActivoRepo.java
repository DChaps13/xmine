package com.restful.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.restful.entity.Activo;

@Repository("activoRepo")
public interface ActivoRepo extends JpaRepository<Activo, Integer>{
	@Query(value = "SELECT * FROM pruebas8.ACTIVO A WHERE A.ESTADO = ?1", nativeQuery = true)
	public abstract List<Activo> findByEstado(boolean estado);
	
	public abstract Activo findByIdAndEstado(int id,boolean estado);
	
	public abstract Activo findByPlacaAndEstado(String placa, boolean estado);
}	
