package com.restful.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Solicitud;

@Repository("solicitudRepo")
public interface SolicitudRepo extends JpaRepository<Solicitud, Integer>{

	public abstract List<Solicitud> findByActivo(boolean activo);

	public abstract Solicitud findByIdAndActivo(int id,boolean activo);
}

