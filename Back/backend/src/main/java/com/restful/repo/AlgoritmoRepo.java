package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Algoritmo;

@Repository("algoritmoRepo")
public interface AlgoritmoRepo extends JpaRepository<Algoritmo,Integer> {
	
}
