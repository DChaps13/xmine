package com.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Auditoria;

@Repository("auditoriaRepo")
public interface AuditoriaRepo extends JpaRepository<Auditoria, Integer>{

}
