package com.restful.convert;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.restful.entity.Persona;
import com.restful.model.MPersona;

@Component("convertidor")
public class CPersona {
	public List<MPersona> convertirLista (List<Persona> personas){
		List<MPersona> mpersonas = new ArrayList<>();
		
		for(Persona persona: personas) {
			mpersonas.add(new MPersona(persona));
		}
		
		return mpersonas;
	}
}
