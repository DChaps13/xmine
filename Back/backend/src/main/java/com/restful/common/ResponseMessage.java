package com.restful.common;

public class ResponseMessage {

	private int codigo_operacion;
	
	private String mensaje;

	public int getCodigo_operacion() {
		return codigo_operacion;
	}

	public void setCodigo_operacion(int codigo_operacion) {
		this.codigo_operacion = codigo_operacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "ResponseMessage [codigo_operacion=" + codigo_operacion + ", mensaje=" + mensaje + "]";
	}

	public ResponseMessage(int codigo_operacion, String mensaje) {
		this.codigo_operacion = codigo_operacion;
		this.mensaje = mensaje;
	}

	public ResponseMessage() {
		// TODO Auto-generated constructor stub
	}


	
	
}
