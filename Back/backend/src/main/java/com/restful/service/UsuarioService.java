package com.restful.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import com.restful.entity.Usuario;
import com.restful.model.UsuarioModel;
import com.restful.repo.UsuarioRepo;

@Service("usuarioService")
public class UsuarioService {
	@Autowired
	@Qualifier("UsuarioRepo")
	private UsuarioRepo usuarioRepo;
	private static final Log logger = LogFactory.getLog(UsuarioService.class);

	//CREAR
	public boolean crearUsuario(Usuario u) {
		logger.info("CREANDO UN USUARIO");
		u.setActivo(true);
		//u.setContrasena(u.getUsername());
		usuarioRepo.save(u);
		return true;
	}
	
	//ACTUALIZAR
	public boolean actualizarUsuario(Usuario a) {
		logger.info("ACTUALIZAR EL USUARIO CON ID: "+a.getId());
		if(usuarioRepo.existsById(a.getId())) {
			usuarioRepo.save(a);
			logger.info("SE ACTUALIZO EL USUARIO CON ID: "+a.getId());
			return true;
		}else {
			logger.error("NO SE ACTUALIZO EL USUARIO CON ID: "+a.getId());
			return false;
		}
	}
	
	//ELIMINAR 
	public boolean borrarUsuario(int idU) {
		logger.info("ELIMINANDO USUARIO CON ID: "+idU);
		Optional<Usuario> u = usuarioRepo.findById(idU);
		if(u.isPresent()) {
			Usuario aux = u.get();
			aux.setActivo(false);
			usuarioRepo.save(aux);
			logger.info("USUARIO ELIMINADO CON ID: "+idU);
			return true;
		}else {
			logger.error("ERROR AL ELIMINAR AL USUARIO CON ID: "+idU);
			return false;
		}
	}
	//LISTAR USUARIOS
	public List<UsuarioModel> listarUsuario(){
		logger.info("LISTAR TODOS LOS USUARIO");
		List<Usuario> l = usuarioRepo.findByActivo(true);
		List<UsuarioModel> lModel = new ArrayList<UsuarioModel>();
		for(Usuario u : l) {
			UsuarioModel aux= new UsuarioModel(u);
			lModel.add(aux);
		}
		return lModel;
	}
	
}
