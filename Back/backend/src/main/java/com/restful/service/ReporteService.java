package com.restful.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.restful.entity.Contrato;
import com.restful.entity.Contrato_x_tipo_de_activo;
import com.restful.entity.Log_de_estados;
import com.restful.entity.Proveedor;
import com.restful.entity.Solicitud;
import com.restful.entity.Tipo_de_activo;
import com.restful.model.ProveedorModel;
import com.restful.model.Tipo_de_activoModel;
import com.restful.model.reporteCapacidad;
import com.restful.model.reporteGeneral1;
import com.restful.repo.Log_de_estadosRepo;
import com.restful.repo.ProveedorRepo;
import com.restful.repo.SolicitudRepo;
import com.restful.repo.Tipo_de_activoRepo;

@Service("reporteService")
public class ReporteService {
	@Autowired
	@Qualifier("log_de_estadosRepo")
	Log_de_estadosRepo logDeEstadoRepo;
	
	@Autowired
	@Qualifier("solicitudRepo")
	SolicitudRepo solicitudRepo;

	@Autowired
	@Qualifier("proveedorRepo")
	ProveedorRepo proveedorRepo;

	@Autowired
	@Qualifier("tipo_de_activoRepo")
	Tipo_de_activoRepo tipo_de_activoRepo;

	private static final Log logger = LogFactory.getLog(ReporteService.class);
	
	@SuppressWarnings("deprecation")
	public List<reporteGeneral1> capacidadDeUso(int mes){
		List<reporteGeneral1> listaG = new ArrayList<reporteGeneral1>();
		//List<reporteCapacidad> lstTipoActivo = new ArrayList<reporteCapacidad>();
		
		List<Log_de_estados> l = logDeEstadoRepo.findByEstadoFinalAndFechaBetween("ATENDIDA", mes);
		logger.info("ENCONTRO LA ATENDIDA");
		for (Log_de_estados lg : l) {
			//Log_de_estadosModel lgAux = new Log_de_estadosModel(lg);
			Solicitud s = lg.getSolicitud();
			//logger.info("ENCONTRO EL LOG DE LA SOLICITUD "+s.getId());
			
			Log_de_estados aux = logDeEstadoRepo.findBySolicitudAndEstadoFinal(s, "EN ATENCION").get(0);
			
			
			logger.info("ENCONTRO EL LOG DE LA SOLICITUD "+s.getId());
			
			int diasUsado=0;
			if(aux == null) {
				diasUsado= lg.getFecha().getDate();
			}else {
				int auxDia = aux.getFecha().getDate();
				diasUsado =lg.getFecha().getDate() - auxDia+1;
				
			}
			logger.info("DIAS USADO: "+diasUsado);
			// saco datos de la solicitud
			// verifico si esta el proveedor en la listaG
			// si esta acumulo valores si no solo agrego
			int estaProveedor=0;
			
			for (reporteGeneral1 rptg1 : listaG) { // verificar que esta el proveedor
				if(rptg1.getProveedor().getId() == s.getProveedor().getId()) {
					// verificar que este el tipo de activo
					logger.info("SE ENCONTRE EL PROVEEDOR ID "+s.getProveedor().getId());
					logger.info("verificar que este el tipo de activo");
					int estaTipoActivo=0;
					for (reporteCapacidad repCap : rptg1.getLstTipoActivo()) {
						if(repCap.getTpActivoModel().getId() == s.getActivo_movil().getTipo_de_activo().getId()) {
							// encontro el tipo de activo
							logger.info("SE ENCONTRO EL ACTIVO ID "+s.getActivo_movil().getTipo_de_activo().getId());
							if(s.getTipo() == 2) {
								// reparacion
								if(repCap.getTipo_solicitud_repa_cap() == 0) {
									// poner la capacidad
								}
								int x = repCap.getTipo_solicitud_repa_usado()+ diasUsado;
								repCap.setTipo_solicitud_repa_usado(x);
								int  y =  repCap.getTipo_solicitud_repa_cap();
								if(y==0) y =-1;
								repCap.setTipo_solicitud_repa_usado_porc((double)x/y);
							}else {
								// mantenimiento
								int x = repCap.getTipo_solicitud_mant_usado()+ diasUsado;
								repCap.setTipo_solicitud_mant_usado(x);
								int  y =  repCap.getTipo_solicitud_mant_cap();
								repCap.setTipo_solicitud_mant_usado_porc((double)x/y);
							}
							repCap.actualizarPorcentajegeneral();
							estaTipoActivo=1;
							break;	
						}
						
					}
					
					if(estaTipoActivo == 0) {
						//no encontro y tengo q agregar el tipo de activo 
						logger.info("NO SE ENCONTRO EL ACTIVO ID "+s.getActivo_movil().getTipo_de_activo().getId());
						
						reporteCapacidad r = new reporteCapacidad();
						r.setTpActivoModel(new Tipo_de_activoModel(s.getActivo_movil().getTipo_de_activo()));
						List<Contrato>lstContrato =s.getProveedor().getContratos();
						for (Contrato cont : lstContrato) {
							if(cont.isActivo()) {
								List<Contrato_x_tipo_de_activo> lstContratoXTipo= cont.getContratos_x_tipo_de_activos();
								for (Contrato_x_tipo_de_activo contratoXTipo : lstContratoXTipo) {
									System.out.println(contratoXTipo.getTipo_de_activo().getId() +" , "+s.getActivo_movil().getTipo_de_activo().getId());
									if(contratoXTipo.getTipo_de_activo().getId() == s.getActivo_movil().getTipo_de_activo().getId()) {
										
										// pregunto su el activo esta en el contrato x acitvo 
										r.setTipo_solicitud_mant_cap(contratoXTipo.getCapacidad_mantenimiento_inicial());
										r.setTipo_solicitud_repa_cap(contratoXTipo.getCapacidad_reparacion_inicial());
										System.out.println(r.getTipo_solicitud_mant_cap() +","+r.getTipo_solicitud_repa_cap());
										if(s.getTipo() == 2) {
											// reparacion
											logger.info(s.getActivo_movil().getId() + " <- id activo de reparacion");
											int x = r.getTipo_solicitud_repa_usado()+ diasUsado;
											r.setTipo_solicitud_repa_usado(x);
											int  y =  r.getTipo_solicitud_repa_cap();
											r.setTipo_solicitud_repa_usado_porc((double)x/y);
										}else {
											// mantenimiento
											logger.info(s.getActivo_movil().getId() + " <- id activo de mantenimiento");
											int x = r.getTipo_solicitud_mant_usado()+ diasUsado;
											r.setTipo_solicitud_mant_usado(x);
											int  y =  r.getTipo_solicitud_mant_cap();
											r.setTipo_solicitud_mant_usado_porc((double)x/y);
										}
										r.actualizarPorcentajegeneral();
										
										
										
										break;
									}
								}
								break;
							}
						}
						rptg1.addReporteCapacidad(r);
						
					}
					estaProveedor=1;
					break;
				}
			}
			if(estaProveedor ==0) {
				reporteGeneral1 repGenAux = new reporteGeneral1();
				repGenAux.setProveedor(new ProveedorModel(s.getProveedor()));
				
				reporteCapacidad repCapAux = new reporteCapacidad();
				// falta agregar el tipo
				repCapAux.setTpActivoModel(new Tipo_de_activoModel(s.getActivo_movil().getTipo_de_activo()));
				List<Contrato>lstContrato =s.getProveedor().getContratos();
				for (Contrato cont : lstContrato) {
					if(cont.isActivo()) {
						List<Contrato_x_tipo_de_activo> lstContratoXTipo= cont.getContratos_x_tipo_de_activos();
						
						for (Contrato_x_tipo_de_activo contratoXTipo : lstContratoXTipo) {
							if(contratoXTipo.getTipo_de_activo().getId() == s.getActivo_movil().getTipo_de_activo().getId()) {
								logger.info(s.getActivo_movil().getId() + " <- id activo que no esta");
								repCapAux.setTipo_solicitud_mant_cap(contratoXTipo.getCapacidad_mantenimiento_inicial());
								repCapAux.setTipo_solicitud_repa_cap(contratoXTipo.getCapacidad_reparacion_inicial());
								if(s.getTipo() == 2) {
									// reparacion
									logger.info(s.getActivo_movil().getId() + " <- id activo de reparacion");
									
									int x = repCapAux.getTipo_solicitud_repa_usado()+ diasUsado;
									repCapAux.setTipo_solicitud_repa_usado(x);
									int  y =  repCapAux.getTipo_solicitud_repa_cap();
									repCapAux.setTipo_solicitud_repa_usado_porc((double)x/y);
								}else {
									// mantenimiento
									int x = repCapAux.getTipo_solicitud_mant_usado()+ diasUsado;
									repCapAux.setTipo_solicitud_mant_usado(x);
									int  y =  repCapAux.getTipo_solicitud_mant_cap();
									repCapAux.setTipo_solicitud_mant_usado_porc((double)x/y);
								}
								repCapAux.actualizarPorcentajegeneral();
								break;
							}
						}
						break;
					}
					
				}
				
				// ya agrego el tipo de activo con sus datos
				repGenAux.addReporteCapacidad(repCapAux);
				repGenAux.actualizarPorcentajeUsoProveedor();
				listaG.add(repGenAux);
				
				
			}
			
			
			
		}
		
		return listaG;
	}

	public List<ProveedorModel> costoPorProveedor(int mes){
		List<Proveedor> lProveedores= proveedorRepo.findAll();
		List<ProveedorModel> lProveedoresM = new ArrayList<ProveedorModel>();

		List<Log_de_estados> l = logDeEstadoRepo.findByEstadoFinalAndFechaBetween("CERRADA", mes);

		for(Proveedor p : lProveedores){
			double costo=0.0;
			ProveedorModel pM = new ProveedorModel(p);
			for(Log_de_estados lg : l){
				Solicitud s = lg.getSolicitud();
				if(s.getProveedor().getId()==p.getId()){
					costo+=s.getCosto_total();
				}
			}
			pM.setCostoActual(costo);
			lProveedoresM.add(pM);
		}
		return lProveedoresM;
	}

	public List<Tipo_de_activoModel> costoPorTipoActivo(int mes){
		List<Tipo_de_activo> lTipos= tipo_de_activoRepo.findAll();
		List<Tipo_de_activoModel> lTiposM = new ArrayList<Tipo_de_activoModel>();

		List<Log_de_estados> l = logDeEstadoRepo.findByEstadoFinalAndFechaBetween("CERRADA", mes);

		for(Tipo_de_activo p : lTipos){
			double costo=0.0;
			Tipo_de_activoModel pM = new Tipo_de_activoModel(p);
			for(Log_de_estados lg : l){
				Solicitud s = lg.getSolicitud();
				if(s.getProveedor().getId()==p.getId()){
					costo+=s.getCosto_total();
				}
			}
			pM.setCostoActual(costo);
			lTiposM.add(pM);
		}
		return lTiposM;
	}
}
