package com.restful.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import com.restful.convert.CPersona;
import com.restful.entity.Persona;
import com.restful.model.MPersona;
import com.restful.repo.IPersona;

@Service ("servicio")
public class PersonaService {
	@Autowired
	@Qualifier("repositorio")
	private IPersona repo; 
	 
	@Autowired
	@Qualifier("convertidor")
	private CPersona cpersona;
	
	private static final Log logger =  LogFactory.getLog(PersonaService.class);
	
	public boolean crear(Persona persona) {
		logger.info("CREANDO PERSONA");
		try {
			repo.save(persona);
			logger.info("PERSONA CREADA");
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("ERROS AL CREAR LA PERSONA");
			return false;
		}
	}
	
	public boolean actualizar(Persona persona) {
		try {
			// se tiene q verificar que exista esa persona
			repo.save(persona);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean borrar(String nombre, int id) {
		try {
			Persona p = repo.findByNombreAndId(nombre, id);
			
			repo.delete(p);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public List<MPersona> obtener(){

		
		
		List<Persona>  personas = repo.findAll() ;
		return cpersona.convertirLista(personas);
	}
	
	public MPersona obtenerPorNombreApellido(String nombre,String apellido) {
		return new MPersona(repo.findByNombreAndApellido(nombre, apellido));
	}
	
	public List<MPersona> obtenerPorApellido(String apellido) {
		return cpersona.convertirLista(repo.findByApellido(apellido));
	}
	
}
