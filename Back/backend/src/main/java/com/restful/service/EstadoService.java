package com.restful.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.restful.entity.Estado;
import com.restful.model.EstadoModel;
import com.restful.repo.EstadoRepo;

@Service("estadoService")
public class EstadoService {
	@Autowired
	@Qualifier("estadoRepo")
	private EstadoRepo estadoRepo;
	private static final Log logger = LogFactory.getLog(EstadoService.class);

	public List<EstadoModel> obtenerEstados() {
		logger.info("LISTAR TODOS LOS ESTADOS EN ESTADO VIGENTE");
		List<Estado> l = estadoRepo.findAll();
		List<EstadoModel> lModel = new ArrayList<EstadoModel>();
		for (Estado estado : l) {
			EstadoModel aux = new EstadoModel(estado);
			lModel.add(aux);
		}

		return lModel;
	}

	public EstadoModel obtenerEstado(int id) {
		logger.info("OBTENER UN ESTADO A PARTIR DE SU ID");
		logger.info(estadoRepo);
		try{
			Optional<Estado> a = estadoRepo.findById(id);
			if (a.isPresent()){
				Estado obj = a.get();
				return new EstadoModel(obj);
			}
			return new EstadoModel();
		}catch(Exception e){
			logger.info(e);
			return null;
		}
		
		
		
		
	}
}
