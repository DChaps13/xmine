package com.restful.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.restful.entity.Proveedor;
import com.restful.entity.Usuario;
import com.restful.model.ContratoModel;
import com.restful.model.ProveedorModel;
import com.restful.model.SolicitudModel;
import com.restful.repo.ProveedorRepo;
import com.restful.repo.UsuarioRepo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("proveedorService")
public class ProveedorService {

	@Autowired
	@Qualifier("UsuarioRepo")
	private UsuarioRepo usuarioRepo;
	
	@Autowired
	@Qualifier("proveedorRepo")
	private ProveedorRepo proveedorRepo;

	@Autowired
	private ContratoService contratoService;

	@Autowired
	private MantenimientoSolicitudes mantenimientoSolicitudes;

	private static final Log logger = LogFactory.getLog(ProveedorService.class);
	
	//LISTAR
	public List<ProveedorModel> obtenerProveedores(){
		logger.info("LISTAR TODOS LOS PROVEEDORES");
		List<Proveedor> l = proveedorRepo.findAll();
		List<ProveedorModel> lModel = new ArrayList<ProveedorModel>();
		for (Proveedor proveedor : l) {
			ProveedorModel aux  = new ProveedorModel(proveedor);
			if (aux.isEstado())	lModel.add(aux);
		}
		return lModel;
	}
	
	//CREAR
	public Proveedor crearProvedor(Proveedor p) {
		logger.info("CREANDO UN PROVEEDOR");
		p.setCantidad_calificado(0);
		p.setEstado(true);
		proveedorRepo.save(p);
		Usuario u= new Usuario();
		u.setUsername(p.getRuc());
		u.setContrasena("12345");
		u.setActivo(true);
		u.setAdmin(false);
		u.setRol(3);
		u.setProveedor(p);
		usuarioRepo.save(u);
		logger.info("PROVEEDOR CREADO CON EL ID" + p.getId());
		return p;	
	}
	
	//ACTUALIZAR
	public Proveedor actualizarProveedor(Proveedor p) {
		logger.info("ACTULIZAR PROVEEDOR");
		if (proveedorRepo.existsById(p.getId())) {
			proveedorRepo.save(p);
			logger.info("SE ACTUALIZO EL PROVEEDOR CON ID: "+p.getId());
			return p;
		}else {
			logger.error("SE ACTUALIZO EL PROVEEDOR CON ID: "+p.getId());
			return null;
		}
	}
	
	// ELIMINAR 
	public boolean borrarProveedor(int idProveedor){
		logger.info("ELIMINANDO PROVEEDOR ID : " +idProveedor);
		Optional<Proveedor> p = proveedorRepo.findById(idProveedor);
		if (p.isPresent()) {
			Proveedor aux = p.get();
			logger.info("BORRADO LOGICO ID : "+idProveedor);
			//validar que no tenga solicitudes
			List<SolicitudModel> lSoli = mantenimientoSolicitudes.obtenerSolicitudes();
			for(SolicitudModel s: lSoli){
				if(s.getId_proveedor()==idProveedor) return false;
			}
			//eliminar contratos asociados al proveedor
			List<ContratoModel> lModel = contratoService.obtenerContratos();
            for (ContratoModel c : lModel){
                if(c.getProveedor().getId()==idProveedor){
                    contratoService.borrarContrato(c.getId());
                }
            }
			aux.setEstado(false);
			proveedorRepo.save(aux);
			Optional<Usuario> auxU = usuarioRepo.findById(aux.getUsuario().getId());
			if(auxU.isPresent()) {
				Usuario u = auxU.get();
				u.setActivo(false);
				usuarioRepo.save(u);
			}else {
				logger.error("FALLO EN BORRADO LOGICO ID : "+idProveedor + " NO TIENE ID USUARIO");
				return false;
			}
			return true;
		}else {
			logger.error("FALLO EN BORRADO LOGICO ID : "+idProveedor);
			return false;
		}
	}
	public String obtenerNombre(int id){
		logger.info("OBTENER UN ESTADO A PARTIR DE SU ID");
		
		Optional<Proveedor> a = proveedorRepo.findById(id);
		if (a.isPresent()){
			Proveedor obj = a.get();
			return obj.getRazon_social();
		}
		return "";
	}
	
	public boolean bloquearProveedor(int idP) {
		logger.info("SE ESTA BLOQUEANDO AL PROVEEDOR CON ID: "+idP);
		Optional<Proveedor> a = proveedorRepo.findById(idP);
		if(a.isPresent()) {
			Proveedor p = a.get();
			p.setBloqueado(true);
			proveedorRepo.save(p);
			logger.info("SE HA BLOQUEADO AL PROVEEDOR CON ID: "+idP);
			return true;
		}else {
			logger.info("NO SE ENCUENTRA EL PROVEEDOR CON ID: "+idP);
			return false;
		}
		
	}
	
	
	public boolean verEstadoBloqueado(int idP) {
		logger.info("SE ESTA CONSULTANDO EL ESTADO DE BLOQUEO AL PROVEEDOR CON ID: "+idP);
		Optional<Proveedor>a = proveedorRepo.findById(idP);
		if(a.isPresent()) {
			Proveedor p = a.get();
			if(p.isBloqueado()) return true;
			else return false;
		}else {
			logger.info("NO SE ENCUENTRA EL PROVEEDOR CON ID: "+idP);
			return false;
		}	
	}
	
	public boolean desbloquearProveedor(int idP) {
		logger.info("SE ESTA DESBLOQUEANDO AL PROVEEDOR CON ID: "+idP);
		Optional<Proveedor> a = proveedorRepo.findById(idP);
		if(a.isPresent()) {
			Proveedor p = a.get();
			p.setBloqueado(false);
			proveedorRepo.save(p);
			logger.info("SE HA DESBLOQUEADO AL PROVEEDOR CON ID: "+idP);
			return true;
		}else {
			logger.info("NO SE ENCUENTRA EL PROVEEDOR CON ID: "+idP);
			return false;
		}		
	}
	
}
