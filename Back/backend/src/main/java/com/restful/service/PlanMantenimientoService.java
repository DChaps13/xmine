package com.restful.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.restful.entity.Plan_de_mantenimiento;
import com.restful.model.Plan_de_mantenimientoModel;
import com.restful.repo.Plan_de_mantenimientoRepo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("planMantenimientoService")
public class PlanMantenimientoService {

	@Autowired
	@Qualifier("plan_de_mantenimientoRepo")
	private Plan_de_mantenimientoRepo plan_de_mantenimientoRepo;
	
	private static final Log logger = LogFactory.getLog(ProveedorService.class);
	
	//LISTAR
	public List<Plan_de_mantenimientoModel> obtenerPlanes(){
		logger.info("LISTAR TODOS LOS PLANES");
		List<Plan_de_mantenimiento> l = plan_de_mantenimientoRepo.findAll();
		List<Plan_de_mantenimientoModel> lModel = new ArrayList<Plan_de_mantenimientoModel>();
		for (Plan_de_mantenimiento plan : l) {
			Plan_de_mantenimientoModel aux  = new Plan_de_mantenimientoModel(plan);
			if(aux.isActivo()) lModel.add(aux);
		}
		return lModel;
	}
	
	public boolean borrarPlan(int id) {
        logger.info("ELIMINANDO PLAN ID : " + id);
        Optional<Plan_de_mantenimiento> p = plan_de_mantenimientoRepo.findById(id);
		if (p.isPresent()) {
			Plan_de_mantenimiento aux = p.get();
            aux.setActivo(false);
            plan_de_mantenimientoRepo.save(aux);
			logger.info("BORRADO LOGICO ID : "+id);            
			return true;
		}else {
			logger.error("FALLO EN BORRADO LOGICO ID : "+id);
			return false;
		}
    }
    
    public boolean crearPlan(Plan_de_mantenimiento p) {
        try{
            logger.info("SE ESTA REGISTRANDO EL PLAN");
            plan_de_mantenimientoRepo.save(p);
            return true;
        }catch(Exception e){
			//TODO: handle exception
			logger.error(e.getMessage());
			return false;
        }
	}
	
	public boolean actualizarPlan(Plan_de_mantenimiento p) {
		try {
			logger.info("ACTUALIZAR PLAN");
			if (plan_de_mantenimientoRepo.existsById(p.getId())) {
				plan_de_mantenimientoRepo.save(p);
				logger.info("PLAN ACTUALIZADO");
				return true;
			}else {
				logger.error("NO EXISTE ESE ID DEL PLAN");
				return false;
			}
		}catch (Exception e) {
			//TODO: handle exception
			logger.error(e.getMessage());
			return false;
		}
	}
	
}
