package com.restful.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.restful.entity.Contrato_x_tipo_de_activo;
import com.restful.model.Contrato_x_tipo_de_activoModel;
import com.restful.repo.Contrato_x_tipo_de_activoRepo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("contratoTipoService")
public class ContratoTipoService {

	@Autowired
	@Qualifier("contrato_x_tipo_de_activoRepo")
	private Contrato_x_tipo_de_activoRepo contratoTipoRepo;
	
	private static final Log logger = LogFactory.getLog(ProveedorService.class);
	
	//LISTAR
	public List<Contrato_x_tipo_de_activoModel> obtenerContratosTipo(){
		logger.info("LISTAR TODOS LOS PROVEEDORES");
		List<Contrato_x_tipo_de_activo> l = contratoTipoRepo.findAll();
		List<Contrato_x_tipo_de_activoModel> lModel = new ArrayList<Contrato_x_tipo_de_activoModel>();
		for (Contrato_x_tipo_de_activo contratoTipo : l) {
			Contrato_x_tipo_de_activoModel aux  = new Contrato_x_tipo_de_activoModel(contratoTipo);
			if(aux.isActivo()) lModel.add(aux);
		}
		return lModel;
	}
	
	public boolean borrarContratoTipo(int idContratoTipo) {
        logger.info("ELIMINANDO CONTRATO ID : " + idContratoTipo);
        Optional<Contrato_x_tipo_de_activo> p = contratoTipoRepo.findById(idContratoTipo);
		if (p.isPresent()) {
			Contrato_x_tipo_de_activo aux = p.get();
            aux.setActivo(false);
            contratoTipoRepo.save(aux);
			logger.info("BORRADO LOGICO ID : "+idContratoTipo);            
			return true;
		}else {
			logger.error("FALLO EN BORRADO LOGICO ID : "+idContratoTipo);
			return false;
		}
	}
	
}
