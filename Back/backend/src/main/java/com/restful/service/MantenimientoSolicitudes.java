package com.restful.service;

import static java.util.Comparator.comparing;

import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.restful.algorithm.SimulatedAnnealing;
import com.restful.entity.Activo;
import com.restful.entity.Algoritmo;
import com.restful.entity.Contrato;
import com.restful.entity.Contrato_x_tipo_de_activo;
import com.restful.entity.Estado;
import com.restful.entity.Log_de_estados;
import com.restful.entity.Proveedor;
import com.restful.entity.Solicitud;
import com.restful.model.ContratoModel;
import com.restful.model.Contrato_x_tipo_de_activoModel;
import com.restful.model.EstadoModel;
import com.restful.model.SolicitudModel;
import com.restful.repo.ActivoRepo;
import com.restful.repo.AlgoritmoRepo;
import com.restful.repo.ContratoRepo;
import com.restful.repo.Contrato_x_tipo_de_activoRepo;
import com.restful.repo.EstadoRepo;
import com.restful.repo.Log_de_estadosRepo;
import com.restful.repo.ProveedorRepo;
import com.restful.repo.SolicitudRepo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("mantenimientoSolicitudes")
public class MantenimientoSolicitudes {
	@Autowired
	@Qualifier("log_de_estadosRepo")
	private Log_de_estadosRepo logRepo;
	
	@Autowired
	@Qualifier("solicitudRepo")
	private SolicitudRepo solicitudRepo;

	@Autowired
	private EstadoService estadoService;

	@Autowired
	@Qualifier("activoRepo")
	private ActivoRepo activoRepo;

	@Autowired
	@Qualifier("proveedorRepo")
	private ProveedorRepo proveedorRepo;

	@Autowired
	private ContratoTipoService contratoTipoService;
	
	@Autowired
	@Qualifier("estadoRepo")
	private EstadoRepo estadoRepo;
	
	@Autowired
	@Qualifier("algoritmoRepo")
	private AlgoritmoRepo algoritmoRepo;

	@Autowired
	@Qualifier("contrato_x_tipo_de_activoRepo")
	Contrato_x_tipo_de_activoRepo contratoTipoRepo;
	
	@Autowired
	@Qualifier("log_de_estadosRepo")
	Log_de_estadosRepo logDeEstadoRepo;

	@Autowired
	@Qualifier("contratoRepo")
	ContratoRepo contratoRepo;

	private static final Log logger = LogFactory.getLog(MantenimientoSolicitudes.class);
	
	public boolean registrarSolicitud(Solicitud s) {
		logger.info("SE ESTA REGISTRANDO LA SOLICITUD");
		Estado e = new Estado(1, "REGISTRADA");
		s.setEstado(e);
		
		if(s.getProveedor()!=null){
			e= new Estado(2,"ASIGNADA");
			s.setEstado(e);
		}

		List<SolicitudModel> lstSolicitudes= obtenerSolicitudes();
		int idActivo = s.getActivo_movil().getId();
		for (SolicitudModel soli : lstSolicitudes){
			int idActivoAux=soli.getId_activo();
			int idEstadoAux=soli.getEstado().getId();
			if (idActivoAux==idActivo && soli.getTipo()==s.getTipo() && idEstadoAux!=5 && idEstadoAux!=6 && idEstadoAux!=7){
				return false;
			}
		}
		/*Agregando la complejidad */
		int idActivoMovil = s.getActivo_movil().getId();
		System.out.println(idActivoMovil+ "ID ACTIVO MOVIL");
		Activo criticidad =  activoRepo.findByIdAndEstado(idActivoMovil, true);
		int c=criticidad.getTipo_de_activo().getCriticidad();
		s.setComplejidad(s.getPrioridad() *c  );
		solicitudRepo.save(s);
		
		Log_de_estados lg = new Log_de_estados("INI", s.getEstado().getNombre().toUpperCase(), s.getUsuario().getId(),s.getId());
		logger.info("SE ESTA REGISTRANDO LA SOLICITUD ID: "+s.getId() + " AL LOG DE ESTADO");
		logRepo.save(lg);
		return true;
	}
	
	public boolean actualizar(Solicitud solicitud) {
		try {
			logger.info("ACTUALIZAR SOLICITUD");
			if (solicitudRepo.existsById(solicitud.getId())) {
				solicitudRepo.save(solicitud);
				logger.info("SOLICITUD ACTUALIZADA");
				return true;
			}else {
				logger.error("NO EXISTE ESE ID DE LA SOLICITUD");
				return false;
			}
		}catch (Exception e) {
			//TODO: handle exception
			logger.error(e.getMessage());
			return false;
		}
	}
	
	public boolean borrar(int idSoli) {
		logger.info("BORRANDO SOLICITUD ID: "+ idSoli);
		Optional<Solicitud> s  = solicitudRepo.findById(idSoli);
		if (s.isPresent()) {
			Solicitud aux = s.get();
			aux.setActivo(false);
			solicitudRepo.save(aux);
			logger.info("BORRADO LOGICO");
			return true;
		}else {
			logger.error("NO EXISTE LA ID");
			return false;
		}
		
	}

	public boolean calificarSolicitud(int idSoli, double calif, String coment) {
		Optional<Solicitud> s  = solicitudRepo.findById(idSoli);
		if (s.isPresent()) {
			Solicitud aux = s.get();
			aux.setCalificacion(calif);
			aux.setDescripcion(aux.getDescripcion()+ " \nCALIFICACION: "+ coment);
			solicitudRepo.save(aux);
			List<Contrato_x_tipo_de_activo> lAux = aux.getActivo_movil().getTipo_de_activo().getContratos_x_tipo_de_activos();
			for(Contrato_x_tipo_de_activo c : lAux){
				if(c.getContrato().getProveedor().getId()==aux.getProveedor().getId()){
					//SE CALIFICA CONTRATO_X_TIPO_ACTIVO
					double newCalif= (c.getCalificacion_tipo()*c.getCantidad_calificado()+calif)/(c.getCantidad_calificado()+1);
					c.setCalificacion_tipo(newCalif);
					c.setCantidad_calificado(c.getCantidad_calificado()+1);
					contratoTipoRepo.save(c);
					
					//SE CALIFICA CONTRATO
					Contrato cC = c.getContrato();
					newCalif= (cC.getCalificacion_contrato()*cC.getCantidad_calificado()+calif)/(cC.getCantidad_calificado()+1);
					cC.setCalificacion_contrato(newCalif);
					cC.setCantidad_calificado(cC.getCantidad_calificado()+1);
					contratoRepo.save(cC);
					//SE CALIFICA PROVEEDOR
					Proveedor p= aux.getProveedor();
					newCalif=(p.getCalificacion()*p.getCantidad_calificado()+calif)/(p.getCantidad_calificado()+1);
					p.setCalificacion(newCalif);
					p.setCantidad_calificado(p.getCantidad_calificado()+1);
					proveedorRepo.save(p);
					break;
				}
			}
			return true;
		}else {
			return false;
		}
		
	}
	
	
	public List<SolicitudModel> obtenerSolicitudes(){
		logger.info("LISTAR TODAS LAS SOLICITUDES ACTIVAS");
		List<Solicitud> l =  solicitudRepo.findByActivo(true);
		List<SolicitudModel> lModel = new ArrayList<SolicitudModel>();
		for (Solicitud solicitud: l) {
			SolicitudModel aux = new SolicitudModel(solicitud);
			lModel.add(aux);
		}
		List<SolicitudModel> rechazadas = new ArrayList<SolicitudModel>();
		List<SolicitudModel> registradas = new ArrayList<SolicitudModel>();
		List<SolicitudModel> asignadas = new ArrayList<SolicitudModel>();
		List<SolicitudModel> enatenciones = new ArrayList<SolicitudModel>();
		List<SolicitudModel> atendidas = new ArrayList<SolicitudModel>();
		List<SolicitudModel> cerradas = new ArrayList<SolicitudModel>();
		List<SolicitudModel> canceladas = new ArrayList<SolicitudModel>();

		for(SolicitudModel s : lModel){
			int idEstado = s.getEstado().getId();
			switch (idEstado){
				case 1:
					registradas.add(s);
					break;
				case 2:
					asignadas.add(s);
					break;
				case 3:
					enatenciones.add(s);
					break;
				case 4:
					atendidas.add(s);
					break;
				case 5:
					rechazadas.add(s);
					break;
				case 6:
					canceladas.add(s);
					break;
				case 7:
					cerradas.add(s);
				default:
					break;
			}
		}
		registradas.addAll(asignadas);
		registradas.addAll(enatenciones);
		registradas.addAll(atendidas);
		registradas.addAll(rechazadas);
		registradas.addAll(cerradas);
		registradas.addAll(canceladas);
		return registradas;
	}

	public boolean asignar(int tipo){
		logger.info("ASIGNA LAS SOLICITUDES A UN PROVEEDOR");
		Optional<Algoritmo> a = algoritmoRepo.findById(1);
		if(a.isPresent()) {
			Algoritmo aux = a.get();
			aux.setFlag(1);
			algoritmoRepo.save(aux);
		}
		//obtengo lista de solicitudes registradas tipo M=1 o tipo R=2
		List<SolicitudModel> LstSol= obtenerSolicitudes();
		ArrayList<SolicitudModel> LstSolEsp= new ArrayList<>();
		List<SolicitudModel> lstSolicitudes2= obtenerSolicitudes();
		
		EstadoModel registrado = estadoService.obtenerEstado(1);
		for (int i = 0; i < LstSol.size(); i++) {
			if (LstSol.get(i).getTipo()==tipo && LstSol.get(i).getEstado().getId()==registrado.getId()){
				int idActivo = LstSol.get(i).getId_activo();
				int flag2=0;
				for (SolicitudModel soli2 : lstSolicitudes2){
					int idActivoAux=soli2.getId_activo();
					int idEstadoAux=soli2.getEstado().getId();
					if (idActivoAux==idActivo && soli2.getId()!=LstSol.get(i).getId() && (idEstadoAux==2 || idEstadoAux==3 || idEstadoAux==4)){
						flag2=1;
						break;
					}
				}
				if (flag2==0) LstSolEsp.add(LstSol.get(i));
			}
		}
		Collections.sort(LstSolEsp,comparing(SolicitudModel::getComplejidad));
		Collections.reverse(LstSolEsp);
		//obtengo lista de contratos
		
		ArrayList<Contrato_x_tipo_de_activoModel> LstContAux= (ArrayList<Contrato_x_tipo_de_activoModel>)contratoTipoService.obtenerContratosTipo();
		ArrayList<Contrato_x_tipo_de_activoModel> LstCont= new ArrayList<Contrato_x_tipo_de_activoModel>();
		Date localDate= new Date();
		for(Contrato_x_tipo_de_activoModel cxt: LstContAux){
			if(cxt.getContrato().getProveedor().isBloqueado()) continue;
			if(localDate.compareTo(cxt.getContrato().getFecha_fin())>0){
				continue;
			}
			else if(localDate.compareTo(cxt.getContrato().getFecha_inicio())<0){
				continue;
			}
			else{
				LstCont.add(cxt);
			}

		}

		ArrayList<Contrato_x_tipo_de_activoModel> solution;
		char carTipo= 'M';
		if (tipo==2) carTipo='R';
		SimulatedAnnealing sim = new SimulatedAnnealing(carTipo);
		
		solution = sim.solve(LstSolEsp,LstCont,activoRepo);
		logger.info(LstSolEsp.size());
		for (int i = 0; i < LstSolEsp.size(); i++) {
			/*System.out.println("Id del Solicitud: " + LstSolEsp.get(i).getId());
			System.out.println("Id del Proveedor: " + solution.get(i).getContrato().getProveedor().getId());
			System.out.println("Id del ContratoXtipoActivo: " + solution.get(i).getId());
			*/
			int idSol = LstSolEsp.get(i).getId();
			ContratoModel ccc= solution.get(i).getContrato();
			if(ccc== null) continue;
			int idProv= ccc.getProveedor().getId();
			Optional<Solicitud> s  = solicitudRepo.findById(idSol);
			Optional<Proveedor> p = proveedorRepo.findById(idProv);
			
			if(s.isPresent()){
				if(p.isPresent()){
					
					Proveedor pObj = p.get();
					Solicitud obj = s.get();
					Estado es= new Estado(2,"ASIGNADA");
					obj.setProveedor(pObj);

					List<Contrato_x_tipo_de_activo> lAux = obj.getActivo_movil().getTipo_de_activo().getContratos_x_tipo_de_activos();
					for(Contrato_x_tipo_de_activo c : lAux){
						if(c.getContrato().getProveedor().getId()==idProv){
							Contrato cc = c.getContrato();
							if(obj.getTipo()==1){
								cc.setCapacidad_total_mant(cc.getCapacidad_total_mant()-1);
								c.setCapacidad_mantenimiento(c.getCapacidad_mantenimiento()-1);
							} 
							else{
								cc.setCapacidad_total_rep(cc.getCapacidad_total_rep()-1);
								c.setCapacidad_reparacion(c.getCapacidad_reparacion()-1);
							}
							contratoTipoRepo.save(c);
							contratoRepo.save(cc);
						}
					}
					try {
						Log_de_estados lg = new Log_de_estados(obj.getEstado().getNombre(),"ASIGNADA",obj.getUsuario().getId(),obj.getId());
						logger.info("SE ESTA REGISTRANDO LA SOLICITUD ID: "+obj.getId() + " AL LOG DE ESTADO");
						logRepo.save(lg);
						obj.setEstado(es);
						logger.info(pObj.getId());
						actualizar(obj);
					}catch (Exception e) {
						// TODO: handle exception
						logger.error("ERROR EN EL REGISTRO DE LA SOLICITUD ID: "+obj.getId() + " AL LOG DE ESTADO");
					}

					
				}
			}
		}

		if(a.isPresent()) {
			Algoritmo aux = a.get();
			aux.setFlag(0);
			algoritmoRepo.save(aux);
		}	
		return true;
		
	}
	
	public boolean asignarProveedorASolicitud(int idSoli,int idProvee) {
		logger.info("A la solicitud id:"+idSoli+" se le asigna el proveedor id: "+idProvee);
		Optional<Solicitud> s = solicitudRepo.findById(idSoli);
		if(s.isPresent()) {
			Solicitud aux =  s.get();
			Estado es = new Estado(2,"ASIGNADA");
			Proveedor p = new Proveedor();
			p.setId(idProvee);
			aux.setProveedor(p);
			logger.info("SE REALIZO CON EXITO");
			Log_de_estados lg = new Log_de_estados(aux.getEstado().getNombre(),"ASIGNADA",aux.getUsuario().getId(),aux.getId());			
			logger.info("SE ESTA REGISTRANDO LA SOLICITUD ID: "+aux.getId() + " AL LOG DE ESTADO");
			logRepo.save(lg);
			aux.setEstado(es);
			solicitudRepo.save(aux);
			
			
			return true;
		}else {
			logger.error("NO EXISTE LA ID");
		}
		return true;
    }

	public SolicitudModel obtenerSoli(int id){
		Optional<Solicitud> s  = solicitudRepo.findById(id);
		if (s.isPresent()) {
			Solicitud aux = s.get();
			SolicitudModel soli = new SolicitudModel(aux);
			return soli;
		}else {
			logger.error("NO EXISTE LA ID");
			return new SolicitudModel();
		}
	}
	
	public double calcularCosto(Solicitud aux){
		List<Log_de_estados> l1 = logDeEstadoRepo.findBySolicitudAndEstadoFinal(aux,"EN ATENCION");
		List<Log_de_estados> l2 = logDeEstadoRepo.findBySolicitudAndEstadoFinal(aux,"ATENDIDA");		
		Collections.sort(l1); //orden ascendente
		Collections.sort(l2); //orden ascendente
		double suma=0;
		for(int i=0;i<l1.size();i++){
			long diffInMillies= l2.get(i).getFecha().getTime()- l1.get(i).getFecha().getTime();
			
			double diff = (double) diffInMillies / TimeUnit.HOURS.toMillis(1);
			suma+=diff;
		}
		double tarifa=1000000;
		List<Contrato_x_tipo_de_activo> lAux = aux.getActivo_movil().getTipo_de_activo().getContratos_x_tipo_de_activos();
		for(Contrato_x_tipo_de_activo c : lAux){
			if(c.getContrato().getProveedor().getId()==aux.getProveedor().getId()){
				tarifa=c.getTarifa();
			}
		}
		suma*=tarifa;
		return suma;
	}
	public boolean cambiarEstado(int id, int idEstado, String coments){
		Optional<Solicitud> s  = solicitudRepo.findById(id);
		Optional<Estado> e = estadoRepo.findById(idEstado);
		if (s.isPresent()) {
			if( e.isPresent()){
				Solicitud aux = s.get();
				Estado auxE= e.get();
				Log_de_estados lg;
				switch(idEstado){
					case 7:
						lg = new Log_de_estados(aux.getEstado().getNombre(),auxE.getNombre(),aux.getUsuario().getId(),aux.getId());
						logger.info("SE ESTA REGISTRANDO LA SOLICITUD ID: "+aux.getId() + " AL LOG DE ESTADO");
						logRepo.save(lg);
						double costo = calcularCosto(aux);
						aux.setCosto_total(costo);
						break;
					case 4:
						lg = new Log_de_estados(aux.getEstado().getNombre(),auxE.getNombre(),aux.getUsuario().getId(),aux.getId());
						logger.info("SE ESTA REGISTRANDO LA SOLICITUD ID: "+aux.getId() + " AL LOG DE ESTADO");
						logRepo.save(lg);
						break;
					case 3:
						lg = new Log_de_estados(aux.getEstado().getNombre(),auxE.getNombre(),aux.getUsuario().getId(),aux.getId());
						logger.info("SE ESTA REGISTRANDO LA SOLICITUD ID: "+aux.getId() + " AL LOG DE ESTADO");
						logRepo.save(lg);
						break;
					default:
						break;
				
				}
				aux.setEstado(auxE);
				solicitudRepo.save(aux);	
				return true;
			}
			else{
				logger.error("NO EXISTE LA ID ESTADO");
				return false;
			}
			
		}else {
			logger.error("NO EXISTE LA ID");
			return false;
		}
	}
	
	public boolean disponibilidadAlgoritmo() {
		logger.info("CONSULTADO EL PODER USAR EL ALGORITMO");
		Optional<Algoritmo> a = algoritmoRepo.findById(1);
		if(a.isPresent()) {
			Algoritmo aux = a.get();
			if(aux.getFlag()==0) {
				logger.info("SI PUEDE USAR EL ALGORITMO FLAG EN 0");
				return true;
			}else {
				logger.info("NO SE PUEDE USAR EL ALGORITMO FLAG EN 1");
				return false;
			}
		}else {
			logger.error("NO SE ENCUENTRA EL FLAG DEL ALGORITMO EN LA TABLA, REVISAR LA BASE DE DATOS");
			return true;
		}
		
	}
	
}
