package com.restful.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.restful.entity.Activo;
import com.restful.entity.Estado;
import com.restful.entity.Plan_de_mantenimiento;
import com.restful.entity.Solicitud;
import com.restful.entity.Tipo_de_activo;
import com.restful.entity.Usuario;
import com.restful.model.ActivoModel;
import com.restful.model.Plan_de_mantenimientoModel;
import com.restful.model.SolicitudModel;
import com.restful.model.Tipo_de_activoModel;
import com.restful.repo.ActivoRepo;
import com.restful.repo.Tipo_de_activoRepo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("activoService")
public class ActivoService {
	@Autowired
	@Qualifier("activoRepo")
	private ActivoRepo activoRepo;
	
	@Autowired
	@Qualifier("tipo_de_activoRepo")
	private Tipo_de_activoRepo tipo_de_activoRepo;
	private static final Log logger = LogFactory.getLog(ActivoService.class);

	@Autowired
	private MantenimientoSolicitudes solicitudService;

	@Autowired
	private PlanMantenimientoService planesService;
	
	public ActivoService() {
	}
	public List<Tipo_de_activoModel> obtenerTipoDeActivo(){
		logger.info("LISTAR LOS TIPOS DE ACTIVO");
		List<Tipo_de_activo> l = tipo_de_activoRepo.findAll();
		List<Tipo_de_activoModel> lModel = new ArrayList<Tipo_de_activoModel>();
		for (Tipo_de_activo t : l) {
			Tipo_de_activoModel aux = new Tipo_de_activoModel(t);
			lModel.add(aux);
		}
		return lModel;
	}
	
	public List<ActivoModel> obtenerActivos() {
		logger.info("LISTAR TODOS LOS ACTIVOS EN ESTADO VIGENTE");
		List<Activo> l = activoRepo.findAll();
		List<ActivoModel> lModel = new ArrayList<ActivoModel>();
		for (Activo activo : l) {
			ActivoModel aux = new ActivoModel(activo);
			if (aux.isEstado() == true) lModel.add(aux);
		}

		return lModel;
	}

	public List<ActivoModel> obtenerActivosLibres() {
		logger.info("LISTAR TODOS LOS ACTIVOS SIN SOLIS");
		List<Activo> l = activoRepo.findAll();
		List<ActivoModel> lModel = new ArrayList<ActivoModel>();
		
		List<SolicitudModel> lSolicitudes =solicitudService.obtenerSolicitudes();
		for (Activo activo : l) {
			ActivoModel aux = new ActivoModel(activo);
			if (aux.isEstado() == true){
				int flag =1; 
				for(SolicitudModel s : lSolicitudes){
					if(s.getId_activo()!=aux.getId()) continue;
					if(s.getEstado().getId()<5) flag=0;
				}
				if(flag==1) lModel.add(aux);
			} 
		}

		return lModel;
	}

	public List<String> obtenerModelos() {
		logger.info("LISTAR TODOS LOS ACTIVOS EN ESTADO VIGENTE");
		List<Activo> l = activoRepo.findAll();
		List<String> modelos = new ArrayList<String>();
		for (Activo activo : l) {
			ActivoModel aux = new ActivoModel(activo);
			if (aux.isEstado() == true){
				if(modelos.contains(aux.getModelo())) continue;
				modelos.add(aux.getModelo());
			} 
		}

		return modelos;
	}

	public ActivoModel obtenerActivo(int id) {
		logger.info("OBTENER UN ACTIVO A PARTIR DE SU ID: "+ id);
		
		Activo a = activoRepo.findByIdAndEstado(id, true);
	
		return new ActivoModel(a);

	}

	public String obtenerPlaca(int id){
		Optional<Activo> a = activoRepo.findById(id);
		if (a.isPresent()){
			Activo obj = a.get();
			return obj.getPlaca();
		}
		return "";
	}

	//CREAR
	public boolean crearActivo(Activo a) {
		logger.info("CREANDO UN ACTIVO");
		activoRepo.save(a);
		return true;	
	}
	
	//ACTUALIZAR
	public boolean actualizarActivo(Activo a) {
		logger.info("ACTULIZAR ACTIVO");
		if (activoRepo.existsById(a.getId())) {
			activoRepo.save(a);
			logger.info("SE ACTUALIZO EL ACTIVO CON ID: "+a.getId());
			return true;
		}else {
			logger.error("NO SE PUDO ACTUALIZAR EL ACTIVO CON ID: "+a.getId());
			return false;
		}
	}

	//ACTUALIZAR HORAS
	public boolean actualizarHoras(int id, int horas) {
		logger.info("ACTULIZAR ACTIVO");
		Optional<Activo> a = activoRepo.findById(id);
		if (a.isPresent()) {
			Activo aux = a.get();
			List<Plan_de_mantenimientoModel> planes= planesService.obtenerPlanes();
			Collections.sort(planes);
			Collections.reverse(planes);
			logger.info("ENCONTRO MODELO" + aux.getModelo());
			for(Plan_de_mantenimientoModel p : planes){
				if(p.getModelo().equals(aux.getModelo())){
					if(p.getHoras_uso()<=aux.gethoras_uso()+horas && p.getHoras_uso()>aux.gethoras_uso()){
						Solicitud s = new Solicitud();
						s.setAsunto("solicitud de mantenimiento");
						s.setPrioridad(1);
						s.setComplejidad(aux.getTipo_de_activo().getCriticidad());
						s.setDescripcion(p.getDescripcion());
						s.setEstado(new Estado(1,"Registrada"));
						s.setActivo_movil(aux);
						s.setPlan_de_mantenimiento(new Plan_de_mantenimiento(p.getId(),p.getHoras_uso(),true,p.getModelo(),p.getDescripcion()));
						Usuario u = new Usuario();
						u.setId(1);
						s.setUsuario(u);
						s.setTipo(1);					
						solicitudService.registrarSolicitud(s);
					}
				}
			}			
			aux.sethoras_uso(aux.gethoras_uso()+horas);
			activoRepo.save(aux);
			logger.info("SE ACTUALIZO ID : "+id);
			return true;
		}else {
			logger.error("FALLO EN ACTUALIZAR ID : "+id);
			return false;
		}
	}	
	// ELIMINAR 
	public boolean borrarActivo(int idActivo){
		logger.info("ELIMINANDO ACTIVO ID : " +idActivo);
		Optional<Activo> a = activoRepo.findById(idActivo);
		if (a.isPresent()) {
			Activo aux = a.get();
			List<SolicitudModel> lModel = solicitudService.obtenerSolicitudes();
			for (SolicitudModel c : lModel){
                if(c.getId_activo()==idActivo && c.isActivo() && c.getEstado().getId()<=5){
                	logger.info("ESTA ACTIVO Y ESTA EN UN ESTADO 1 2 O 3 idActivo:" +idActivo);
                    return false;
                }
            }
			aux.setEstado(false);
			activoRepo.save(aux);
			logger.info("BORRADO LOGICO ID : "+idActivo);
			return true;
		}else {
			logger.error("FALLO EN BORRADO LOGICO ID : "+idActivo);
			return false;
		}
	}
	
}
