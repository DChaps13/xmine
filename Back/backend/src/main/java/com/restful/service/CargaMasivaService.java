package com.restful.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.restful.entity.Activo;
import com.restful.entity.Solicitud;
import com.restful.entity.Usuario;
import com.restful.repo.ActivoRepo;

@Service("cargaMasivaService")
public class CargaMasivaService {
	
	@Autowired
	@Qualifier("activoRepo")
	private ActivoRepo activoRepo;
	
	@Autowired
	@Qualifier("mantenimientoSolicitudes")
	private MantenimientoSolicitudes soliService;
	
	private static final Log logger = LogFactory.getLog(CargaMasivaService.class);
	
	public void cargaMasivaSolicitudes(File file,int idU)throws IOException {
		logger.info("ESTA LEYENDO EL EXCEL DE CARGA MASIVA DE SOLICITUDES");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
	    // we get first sheet
	    XSSFSheet sheet = workbook.getSheetAt(0);
	    boolean cabecera=true;
	    // we iterate on rows
	    Iterator<Row> rowIt = sheet.iterator();
	    Usuario u = new Usuario();
	    u.setId(idU);
	    while(rowIt.hasNext()) {
	    
	      if(cabecera) {
	    	  rowIt.next();
	    	  cabecera = false;
	      }else {
	    	  Row row = rowIt.next();  
		      // iterate on cells for the current row
		      Iterator<Cell> cellIterator = row.cellIterator();

		      Solicitud s = new Solicitud();
		      s.setUsuario(u);
		      int i =1 ;
		      while (cellIterator.hasNext()) {
		        Cell cell = cellIterator.next();
		        //System.out.print(cell.toString() + ";");
		        switch (i) {
				case 1:
					s.setDescripcion(cell.toString());
					break;
				case 2:
					// placa
					Activo a = activoRepo.findByPlacaAndEstado(cell.toString().toUpperCase(), true);
					if(a != null){
						logger.info("ACTIVO PLACA: "+ cell.toString().toUpperCase()+ " CON ID: "+a.getId());
						s.setActivo_movil(a);
						s.setComplejidad(a.getTipo_de_activo().getCriticidad());
					}else {
						logger.error("ACTIVO PLACA: "+ cell.toString().toUpperCase()+ " NO ENCONTRADO");	
					}
					break;
				case 3:
					logger.info(" SETEANDO EL TIPO A LA SOLICITUD");
					if (cell.toString().compareTo("M") == 0) s.setTipo(1);
					else s.setTipo(2);
					break;
				case 4:
					logger.info(" SETEANDO LA PRIORIDAD A LA SOLICITUD");
					s.setPrioridad((int)Double.parseDouble(cell.toString()));
					break;
				}
		        i++;
		      }
		      soliService.registrarSolicitud(s);
		      

		      //System.out.println();
	      }


	    }

	    workbook.close();
	    fis.close();
	    
	}
	
}
