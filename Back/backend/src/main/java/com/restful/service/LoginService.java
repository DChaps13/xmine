package com.restful.service;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.restful.entity.Usuario;
import com.restful.repo.UsuarioRepo;

@Service("loginServicio")
public class LoginService {

	@Autowired
	@Qualifier("UsuarioRepo")
	private UsuarioRepo usuariorepo;
	
	private static final Log logger = LogFactory.getLog(LoginService.class);
	
	/*
	 * @RequestMapping(path = "/usuarios/login", method = RequestMethod.POST)
	public @ResponseBody Usuario loguearUsuario(@RequestParam String correo,@RequestParam String password ) throws Exception {
        Usuario respuesta = usuarioService.logueo(correo,password);
		return respuesta;
	}
	 * */
	
	public Usuario login (String username, String contrasena) {
		logger.info("ENTRANDO AL LOGIN");
		Usuario u = usuariorepo.findByUsernameAndContrasena(username, contrasena);
		

		if (u == null) {
			logger.info("NO INGRESO");
			return u;
		}
		else {
			if (u.isActivo()) {
				logger.info("INGRESO");
				return u;	
			}else {
				logger.info("USUARIO CON ID: "+ u.getId() + " NO ACTIVO EN EL SISTEMA");
				u  = new Usuario();
				u.setId(-1);
				return u;
			}
			
		}
	}
}
