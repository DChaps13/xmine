package com.restful.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.restful.entity.Contrato;
import com.restful.entity.Contrato_x_tipo_de_activo;
import com.restful.model.ContratoModel;
import com.restful.model.Contrato_x_tipo_de_activoModel;
import com.restful.model.SolicitudModel;
import com.restful.repo.ContratoRepo;
import com.restful.repo.Contrato_x_tipo_de_activoRepo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("contratoService")
public class ContratoService {

    @Autowired
    @Qualifier("contratoRepo")
    private ContratoRepo contratoRepo;

    @Autowired
    private ContratoTipoService contratoTipoService;

    @Autowired
	@Qualifier("contrato_x_tipo_de_activoRepo")
    private Contrato_x_tipo_de_activoRepo contratoTipoRepo;
    
    @Autowired
    private MantenimientoSolicitudes mantenimientoSolicitudes;

    private static final Log logger = LogFactory.getLog(ProveedorService.class);


    // LISTAR
    public List<ContratoModel> obtenerContratos() {
        List<Contrato> l = contratoRepo.findAll();
        List<ContratoModel> lModel = new ArrayList<ContratoModel>();
        for (Contrato contrato : l) {
            ContratoModel aux = new ContratoModel(contrato);
            if (aux.isActivo()) lModel.add(aux);
        }
        return lModel;
    }

    public boolean borrarContrato(int idContrato) {
        logger.info("ELIMINANDO CONTRATO ID : " + idContrato);
        Optional<Contrato> p = contratoRepo.findById(idContrato);
		if (p.isPresent()) {
            Contrato aux = p.get();
            
			//validar que no tenga solicitudes
			List<SolicitudModel> lSoli = mantenimientoSolicitudes.obtenerSolicitudes();
			for(SolicitudModel s: lSoli){
				if(s.getId_proveedor()==aux.getProveedor().getId()) return false;
			}
			logger.info("BORRADO LOGICO ID : "+idContrato);
            //borrar contrato_x_tipo_activo asociados
            List<Contrato_x_tipo_de_activoModel> lModel = contratoTipoService.obtenerContratosTipo();
            for (Contrato_x_tipo_de_activoModel ct : lModel){
                if(ct.getContrato().getId()==idContrato){
                    contratoTipoService.borrarContratoTipo(ct.getId());
                }
            }
            aux.setActivo(false);
            contratoRepo.save(aux);
			return true;
		}else {
			logger.error("FALLO EN BORRADO LOGICO ID : "+idContrato);
			return false;
		}
    }
    
    //CREAR
	public boolean crearContrato(Contrato a) {
        logger.info("CREANDO UN CONTRATO");
        //System.out.print(a.getCapacidad_total_mant());
        List<Contrato_x_tipo_de_activo>aux = a.getContratos_x_tipo_de_activos();
        a.setContratos_x_tipo_de_activos(null);
        a.setCantidad_calificado(0);
        a.setCapacidad_mantenimiento_inicial(a.getCapacidad_total_mant());
        a.setCapacidad_reparacion_inicial(a.getCapacidad_total_rep());
        a.setCapacidad_total_inicial(a.getCapacidad_mantenimiento_inicial() + a.getCapacidad_reparacion_inicial());
        contratoRepo.save(a);
        logger.info("CREANDO Contrato_x_tipo_de_activo");
        for(Contrato_x_tipo_de_activo c : aux){
            Contrato d= new Contrato();
            d.setId(a.getId());
        	c.setContrato(d);
        	c.setCantidad_calificado(0);
            contratoTipoRepo.save(c);
        }
		return true;	
	}

    public ContratoModel obtenerContrato(int id) {
		logger.info("OBTENER UN CONTRATO A PARTIR DE SU ID: "+ id);
		
        Contrato a = contratoRepo.findByIdAndActivo(id, true);
        ContratoModel c= new ContratoModel(a);
        List<Contrato_x_tipo_de_activoModel> ls = contratoTipoService.obtenerContratosTipo();
        List<Contrato_x_tipo_de_activoModel> ls2 = new ArrayList<Contrato_x_tipo_de_activoModel>();
        for(Contrato_x_tipo_de_activoModel cT : ls){
            if(cT.getContrato().getId()==id) ls2.add(cT);
        }
        c.setContratoDetalle(ls2);
		return c ;

	}
	
	
}
