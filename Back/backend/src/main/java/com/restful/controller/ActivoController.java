package com.restful.controller;

import java.util.List;
import java.util.Map;

import com.restful.entity.Activo;
import com.restful.model.ActivoModel;
import com.restful.model.Tipo_de_activoModel;
import com.restful.service.ActivoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/activo")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.OPTIONS})
public class ActivoController {
	@Autowired
	@Qualifier("activoService")
	private ActivoService activoService;
	
	@CrossOrigin
	@PostMapping("/lista")
	public @ResponseBody List<ActivoModel> obtenerActivo(){
		//System.out.println(activoService);
		return activoService.obtenerActivos();
	}
	
	@PostMapping("/lista/tipo_de_activo")
	public @ResponseBody List<Tipo_de_activoModel> listarTipoDeActivo(){
		return activoService.obtenerTipoDeActivo();
	}

	@PostMapping("/obtenerPlaca")
	public String obtenerPlaca(@RequestBody Map<String, Integer> data){
		return activoService.obtenerPlaca(data.get("id"));
	}

	@PostMapping("/crear")
	public boolean crearActivo(@RequestBody Activo a){
		System.out.println(a);
		return activoService.crearActivo(a);
	}

	@PostMapping("/actualizar")
	public boolean actualizarActivo(@RequestBody Activo a){
		return activoService.actualizarActivo(a);
	}

	@PostMapping("/borrar")
	public boolean borrarActivo(@RequestBody Map<String, Integer> data){
		return activoService.borrarActivo(data.get("idActivo"));
	}

	@PostMapping("/modelos")
	public List<String> obtenerModelos(){
		return activoService.obtenerModelos();
	}

	@PostMapping("/modelosLibres")
	public List<ActivoModel> obtenerActivosLibres(){
		return activoService.obtenerActivosLibres();
	}

	@PostMapping("/actualizarHoras")
	public boolean actualizarHoras(@RequestBody Map<String, Integer> data){
		return activoService.actualizarHoras(data.get("id"),data.get("horas"));
	}
}
