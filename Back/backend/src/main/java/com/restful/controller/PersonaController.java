
package com.restful.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restful.entity.Persona;
import com.restful.model.MPersona;
import com.restful.service.PersonaService;

@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class PersonaController {
	@Autowired
	@Qualifier("servicio")
	PersonaService service;
	
	@PutMapping("/nota")
	public boolean agregarPersona(@RequestBody @Valid Persona persona){
		return service.crear(persona);
	}
	
	@PostMapping("/nota")
	public boolean actualizarNota(@RequestBody @Valid Persona persona) {
		return service.actualizar(persona);
	}
	
	@DeleteMapping("/nota/{id}/{nombre}")
	public boolean borrarPersona(@PathVariable("id") int id, @PathVariable("nombre") String nombre) {
		return service.borrar(nombre, id);
	}
	
	@GetMapping("/notas")
	public List<MPersona> obtenerNotas(){
		return service.obtener();
	}
 }
