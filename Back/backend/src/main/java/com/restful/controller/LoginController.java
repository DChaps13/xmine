package com.restful.controller;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.restful.entity.Usuario;
import com.restful.model.UsuarioModel;
import com.restful.service.LoginService;

@RestController
@RequestMapping("/login")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})

public class LoginController {
	@Autowired
	@Qualifier("loginServicio")
	LoginService loginServicio;
	
	@PostMapping("/")
	public @ResponseBody UsuarioModel login(@RequestBody Map<String, String> data) {
		Usuario respuesta = loginServicio.login(data.get("usuario"), data.get("contrasena"));
		
		if (respuesta != null) {
			UsuarioModel u = new UsuarioModel(respuesta);
			return u;
		}
		else {
			UsuarioModel u = new UsuarioModel();
			u.setId(0);
			return u;
		}
			
	}
}
