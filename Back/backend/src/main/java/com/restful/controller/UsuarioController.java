package com.restful.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.restful.entity.Usuario;
import com.restful.model.UsuarioModel;
import com.restful.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.OPTIONS})
public class UsuarioController {
	@Autowired
	@Qualifier("usuarioService")
	private UsuarioService usuarioService;
	
	
	@PostMapping("/lista")
	public @ResponseBody List<UsuarioModel> obtenerUsuarios(){
		return usuarioService.listarUsuario();
	}
	
	@PostMapping("/crear")
	public @ResponseBody boolean crearUsuario(@RequestBody Usuario u) {
		return usuarioService.crearUsuario(u);
	}
	
	@PostMapping("/eliminar")
	public @ResponseBody boolean eliminarUsuario(@RequestBody Map<String, Integer> data) {
		return usuarioService.borrarUsuario(data.get("idUsuario"));
	}
	
	@PostMapping("/actualizar")
	public @ResponseBody boolean actualizarUsuario(@RequestBody Usuario u) {
		return usuarioService.actualizarUsuario(u);
	}
}
