package com.restful.controller;

import java.util.List;
import java.util.Map;

import com.restful.entity.Contrato;
import com.restful.model.ContratoModel;
import com.restful.service.ContratoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contrato")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.OPTIONS})
public class ContratoController {
	@Autowired
	@Qualifier("contratoService")
	private ContratoService contratoService;
	
	@CrossOrigin
	@PostMapping("/lista")
	public @ResponseBody List<ContratoModel> obtenerContratos(){
		//System.out.println(activoService);
		return contratoService.obtenerContratos();
	}

	@PostMapping("/crear")
	public @ResponseBody boolean crearContrato(@RequestBody Contrato a){
		return contratoService.crearContrato(a);
	}


	@PostMapping("/borrar")
	public @ResponseBody boolean borrarContrato(@RequestBody Map<String, Integer> data){
		return contratoService.borrarContrato(data.get("id"));
	}

	@PostMapping("/obtenerContrato")
	public @ResponseBody ContratoModel obtenerContrato(@RequestBody Map<String, Integer> data){
		return contratoService.obtenerContrato(data.get("id"));
	}
}
