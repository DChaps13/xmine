package com.restful.controller;

import java.util.List;
import java.util.Map;

import com.restful.entity.Plan_de_mantenimiento;
import com.restful.model.Plan_de_mantenimientoModel;
import com.restful.service.PlanMantenimientoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/planM")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class PlanMantenimientoController {
	@Autowired
	@Qualifier("planMantenimientoService")
	private PlanMantenimientoService planMantenimientoService;
	
	@PostMapping("/lista")
	public @ResponseBody List<Plan_de_mantenimientoModel> obtenerProveedores(){
		return planMantenimientoService.obtenerPlanes();
	}

	@PostMapping("/crear")
	public @ResponseBody boolean crearProvedor(@RequestBody Plan_de_mantenimiento p) {
		
		return planMantenimientoService.crearPlan(p);
	}

	@PostMapping("/actualizar")
	public @ResponseBody boolean actualizarProveedor(@RequestBody Plan_de_mantenimiento p) {
		return planMantenimientoService.actualizarPlan(p);
	}

	@PostMapping("/borrar")
	public @ResponseBody boolean borrarProveedor(@RequestBody Map<String, Integer> data){
		return planMantenimientoService.borrarPlan(data.get("id"));
	}

}
