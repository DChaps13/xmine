package com.restful.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.restful.service.CargaMasivaService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/cargaMasiva")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.OPTIONS})

public class CargaMasivaController {

	@Autowired
	@Qualifier("cargaMasivaService")
	private CargaMasivaService cargaService;
	
	@RequestMapping(value = "/cargaSolicitudes",method=RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public boolean subirSoliciudes(@RequestParam("file") MultipartFile file,@RequestParam("id_usuario") int id)throws IOException {
		File convertFile = new File(file.getOriginalFilename()); // CREAR EL ARCHIVO VACIO
		
		
		convertFile.createNewFile(); // LO CREAR
		try(FileOutputStream fout = new FileOutputStream(convertFile))
		{
			fout.write(file.getBytes());
			// LLAMAR AL SERVICIO PARA CARGAR LAS SOLICITUDES EL ARCHIVO
			cargaService.cargaMasivaSolicitudes(convertFile,id);
			convertFile.delete();
			System.out.println(id);
			return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		
	}
	
}
