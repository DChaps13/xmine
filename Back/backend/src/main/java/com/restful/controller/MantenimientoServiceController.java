package com.restful.controller;




import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.restful.entity.Estado;
import com.restful.entity.Solicitud;
import com.restful.model.SolicitudModel;
import com.restful.service.MantenimientoSolicitudes;

@RestController
@RequestMapping("/solicitud")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})

public class MantenimientoServiceController {
	/*
	 * Mantenimiento de solicitudes Servicion CRUD 
	 * 
	 * */
	@Autowired
	@Qualifier("mantenimientoSolicitudes")
	private MantenimientoSolicitudes mantenimientoSolicitudes;
	
	//@RequestMapping(path = "/crear", method = RequestMethod.POST,headers="Accept=application/json")
	@PostMapping("/crear")
	public @ResponseBody boolean crearSolicitud(@RequestBody @Valid Solicitud s) {
		//System.out.println(s);
		//System.out.println(s.get("activo").getClass().getName());
		//try {
			if (s.getProveedor() != null) s.setEstado(new Estado(2,"Asignada"));
			return mantenimientoSolicitudes.registrarSolicitud(s);
		//} catch (Exception e) {
			// TODO: handle exception
		//	System.out.println(e.getMessage());
		//	return false;
		//}
		
		//return null;
	}
	
	@PostMapping("/actualizar")
	public @ResponseBody boolean actualizarSolicitud(@RequestBody @Valid Solicitud s) {
		return mantenimientoSolicitudes.actualizar(s);
	}
	
	@PostMapping("/borrar")
	public @ResponseBody boolean borrarSolicitud(@RequestBody Map<String, Integer> data ) {
		return mantenimientoSolicitudes.borrar(data.get("idSolicitud"));
	}
	
	@PostMapping("/lista")
	public @ResponseBody List<SolicitudModel> listarSolicitudes(){
		return mantenimientoSolicitudes.obtenerSolicitudes();
	}

	@PostMapping("/asignacionM")
	public @ResponseBody boolean asignarMantenimientos(){
		return mantenimientoSolicitudes.asignar(1);
	}

	@PostMapping("/asignacionR")
	public @ResponseBody boolean asignarReparaciones(){
		return mantenimientoSolicitudes.asignar(2);
	}

	@PostMapping("/calificar")
	public @ResponseBody boolean calificarSolicitud(@RequestBody Map<String,Object> data ) {
		return mantenimientoSolicitudes.calificarSolicitud((int)data.get("id"), (double)(Integer)data.get("calif"),""+data.get("comment"));
	}	

	@PostMapping("/asignarProveedor")
	public @ResponseBody boolean asignarProveedor(@RequestBody Map<String,Integer> data) {
		return mantenimientoSolicitudes.asignarProveedorASolicitud(data.get("id_solicitud"),data.get("id_proveedor"));
	}
	@PostMapping("/obtenerSolicitud")
	public @ResponseBody SolicitudModel obtenerSoli(@RequestBody Map<String,Integer> data ) {
		return mantenimientoSolicitudes.obtenerSoli(data.get("id"));
	}

	@PostMapping("/cambiarEstado")
	public @ResponseBody boolean cambiarEstado(@RequestBody Map<String,Object> data ) {
		return mantenimientoSolicitudes.cambiarEstado((int)data.get("idSoli"),(int)data.get("idEstado"),""+data.get("comment"));
	}
	
	@PostMapping("/disponibilidadAlgoritmo")
	public @ResponseBody boolean dispAlgoritmo(){
		return mantenimientoSolicitudes.disponibilidadAlgoritmo();
	}
	

}
