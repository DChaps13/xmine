package com.restful.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.restful.model.ProveedorModel;
import com.restful.model.Tipo_de_activoModel;
import com.restful.model.reporteGeneral1;
import com.restful.service.ReporteService;

@RestController
@RequestMapping("/reporte")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.OPTIONS})
public class ReporteController {
	@Autowired
	@Qualifier("reporteService")
	private ReporteService reporteService;
	
	@PostMapping("/capacidadDeUso")
	public @ResponseBody List<reporteGeneral1> capacidadDeUso(@RequestBody Map<String,Integer> data){
		return reporteService.capacidadDeUso(data.get("mes"));
	}

	@PostMapping("/costoProveedores")
	public @ResponseBody List<ProveedorModel> costoPorProveedor(@RequestBody Map<String, Integer> data){
		return reporteService.costoPorProveedor(data.get("mes"));
	}

	@PostMapping("/costoTipoActivos")
	public @ResponseBody List<Tipo_de_activoModel> costoPorTipoActivo(@RequestBody Map<String,Integer> data){
		return reporteService.costoPorTipoActivo(data.get("mes"));
	}
}
