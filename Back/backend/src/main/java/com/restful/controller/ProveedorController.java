package com.restful.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.restful.entity.Proveedor;
import com.restful.model.ProveedorModel;
import com.restful.service.ProveedorService;

@RestController
@RequestMapping("/proveedor")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class ProveedorController {
	@Autowired
	@Qualifier("proveedorService")
	private ProveedorService proveedorServicio;
	
	@PostMapping("/lista")
	public @ResponseBody List<ProveedorModel> obtenerProveedores(){
		return proveedorServicio.obtenerProveedores();
	}

	@PostMapping("/crear")
	public @ResponseBody Proveedor crearProvedor(@RequestBody Proveedor p) {
		
		return proveedorServicio.crearProvedor(p);
	}

	@PostMapping("/actualizar")
	public @ResponseBody Proveedor actualizarProveedor(@RequestBody Proveedor p) {
		return proveedorServicio.actualizarProveedor(p);
	}

	@PostMapping("/borrar")
	public @ResponseBody boolean borrarProveedor(@RequestBody Map<String, Integer> data){
		return proveedorServicio.borrarProveedor(data.get("idProveedor"));
	}

	@PostMapping("/bloquear")
	public @ResponseBody boolean bloquearProveedor(@RequestBody Map<String, Integer> data){
		return proveedorServicio.bloquearProveedor(data.get("idProveedor"));
	}
	
	@PostMapping("/desbloquear")
	public @ResponseBody boolean desbloquearProveedor(@RequestBody Map<String, Integer> data){
		return proveedorServicio.desbloquearProveedor(data.get("idProveedor"));
	}
	
	@PostMapping("/verEstadoBloqueo")
	public @ResponseBody boolean verEstadoBloqueo(@RequestBody Map<String, Integer> data) {
		return proveedorServicio.verEstadoBloqueado(data.get("idProveedor"));
	}

	@PostMapping("/obtenerNombre")
	public @ResponseBody String obtenerNombre(@RequestBody Map<String, Integer> data){
		return proveedorServicio.obtenerNombre(data.get("id"));
	}
	
	
	
}
